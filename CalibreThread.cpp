/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  CalibreThread.cpp $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Aux thread for Calibre functions
**
**  Entries:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       07 Aug 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"


//
//  Function:   Entrypoint for the Calibre working Thread
//  Purpose:    Startup the Calibre thread by the application's MainFrame :
//              AfxBeginThread(CalibreThread, hWnd, THREAD_PRIORITY_NORMAL);
//
//  Parms:      Window handle
//  Returns:    Not until program exit
//
UINT CalibreThread(LPVOID parameter)
{
   BOOL        fListenForCommands=TRUE;
   CCalCmd    *pclCmd;
   CTitle     *pclTitle;

   pclTitle = new(CTitle);
   pclCmd   = new(CCalCmd);
   //
   HANDLE tEvtHandle = CreateEvent(NULL, TRUE, FALSE, "CmdEvent");
   pclCmd->EventHandlePut(tEvtHandle);
   //
   // Sit here and await CMD class events until we exit the program
   //
   do
   {
      WaitForSingleObject(tEvtHandle, INFINITE);
      //
      fListenForCommands = pclCmd->ExecuteCmd(pclTitle);
   }
   while(fListenForCommands);
   //
   return(0);
}

