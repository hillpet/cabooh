// Calibre.h : main header file for the PROJECT_NAME application
//

#pragma once

#ifndef __AFXWIN_H__
   #error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"         // main symbols
#include <AutoFont.h>
#include <ReadOnlyEdit.h>
#include <CvbFiles.h>
#include <NvmStorage.h>
#include <ClockApi.h>
#include <FolderDlg.h>
#include <Cmdline.h>
#include <CMyString.h>
#include <Log.h>
//
#include "NvmDefs.h"
#include "CalibreThread.h"
#include "CalibreFiles.h"
//
#include "Books.h"
#include "Authors.h"
#include "Titles.h"
#include "Commands.h"


//#define FEATURE_PROMPT_EMPTY_CSV     // Promt if the CSV database if empty
//#define FEATURE_CHECK_MEMORY         // Enable AFX memory pool checker
//#define FEATURE_WRITE_DEBUG          // Enable debug output
//#define FEATURE_VALIDATE_TG          // Validate Tg words agains Db words
#define FEATURE_LOG_BOOKS_FOLDER       // Write nested book folders to logfile
#define FEATURE_NEST_BOOKS_FOLDER      // Allow nested book folders

enum
{
   WM_CALIBRE_OKEE = WM_USER + 100,          // Thread has started
   WM_CALIBRE_DB_READY,                      //
   WM_CALIBRE_BOOKS_READY,                   //
   WM_CALIBRE_ADD_READY,                     //
   WM_CALIBRE_MOVE_READY,                    //
   WM_CALIBRE_DELETE_READY,                  //
   WM_CALIBRE_EXPORT_READY,                  //
   WM_CALIBRE_FREE_READY,                    //
   WM_CALIBRE_MESSAGE,                       // Generic msgs
   WM_CALIBRE_SHOW_PROGRESS,                 //
   WM_CALIBRE_BOOKS_RESCAN,                  //

   NUM_WM_CALIBRE_MESSAGES
};



#define CALIBRE_VERSION                      "v4.20e-002"
#define CALIBRE_DB_VERSION                   "v1.12"
#define CABOOH_CONFIG                        "cabooh.cfg"

#define CMDLINE_REAL                         'R'
#define CMDLINE_QUIET                        'q'
#define CMDLINE_DATABASE                     'd'
#define CMDLINE_INPUT                        'i'
#define CMDLINE_AUTHOR                       'A'
#define CMDLINE_TITLE                        'T'
#define CMDLINE_WORDS                        'W'
#define CMDLINE_LETTERS                      'L'


//
// CCalibreApp:
//
class CCalibreApp : public CWinApp
{
public:
   CCalibreApp();
   CMyCommandLineInfo clCmdInfo;

public:
   virtual BOOL      InitInstance   ();
   BOOL              BrowseFolder   (CString *, char *);
   BOOL              BrowseFile     (CString *, CString *, char *);

private:
   BOOL              NewNvm         (CString *);
   CString           GetAppFolder   ();

   //
   CString           clStorage;
   CAuthor           clMap;
   CCalCmd           clCmd;
   CNvmStorage      *pclNvm;

// Implementation
   DECLARE_MESSAGE_MAP()
};

extern CCalibreApp theApp;
