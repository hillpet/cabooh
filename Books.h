/*  (c) Copyright:  2011  PATRN.NL, Confidential Data
**
**  $Workfile:          Books.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       1 Sep 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_BOOKS_H_)
#define _BOOKS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//
// CBook class: top level data structure for the database
//
class CBook;
typedef class CBook CBOOK;

//
// Title candidates list (references to database entries close to this title)
//
typedef struct TLIST
{
   int      iScore;
   CBOOK   *pclBook;
   TLIST   *pstNext;
}  TLIST;


class CBook
{
public:
            CBook();
   virtual ~CBook();
            CBook & CBook::operator= (const CBook &);
   //
   // PUBLIC Methods
   //
   CBOOK   *DB_AddAuthor                  (void);
   CBOOK   *DB_AddAuthor                  (char *);
   CBOOK   *DB_AddAuthor                  (CString *);
   CBOOK   *DB_AddAuthor                  (char *, BOOL);
   CBOOK   *DB_AddAuthor                  (CString *, BOOL);
   CBOOK   *DB_AddTitle                   (char *, BOOL);
   CBOOK   *DB_AddTitle                   (CString *, BOOL);
   CBOOK   *DB_AddTitle                   (CString *, CBOOK *, BOOL);
   CBOOK   *DB_AddTitle                   (char *, CBOOK *, BOOL);
   int      DB_CalibreCsvAddBooks         (CString *, CASTAT);
   int      DB_CalibreCsvRead             (CString *);
   int      DB_CalibreCsvExport           (CString *, CString *);
   int      DB_CalibreDeleteById          (CString *, CASTAT);
   BOOL     DB_CheckMemory                (void);
   void     DB_CleanupDB                  (void);
   void     DB_FreeAllCandidates          (void);
   int      DB_FreeCandidates             (TLIST *);
   void     DB_FreeDB                     (void);
   CBOOK   *DB_GetAuthorInfo              (int, CString *);
   CBOOK   *DB_GetAuthorList              (void);
   char    *DB_GetAsciiBookFormat         (EBOOK);
   EBOOK    DB_GetBinaryBookFormat        (CString *);
   BOOL     DB_GetFileInfo                (char *, CString *, CString *, u_int64 *);
   char    *DB_GetTitle                   (int, CBOOK *);
   CBOOK   *DB_GetTitleInfo               (int, CBOOK *, CString *);
   BOOL     DB_IsRecognized               (CATA);
   BOOL     DB_IsRecognized               (CBOOK *, CATA);
   int      DB_ReadRecord                 (char *);
   int      DB_ReadRecord                 (CString *);
   int      DB_ReadRecord                 (char *, CString *);
   int      DB_ReadRecord                 (CString *, CString *);
   int      DB_ReIndexAuthors             (void);
   int      DB_ReIndexAuthors             (CBook *);
   void     DB_ResetAllIndex              (void);
   int      DB_RetrieveCandidates         (TLIST *, CString *);
   void     DB_UpdateProgressBar          (int);
   CAVAL    DB_ValidateTarget             (CBOOK *, CATA, CATG);
   //
   // STATIC Data members
   //
public:
   static int           iInstance;        // CBook instance
   static int           iCurLib;          // Currently  used libraries
   static CString      *pclLibrary[CALIBRE_MAXLIBS];
   //
   static CLog         *pclLog;           // LOG 
   static CNvmStorage  *pclNvm;           // Storage
   static BOOL          fFileOpen;        // File open flag
   static CStdioFile   *pclInFile;        //      raw books info file
   static CBOOK        *pclAuthorList;    // Top of the linked list : authors 
   static TLIST        *pstAltSolList;    // Top of the linked list : alternate solutions 
   //
public:
   static CMyString    *pclRaw;           // Raw record
   static int           iWordLength;      // 
   static int           iNumAuthors;      // Total number of Authors
   static int           iNumTitles;       //                 Titles
   static int           iNumDuplicates;   //                 Duplicated
   static int           iNumBadFormat;    //                 Bad formatted books
   static int           iCvsAuthor;       // CSV field index
   static int           iCvsAuthorSort;   //  |
   static int           iCvsTitle;        //  |
   static int           iCvsTitleSort;    //  |
   static int           iCvsFormats;      //  |
   static int           iCvsIds;          //  |
   static int           iCvsBookSize;     //  |

public:
   //
   // PUBLIC Data members
   //
   CBOOK         *pclPrev;                // Prev member in list
   CBOOK         *pclNext;                // Next member in list
   CBOOK         *pclXref;                // Corresponding author/title
   CMyString     *pclName;                // Author/Title name
   CMyString     *pclDir;                 // Raw Book directory path
   CMyString     *pclFile;                // Raw Book Filename
   TLIST         *pstCandidates;          // Linked list of alternatives for new books
   //                                     //    int    iScore
   //                                     //    CBOOK *pclBook
   //                                     //    TLIST *pstNext
   int            iLibNum;                // Library number
   CATA           tAuthorTitle;           // CALIBRE_AUTHOR or CALIBRE_TITLE
   SPLIT          tSplit;                 // Split type  : CALIBRE_SPLIT_AUTHOR_TITLE, ..
   int            iSplit;                 // Split index :
   CASTAT         tStatus;                // eBook status: CALIBRE_STATUS_NEW, ..
   EBOOK          tFormat;                // eBook format: CALIBRE_EBOOK_EPUB, ..
   EBOOK          tAllFormats;            // eBook all formats
   BOOL           fBoxChecked;            // eBook current Checkbox wildcard check marker
   int            iBoxIndex;              // eBook current list index
   int            iWildIndex;             // eBook current Checkbox wildcard listindex
   int            iCalibreId;             // Calibre ID
   int            iCalibreSize;           // Calibre book filesize
   int            iScore;                 // New book author/title score
   int            iScoreWord;             //    word   based score
   int            iScoreLetter;           //    letter based score
   //
   // PRIVATE Data members
   //
private:

   //
   // PRIVATE Methods
   //
private:
   BOOL     db_AddPathToBook              (CBOOK *, CString *);
   CBOOK   *db_CleanupBooks               (CBOOK *, BOOL);
   BOOL     db_ExtractItem                (CString *, CAEXP);
   BOOL     db_CopyItem                   (CString *, char *);
const char *db_GetBookStatus              (CBOOK *);
   EBOOK    db_GetFormats                 (void);
   int      db_GetItemLength              (char *);
   CBOOK   *db_LookupAuthor               (CString *);
   BOOL     db_LookupAuthors              (void);
   int      db_ParseExports               (CAEXP);
   int      db_ReIndexTitles              (CBook *);
   char    *db_SkipItem                   (char *);
   char    *db_SkipString                 (char *);
   int      db_VerifyWords                (CMyString *, CMyString *, int, int *, int *);
   BOOL     db_UpdateCandidates           (CBOOK *, int);
};

#endif // !defined(_BOOKS_H_)
