/*  (c)2011 Patrn.nl
**
**  $Workfile:  Titles.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define the Calibre Title class
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10 Mar 2012
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_TITLES_H_)
#define _TITLES_H_

//
// CTitle
//
class CTitle : public CAuthor
{
public:
            CTitle();
   virtual ~CTitle();
   CTitle&  CTitle::operator=             (const CTitle&);
   //
   // PUBLIC Methods
   //
   int      TITLE_ParseBooks              (CString *);
   CBOOK   *TITLE_GetTitleByIndex         (int);
   CBOOK   *TITLE_GetTitleByWildcardIndex (int);
   //               
   // PRIVATE Methods
   //
private:
   BOOL     title_CheckHighScore          (CBOOK *, CAVAL);
   BOOL     title_CopyTitleAuthor         (CBOOK *, CATA);
   BOOL     title_CopyTitleAuthor         (CBOOK *, CATA, SPLIT, int);
   CADET    title_DetermineAuthor         (void);
   BOOL     title_ExtractAuthor           (CBOOK *);
   int      title_GetRecord               (CString *, BOOL);
   BOOL     title_HandleBatchMoveCopy     (BOOL);
   BOOL     title_HandleBatchDelete       (BOOL);
   int      title_HandleBatchError        (CString *, CString *, BOOL);
   int      title_HandleBatchError        (CString *, char *, BOOL);
   EBOOK    title_ParseBookType           (void);
   BOOL     title_ParseTitles             (CBOOK *);
   BOOL     title_ParseRecord             (BOOL);
   BOOL     title_RetrieveTitleAuthor     (CBOOK *, CATA);
   //
   // PUBLIC Data members
   //
public:

   //
   // PRIVATE Data members
   //
private:

};

#endif // !defined(_TITLES_H_)
