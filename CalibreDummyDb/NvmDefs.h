/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  NvmDefs.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define the NVM storage members
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       06 Jun 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_NVMDEFS_H_)
#define _NVMDEFS_H_

enum
{
   NVM_VERSION = 0,
   NVM_INITIALIZED,              // mark if NVM was bad !
   NVM_NAME_DATABASE_FILE,
   NVM_NAME_DATABASE_DIR,

   NVM_AUTHOR_THOLD,
   NVM_TITLE_THOLD,
   NVM_WORD_THOLD,
   NVM_LETTER_THOLD,

   NVM_CALIBRE_QUIETMODE,
   NVM_DB_VERSION,

   NUM_WNVMS
};
#endif // !defined(_NVMDEFS_H_)
