/*  (c)2011 Patrn.nl
**
**  $Workfile:   CalibreDB.h $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29 Aug 2011
**
 *
 *
 *
 *
**/

#pragma once

#ifndef __AFXWIN_H__
   #error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"      // main symbols
#include "CvbFiles.h"
#include "NvmStorage.h"
#include "ReadOnlyEdit.h"
#include "AutoFont.h"
#include "Cmdline.h"
#include "ClockApi.h"

#define  CALIBREDB_VERSION       "1.02"            // Cabooh CalibreDB version
#define  CALIBREDB_DB_VERSION    "1.00"            // NVM Database version
#define  CABOOH_CONFIG           "caboogDb.cfg"    // Config filename
//
#define  CRLF                    "\r\n"            // CrLf
#define  CALIBRE_TXT_FONT_SIZE   14

const COLORREF CLOUDBLUE   = RGB(128, 184, 223);
const COLORREF WHITE       = RGB(255, 255, 255);
const COLORREF BLACK       = RGB(1, 1, 1);
const COLORREF DKGRAY      = RGB(128, 128, 128);
const COLORREF LTGRAY      = RGB(192, 192, 192);
const COLORREF YELLOW      = RGB(255, 255, 0);
const COLORREF DKYELLOW    = RGB(128, 128, 0);
const COLORREF LTRED       = RGB(255, 128, 128);
const COLORREF RED         = RGB(255, 0, 0);
const COLORREF DKRED       = RGB(128, 0, 0);
const COLORREF BLUE        = RGB(0, 0, 255);
const COLORREF DKBLUE      = RGB(0, 0, 128);
const COLORREF CYAN        = RGB(0, 255, 255);
const COLORREF DKCYAN      = RGB(0, 128, 128);
const COLORREF LTGREEN     = RGB(128, 255, 128);
const COLORREF GREEN       = RGB(128, 255, 128);
const COLORREF DKGREEN     = RGB(0, 128, 0);
const COLORREF MAGENTA     = RGB(255, 0, 255);
const COLORREF DKMAGENTA   = RGB(128, 0, 128);

class CCalibreDBApp : public CWinApp
{
public:
   CCalibreDBApp();

// Overrides
public:
   virtual BOOL InitInstance();
   CMyCommandLineInfo clCmdInfo;

private:
   BOOL              NewNvm               ();
   CString           clStorage;
   CNvmStorage       clNvm;

// Implementation

   DECLARE_MESSAGE_MAP()
};

extern CCalibreDBApp theApp;