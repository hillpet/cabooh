/*  (c)2011 Patrn.nl
**
**  $Workfile:  CalibreFiles.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define the CalibreDBDummy helper functions
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       06 Jun 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_CALIBREFILES_H_)
#define _CALIBREFILES_H_


#define CRLF                     "\r\n"

#define CALIBRE_TXT_FONT_SIZE     14
//
#define CALIBRE_RECORD_LEN        8192
#define CALIBRE_MAX_TEXT_LEN      256
#define CALIBRE_MAX_WORD_LEN      32

#define MIN_WORD_LENGTH           1

typedef enum EBOOK
{
   CALIBRE_EBOOK_NONE = 0,
   CALIBRE_EBOOK_EPUB,
   CALIBRE_EBOOK_LIT,
   CALIBRE_EBOOK_PDF,
   CALIBRE_EBOOK_TXT,
   CALIBRE_EBOOK_PDB,
   CALIBRE_EBOOK_MOBI,

   NUM_CALIBRE_EBOOK_TYPES
}  EBOOK;

typedef enum CAEXP
{
   CALIBRE_EXPORT_NONE = 0,
   CALIBRE_EXPORT_AUTHOR,
   CALIBRE_EXPORT_AUTHOR_SORT,
   CALIBRE_EXPORT_TITLE,
   CALIBRE_EXPORT_TITLE_SORT,
   CALIBRE_EXPORT_FORMATS,
   CALIBRE_EXPORT_IDS,

   NUM_CALIBRE_EXPORTS
}  CAEXP;

typedef enum SPLIT
{
   CALIBRE_SPLIT_TITLE_AUTHOR = 0,
   CALIBRE_SPLIT_AUTHOR_TITLE,
   CALIBRE_SPLIT_DONE,

   NUM_CALIBRE_SPLITS
}  SPLIT;

typedef enum CASTAT
{
   CALIBRE_STATUS_NONE = 0,
   CALIBRE_STATUS_CVS,
   CALIBRE_STATUS_NEW,
   CALIBRE_STATUS_ADDED,
   CALIBRE_STATUS_BAD,
   CALIBRE_STATUS_DUPLICATE,
   CALIBRE_STATUS_DELETE,

   CALIBRE_STATUS_MOVED,
   CALIBRE_STATUS_COPIED,
   CALIBRE_STATUS_DELETED,

   NUM_CALIBRE_STATS
}  CASTAT;

typedef enum CATA
{
   CALIBRE_AUTHOR = 0,
   CALIBRE_TITLE
}  CATA;

typedef enum CATG
{
   CALIBRE_DB_WORDS = 0,
   CALIBRE_DB_SENTENCE
}  CATG;

//
// Calibre Author & Books list
//
typedef struct CBOOK
{
   CASTAT   tStatus;
   EBOOK    tFormat;
   BOOL     fChecked;
   int      iIndex;
   int      iList;
   int      iId;
   char     cRecord[CALIBRE_MAX_TEXT_LEN+1];
   char    *pcDirPath;
   char    *pcFilePath;
   CBOOK   *pstTitle;
   CBOOK   *pstNext;
   CBOOK   *pstPrev;
}  CBOOK;

typedef struct BLIST
{
   char     cRecord[CALIBRE_RECORD_LEN+1];
   char    *pcBname;
   char    *pcPath;
   int      iPathLength;
}  BLIST;

typedef struct EPUB
{
   EBOOK    tFormat;
   char    *pcFormat;
}  EPUB;

typedef struct CASPLIT
{
   SPLIT    tSplit;
   EBOOK    tFormat;
   CBOOK   *pstAuthor;
   CBOOK   *pstTitle;
   BOOL     fTruncated;
   int      iIndex;
   char    *pcTarget;
   int      iWordNum;
   int      iTotalScore;
   int      iWordScore;
   int      iLetterScore;
   char     cRecord[CALIBRE_MAX_TEXT_LEN+1];    // Raw data : "Series 01-This is the-title - Author First Lastname"
   char     cBuffer[CALIBRE_MAX_TEXT_LEN+1];    // 1 Hyphen : "Series 01 This is the title - Author First Lastname"
   char     cTemp  [CALIBRE_MAX_TEXT_LEN+1];    // Isolated Title or Author
   char     cComp  [CALIBRE_MAX_TEXT_LEN+1];    // Scratchpad for compare
}  CASPLIT;

//
// CalibreMap
//
class CCalibreMap : public CObject
{
public:
            CCalibreMap();
   virtual ~CCalibreMap();

public:
   void     Calibre_FreeDB                (void);
   CBOOK   *Calibre_FindTitleById         (int);
   int      Calibre_ParseDB               (CString *);
   //
   CCalibreMap& CCalibreMap::operator=    (const CCalibreMap&);

private:
   BOOL     db_CopyItem                   (char *, char *, int);
   BOOL     db_ExtractItem                (char *, char *, CAEXP, int);
   int      db_GetItemLength              (char *);
   BOOL     db_LookupAuthor               (char *);
   int      db_ParseExports               (CAEXP, char *);
   int      db_ReIndexAuthors             (CBOOK *);
   int      db_ReIndexTitles              (CBOOK *);
   char    *db_SkipItem                   (char *);
   char    *db_SkipString                 (char *);
//
   CBOOK   *AddAuthor                     (char *);
   CBOOK   *AddTitle                      (char *, CBOOK *, BLIST *, BOOL);
   BOOL     GetFileInfo                   (char *, CString *, CString *, u_int64 *);
   BOOL     IsRecognized                  (CASPLIT *, CATA);
   int      ReadRecord                    (char *, char *, int);
   void     SecsToDate                    (CString *, u_int32);
   void     SecsToTime                    (CString *, u_int32);
   BOOL     SetupBuffer                   (CASPLIT *, char *, int);
   BOOL     ValidateTarget                (CASPLIT *, CATA, CATG);
   int      VerifyWords                   (char *, char *, int, int *, int *);


   //
   CStdioFile     clFile;
   BOOL           fFileOpen;
   int            iAuthor;
   int            iAuthorSort;
   int            iTitle;
   int            iTitleSort;
   int            iFormats;
   int            iIds;
   //
private:
   static CBOOK  *pstAuthors;
   CNvmStorage    clNvm;

public:
   static void MessageToApp      (int, int);
   static void TextMessageToApp  (int, char *);
   static void TextMessageToApp  (int, BOOL, char *);
   static void TextMessageToApp  (int, BOOL, char *, int);

public:
   static int     iNumAuthors;
   static int     iNumTitles;
   static int     iNumDuplicates;
   static int     iNumBadFormat;
   static HWND    tGlobalDlgWnd;
};


#endif // !defined(_CALIBREFILES_H_)
