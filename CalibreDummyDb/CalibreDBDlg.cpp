/*  (c)2011 Patrn.nl
**
**  $Workfile:   CalibreDBDlg.cpp $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29 Aug 2011
**
 *
 *
 *
 *
**/
#include "stdafx.h"
#include "CalibreDB.h"
#include "CalibreDBDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


const char *pcLogon = "CaBooH: CalibreDB simulation";
const char *pcPatrn = "(C)2011:www.patrn.nl";
//
// CAboutDlg dialog used for App About
//
class CAboutDlg : public CDialog
{
public:
   CAboutDlg();

// Dialog Data
   enum { IDD = IDD_ABOUTBOX };

protected:
   virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

// Implementation
protected:
   DECLARE_MESSAGE_MAP()
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// CCalibreDBDlg dialog



//
//  Function:  CCalibreDBDlg()
//  Purpose:   Constructor
//
//  Parms:
//  Returns:
//
CCalibreDBDlg::CCalibreDBDlg(CWnd *pParent) : CDialog(CCalibreDBDlg::IDD, pParent)
{
   m_hIcon  = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
   //
   pclNvm   = (CNvmStorage *) new(CNvmStorage);
   pclMap   = (CCalibreMap *) new(CCalibreMap);
   //
   pclFontTxt = (CAutoFont *)   new(CAutoFont)("Tahoma");
   pclFontTxt->SetHeight(CALIBRE_TXT_FONT_SIZE);
   //
   pclMap->tGlobalDlgWnd = this->GetSafeHwnd();
   //
   iCounter  = 20;
   iExitcode = 0;
   fFreeze   = FALSE;
}

//
//  Function:  DoDataExchange()
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalibreDBDlg::DoDataExchange(CDataExchange* pDX)
{
   CDialog::DoDataExchange(pDX);
   DDX_Control(pDX, IDC_EDIT_DISPLAY,                  clEditList);
}

BEGIN_MESSAGE_MAP(CCalibreDBDlg, CDialog)
   ON_WM_SYSCOMMAND()
   ON_WM_PAINT()
   ON_WM_QUERYDRAGICON()
   ON_WM_TIMER()
   //}}AFX_MSG_MAP
   ON_BN_CLICKED(IDC_CHECKBOX_FREEZE, OnBnClickedCheckboxFreeze)
END_MESSAGE_MAP()


//
//  Function:  OnInitDialog()
//  Purpose:
//
//  Parms:
//  Returns:
//
BOOL CCalibreDBDlg::OnInitDialog()
{
   int      iNum, iIdx, iId;
   CString  clStr, clPath, clCmd, clIds;
   BOOL     fEnabled;
   CBOOK   *pstTitle;

   CDialog::OnInitDialog();

   // Add "About..." menu item to system menu.
   // IDM_ABOUTBOX must be in the system command range.
   //
   ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
   ASSERT(IDM_ABOUTBOX < 0xF000);

   CMenu* pSysMenu = GetSystemMenu(FALSE);
   if (pSysMenu != NULL)
   {
      CString strAboutMenu;
      strAboutMenu.LoadString(IDS_ABOUTBOX);
      if (!strAboutMenu.IsEmpty())
      {
         pSysMenu->AppendMenu(MF_SEPARATOR);
         pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
      }
   }

   // Set the icon for this dialog.  The framework does this automatically
   //  when the application's main window is not a dialog
   SetIcon(m_hIcon, TRUE);       // Set big icon
   SetIcon(m_hIcon, FALSE);      // Set small icon
   //
   clStr.Format(_T("[%s] - %s (%s) - %s" CRLF CRLF), CVB_GetTimeDateStamp(), pcLogon, CALIBREDB_VERSION, pcPatrn);
   StatusAddText(clStr.GetBuffer());
   //
   clCmdInfo.GetCommandline(&clCmd);
   clStr.Format(_T("  Commandline from CaBooh: [%s]" CRLF), clCmd.GetBuffer());
   StatusAddText(clStr.GetBuffer());
   //
   pclNvm->NvmGet(NVM_INITIALIZED, &fEnabled);

   clEditList.SetFont(pclFontTxt);
   clEditList.SetBackColor(WHITE);
   clEditList.SetTextColor(BLUE);

   if(fEnabled)
   {
      // NVM was bad: defaults were reloaded !
      clStr.Format(_T("  Configuration has been reset !" CRLF));
      StatusAddText(clStr.GetBuffer());
   }
   //
   // Load database from *.cvs
   //                     1         2         3         4         5         6         7         8         9
   //           0....5....0....5....0....5....0....5....0....5....0....5....0....5....0....5....0....5....0.
   // Cmdline: " remove --library-path "F:\eBooks\Catalog\CalibreDB-EN.csv" 104,2070,4153"
   //
   // Cmd line option:
   // Cmdline: " remove --library-path \"D:\Peter\Proj\Calibre\Doc\CalibreDB-EN.csv\" 104,2070,4153"
   // Cmdline: " remove --library-path \"F:\eBooks\Catalog\CalibreDB-EN.csv\" 104,2070,4153"
   //
   iIdx = GetLibraryPath(&clCmd, &clPath);
   if(iIdx)
   {
      clStr.Format(_T("  Reading CVS database %s........" CRLF), clPath.GetBuffer());
      StatusAddText(clStr.GetBuffer());
      iNum = pclMap->Calibre_ParseDB(&clPath);
      clStr.Format(_T("  Database CSV contained %d Titles." CRLF), iNum);
      StatusAddText(clStr.GetBuffer());
      //
      // Retrieve the record IDs from the CMD line and lookup the ID in the CSV database.
      //
      iIdx += clPath.GetLength() + 1;        // Skip pathname
      iIdx  = clCmd.GetLength() - iIdx;      // Calculate offset to IDs from end backwards
      //
      clIds = clCmd.Right(iIdx); 
      //
      do
      {
         iId = atoi((LPCTSTR)clIds);
         GetToken(clIds, ",");
         pstTitle = pclMap->Calibre_FindTitleById(iId);
         if(pstTitle)
         {
            clStr.Format(_T("  %4d %s" CRLF), iId, pstTitle->cRecord);
         }
         else
         {                      
            clStr.Format(_T("  %4d TITLE NOT FOUND !" CRLF), iId);
            iExitcode = 3;
         }
         StatusAddText(clStr.GetBuffer());
      }
      while(clIds.GetLength());
   }
   else
   {
      clStr.Format(_T("  No valid CSV database %s" CRLF), clPath.GetBuffer());
      StatusAddText(clStr.GetBuffer());
      iExitcode = 2;
   }
   //
   iDbTimer = (int) SetTimer(1, 500, NULL);
   VERIFY(iDbTimer != 0);
   return TRUE;  // return TRUE  unless you set the focus to a control
}

//
//  Function:  OnSysCommand()
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalibreDBDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
   if ((nID & 0xFFF0) == IDM_ABOUTBOX)
   {
      CAboutDlg dlgAbout;
      dlgAbout.DoModal();
   }
   else
   {
      CDialog::OnSysCommand(nID, lParam);
   }
}

//
//  Function:  OnPaint()
//  Purpose:   Repaint the window
//             If you add a minimize button to your dialog, you will need the code below
//             to draw the icon.  For MFC applications using the document/view model,
//             this is automatically done for you by the framework.
//
//  Parms:
//  Returns:
//
void CCalibreDBDlg::OnPaint()
{
   if (IsIconic())
   {
      CPaintDC dc(this); // device context for painting

      SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

      // Center icon in client rectangle
      int cxIcon = GetSystemMetrics(SM_CXICON);
      int cyIcon = GetSystemMetrics(SM_CYICON);
      CRect rect;
      GetClientRect(&rect);
      int x = (rect.Width() - cxIcon + 1) / 2;
      int y = (rect.Height() - cyIcon + 1) / 2;

      // Draw the icon
      dc.DrawIcon(x, y, m_hIcon);
   }
   else
   {
      CDialog::OnPaint();
   }
}

//
//  Function:  OnQueryDragIcon()
//  Purpose:
//             The system calls this function to obtain the cursor to display while the user drags
//             the minimized window.
//
//  Parms:
//  Returns:
//
HCURSOR CCalibreDBDlg::OnQueryDragIcon()
{
   return static_cast<HCURSOR>(m_hIcon);
}

//
//  Function:  OnOK
//  Purpose:   Exit OK button
//             Return codes are:
//                0 : OKee
//                1 : Generic error
//                2 : CVS database not found
//                3 : ID not found
//
//  Parms:
//  Returns:
//
void CCalibreDBDlg::OnOK()
{
   pclMap->Calibre_FreeDB();
   PostQuitMessage(iExitcode);
}

//
//  Function:  OnTimer
//  Purpose:   Setup a solver timer for misc usage
//             Return codes are:
//                0 : OKee
//                1 : Generic error
//                2 : CVS database not found
//                3 : ID not found
//
//  Parms:
//  Returns:
//
void CCalibreDBDlg::OnTimer(UINT nIDEvent)
{

   if(fFreeze)
   {
      // Do not exit after timeout
   }
   else
   {
      if(--iCounter == 0) 
      {
         pclMap->Calibre_FreeDB();
         PostQuitMessage(iExitcode);
      }
   }
   CWnd::OnTimer(nIDEvent);
}

//
//  Function:  OnBnClickedCheckboxFreeze
//  Purpose:   Checkbox Freeze (cancel Timer timeout)
//
//  Parms:     Commandline, dest
//  Returns:   Index to path
//
void CCalibreDBDlg::OnBnClickedCheckboxFreeze()
{
   CButton *pclCheckbox = (CButton *)GetDlgItem(IDC_CHECKBOX_FREEZE);

   if (pclCheckbox->GetCheck() == BST_CHECKED) fFreeze = TRUE;
   else                                        fFreeze = FALSE;
}

//
//  Function:  GetLibraryPath
//  Purpose:   Get pathname from commandline
//
//  Parms:     Commandline, dest
//  Returns:   Index to path
//
int CCalibreDBDlg::GetLibraryPath(CString *pclCmd, CString *pclPath)
{
   BOOL        fParse=TRUE, fDelim=FALSE;
   int         iIdx, iRet=0;
   const char *pcLib = "--library-path ";
   //
   // Cmdline: " remove --library-path 'D:\Peter\Proj\Calibre\Doc\CalibreDB-IMPORT.csv' 123,332,456,665,45"
   //
   iIdx = pclCmd->Find(pcLib);
   if(iIdx >= 0)
   {
      iIdx += (int) strlen(pcLib);
      pclPath->Empty();
      while(fParse)
      {
         switch(pclCmd->GetAt(iIdx))
         {
            case '\'':
            case '\"':
               if(fDelim == FALSE) 
               {
                  // Start of pathname
                  iRet   = iIdx+1;
                  fDelim = TRUE;
               }
               else
               {
                  // End of pathname
                  fParse = FALSE;
               }
               break;

            default:
               if(fDelim == TRUE) 
               {
                  // Copy pathname...
                  *pclPath += pclCmd->GetAt(iIdx);
               }
               break;

            case 0:
               // bad formatted pathname
               fParse = FALSE;
               break;
         }
         iIdx++;
      }
   }
   return(iRet);
}

//
//  Function:  GetToken
//  Purpose:   Get next numeric, skip next one until delimiter
//
//  Parms:     String, delimiter
//  Returns:   String 
//
CString CCalibreDBDlg::GetToken(CString& clStr, LPCTSTR cChar)
{
   int      iPos;
   CString  clToken;

   //pwjh clToken = clStr.Left(0);
   iPos    = clStr.Find(cChar);
   if(iPos >=0)
   {
      clToken = clStr.Left(iPos);
      clStr   = clStr.Mid(iPos+1);
   }
   else
   {
      clStr.Empty();
      clToken.Empty();
   }
   //
   return(clToken);
}

//
//  Function:  StatusAddText
//  Purpose:   Add text to the status window
//
//  Parms:     Text^
//  Returns:   void
//
void CCalibreDBDlg::StatusAddText(char *pcText)
{
   CString  clStr;
   int      iLines;

   clEditList.GetWindowText(clStr);
   clStr += pcText;

   iLines = clEditList.GetLineCount();

   clEditList.SetWindowText(clStr);
   clEditList.LineScroll(iLines);
}


