/*  (c)2011 Patrn.nl
**
**  $Workfile:   CalibreDBDlg.h $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29 Aug 2011
**
 *
 *
 *
 *
**/
#pragma once

#include "CalibreFiles.h"

// CCalibreDBDlg dialog
class CCalibreDBDlg : public CDialog
{
public:
   CCalibreDBDlg                    (CWnd* pParent = NULL);
   //
   enum { IDD = IDD_CALIBREDB_DIALOG };
   //
protected:
   virtual void      DoDataExchange (CDataExchange* pDX);   // DDX/DDV support

private:
   void              StatusAddText  (char *);
   int               GetLibraryPath (CString *, CString *);
   CString           GetToken       (CString&, LPCTSTR);
   //
   HICON m_hIcon;
   // Generated message map functions
   virtual BOOL      OnInitDialog   ();
   afx_msg void      OnSysCommand   (UINT nID, LPARAM lParam);
   afx_msg void      OnPaint        ();
   afx_msg void      OnOK           ();
   afx_msg HCURSOR   OnQueryDragIcon();
   afx_msg void      OnTimer        (UINT nIDEvent);
   //
   afx_msg void      OnBnClickedCheckboxFreeze();
   //
   DECLARE_MESSAGE_MAP()
private:
   CMyCommandLineInfo   clCmdInfo;
   CNvmStorage         *pclNvm;
   CCalibreMap         *pclMap;
   CReadOnlyEdit        clEditList;
   CAutoFont           *pclFontTxt;
   int                  iDbTimer;
   int                  iCounter;
   int                  iExitcode;
   BOOL                 fFreeze;
};
