/*  (c)2011 Patrn.nl
**
**  $Workfile:   CalibreDB.cpp $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       29 Aug 2011
**
 *
 *
 *
 *
**/
#include "stdafx.h"
#include "CalibreDB.h"
#include "CalibreDBDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCalibreDBApp
BEGIN_MESSAGE_MAP(CCalibreDBApp, CWinApp)
   ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


CCalibreDBApp theApp;

//
//  Function:  CCalibreDBApp()
//  Purpose:   Constructor
//
//  Parms:
//  Returns:
//
CCalibreDBApp::CCalibreDBApp()
{
   EnableHtmlHelp();
}


//
//  Function:  InitInstance()
//  Purpose:
//
//  Parms:
//  Returns:
//
BOOL CCalibreDBApp::InitInstance()
{
   int      iNr;
   CString  clStr;
   BOOL     fNvmOKee = TRUE;
   CWnd    *pclWnd   = CWnd::GetActiveWindow();

   //
   // InitCommonControls() is required on Windows XP if an application
   // manifest specifies use of ComCtl32.dll version 6 or later to enable
   // visual styles.  Otherwise, any window creation will fail.
   //
   InitCommonControls();
   CWinApp::InitInstance();

   ParseCommandLine(clCmdInfo);
   //
   // Check the archive first
   //
   clStorage = CABOOH_CONFIG;
   iNr       = clNvm.NvmCheckArchive(&clStorage);
   //
   if(iNr == NUM_WNVMS)
   {
      // Archive is OKee
      if(clNvm.NvmRead(&clStorage) == FALSE)
      {
         // Archive is bad : take new one
         fNvmOKee = FALSE;
      }
      else
      {
         // NVM loaded correctly: check if we rolled the rev.
         clNvm.NvmGet(NVM_DB_VERSION, &clStr);
         fNvmOKee = (clStr.Compare(CALIBREDB_DB_VERSION) == 0);
      }
   }
   else
   {
      // Archive is bad : take new one
      fNvmOKee = FALSE;
   }
   if(!fNvmOKee)
   {
      NewNvm();
   }
   //
   CCalibreDBDlg clDlg;
   m_pMainWnd      = &clDlg;
   INT_PTR iResult = clDlg.DoModal();
   //
   switch(iResult)
   {
      case IDOK:
         clNvm.NvmPut(NVM_INITIALIZED, "0");
         clNvm.NvmWrite(&clStorage);      // Save settings
         break;

      case IDCANCEL:
         break;
   }

   //
   // Since the dialog has been closed, return FALSE so that we exit the
   //  application, rather than start the application's message pump.
   //
   return FALSE;
}

/* ======   Local   Functions separator ===========================================
void ____Class_Methods_NVM____(){}
==============================================================================*/

//
//  Function:   NewNvm
//  Purpose:    Init a new NVM
//
//  Parms:
//  Returns:
//
BOOL CCalibreDBApp::NewNvm()
{
   clNvm.NvmPut(NVM_VERSION,                    CALIBREDB_VERSION);     //   NVM_VERSION
   clNvm.NvmPut(NVM_INITIALIZED,                "1");                   //   NVM_INITIALIZED
   clNvm.NvmPut(NVM_NAME_DATABASE_FILE,         "");                    //   NVM_NAME_DATABASE_FILE
   clNvm.NvmPut(NVM_NAME_DATABASE_DIR,          "");                    //   NVM_NAME_DATABASE_DIR
   clNvm.NvmPut(NVM_AUTHOR_THOLD,               "40");                  //   NVM_AUTHOR_THOLD
   clNvm.NvmPut(NVM_TITLE_THOLD,                "40");                  //   NVM_TITLE_THOLD
   clNvm.NvmPut(NVM_WORD_THOLD,                 "90");                  //   NVM_WORD_THOLD
   clNvm.NvmPut(NVM_LETTER_THOLD,               "90");                  //   NVM_LETTER_THOLD
   clNvm.NvmPut(NVM_CALIBRE_QUIETMODE,          "1");                   //   NVM_CALIBRE_QUIETMODE
   clNvm.NvmPut(NVM_DB_VERSION,                 CALIBREDB_DB_VERSION);  //   NVM_DB_VERSION
   //
   return(TRUE);
}
