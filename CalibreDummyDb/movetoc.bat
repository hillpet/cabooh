@echo off
echo.
echo Distribute calibredbdummy.exe
echo.

if exist Release\CalibreDB.exe goto copydummy
echo CalibreDB.exe not found
pause
goto end

:copydummy
echo copy to Release...
copy /Y Release\CalibreDB.exe Release\calibredbdummy.exe >NUL
echo copy to Debug...
copy /Y Release\CalibreDB.exe Debug\calibredbdummy.exe >NUL
echo copy to WinApps...
copy /Y Release\CalibreDB.exe C:\WinApps\Calibre\calibredbdummy.exe >NUL
echo.
echo.
pause

:end
