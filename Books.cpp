/*  (c) Copyright:  2011  CVB, Confidential Data
**
**  $Workfile:          Books.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       1 Sep 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

//
// External globals
//
extern const char *pcAuthorSubstitute;
extern const char *pcTitleSubstitute;

//
// static members of CBook
// 
int            CBOOK::iInstance;
CMyString     *CBOOK::pclRaw;
int            CBOOK::iCurLib;
CString       *CBOOK::pclLibrary[];
//
CLog          *CBOOK::pclLog;
CNvmStorage   *CBOOK::pclNvm;
BOOL           CBOOK::fFileOpen;
CStdioFile    *CBOOK::pclInFile;
TLIST         *CBOOK::pstAltSolList;
CBOOK         *CBOOK::pclAuthorList;
int            CBOOK::iWordLength;
int            CBOOK::iNumAuthors;
int            CBOOK::iNumTitles;
int            CBOOK::iNumDuplicates;
int            CBOOK::iNumBadFormat;
int            CBOOK::iCvsAuthor;    
int            CBOOK::iCvsAuthorSort;
int            CBOOK::iCvsTitle;     
int            CBOOK::iCvsTitleSort; 
int            CBOOK::iCvsFormats;   
int            CBOOK::iCvsIds;       
int            CBOOK::iCvsBookSize;  
//
extern const char *pcLogon;
extern const char *pcPatrn;

extern const EPUB stBookTypes[]=
{
   { CALIBRE_EBOOK_NONE, "book" },
   { CALIBRE_EBOOK_EPUB, "epub" },
   { CALIBRE_EBOOK_LIT,  "lit"  },
   { CALIBRE_EBOOK_PDF,  "pdf"  },
   { CALIBRE_EBOOK_TXT,  "txt"  },
   { CALIBRE_EBOOK_PDB,  "pdb"  },
   { CALIBRE_EBOOK_MOBI, "mobi" },
   { CALIBRE_EBOOK_DOC,  "doc"  },
   { CALIBRE_EBOOK_RTF,  "rtf"  }
};
extern const int iNumBookTypes = sizeof(stBookTypes)/sizeof(EPUB);

// 
//  Function:  CBook()
//  Purpose:   Constructor
//  Parms:
//  Returns:
//
CBook::CBook()
{
   if(iInstance == 0)
   {
      //
      // These should only happen once
      //
      fFileOpen      = FALSE;
      pclInFile      = new(CStdioFile);
      pclNvm         = new(CNvmStorage);
      pclLog         = new(CLog);
      pclAuthorList  = NULL;
      pstAltSolList  = NULL;
      pclRaw         = new(CMyString);
      //
      iCurLib        = 0;
      iWordLength    = MIN_WORD_LENGTH;
      iCvsAuthor     = 0;
      iCvsAuthorSort = 0;
      iCvsTitle      = 0;
      iCvsTitleSort  = 0;
      iCvsFormats    = 0;
      iCvsIds        = 0;
      iCvsBookSize   = 0;
      //
      for(int i=0; i<CALIBRE_MAXLIBS; i++)
      {
         pclLibrary[i] = new(CString);
      }
   }
   //
   pclPrev           = NULL;
   pclNext           = NULL;
   pclName           = new(CMyString);
   pclFile           = new(CMyString);
   pclDir            = new(CMyString);
   pclXref           = NULL;
   pstCandidates     = NULL;
   tAuthorTitle      = CALIBRE_AUTHOR;
   iSplit            = 0;
   tSplit            = CALIBRE_SPLIT_NONE;
   tStatus           = CALIBRE_STATUS_NONE;
   tFormat           = CALIBRE_EBOOK_NONE;
   tAllFormats       = CALIBRE_EBOOK_NONE;
   fBoxChecked       = FALSE;
   iBoxIndex         = -1;
   iWildIndex        = -1;
   iLibNum           = 0;
   iCalibreId        = 0;
   iCalibreSize      = 0;
   iScore            = 0;
   iScoreLetter      = 0;
   iScoreWord        = 0;
   //
   iInstance++;
}

//
//  Function:  ~CBook()
//  Purpose:   Destructor
//  Parms:
//  Returns:
//
CBook::~CBook()
{
   iInstance--;
   //
   if(iInstance == 0)
   {
      DB_FreeDB();
      delete(pclInFile);
      delete(pclNvm);
      delete(pclLog);
      delete(pclRaw);
      for(int i=0; i<CALIBRE_MAXLIBS; i++)
      {
         delete(pclLibrary[i]);
      }
   }
   //
   //
   // Discard all candidates
   //
   DB_FreeCandidates(pstCandidates);
   //
   if(pclName) delete(pclName);
   if(pclFile) delete(pclFile);
   if(pclDir)  delete(pclDir);
}

//
//  Function:  operator =
//  Purpose:   Copy class data
//  Parms:     Source data
//  Returns:   Copied class
//
//
CBook& CBook::operator=(const CBook& clBook)
{
   if (this != &clBook)
   {
      //this->pclPrev       = clBook.pclPrev;
      //this->pclNext       = clBook.pclNext;
      //this->pclXref       = clBook.pclXref;
      //this->pstAltSolList = clBook.pstAltSolList;
      //this->pstCandidates = clBook.pstCandidates;
      this->tAuthorTitle  = clBook.tAuthorTitle;
      this->tSplit        = clBook.tSplit;
      this->iSplit        = clBook.iSplit;
      this->tStatus       = clBook.tStatus;
      this->tFormat       = clBook.tFormat;
      this->tAllFormats   = clBook.tAllFormats;
      this->fBoxChecked   = clBook.fBoxChecked;
      this->iBoxIndex     = clBook.iBoxIndex;
      this->iWildIndex    = clBook.iWildIndex;
      this->iCalibreId    = clBook.iCalibreId;
      this->iCalibreSize  = clBook.iCalibreSize;
      this->iLibNum       = clBook.iLibNum;
      this->iScore        = clBook.iScore;
      this->iScoreLetter  = clBook.iScoreLetter;
      this->iScoreWord    = clBook.iScoreWord;
      // Deep-Copy the CMyString members
     *this->pclDir        = *clBook.pclDir;
     *this->pclFile       = *clBook.pclFile;
     *this->pclName       = *clBook.pclName;
   }
   return(*this);
}


/* ======   Local   Functions separator ===========================================
void ____Public_Methods____(){}
==============================================================================*/

//
//  Function:  DB_AddAuthor
//  Purpose:   Add this author to the linked list
//
//  Parms:     (this) pclName
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddAuthor(void)
{
   return( DB_AddAuthor(pclName->GetBuffer(), FALSE) );
}

//
//  Function:  DB_AddAuthor
//  Purpose:   Add this author to the linked list
//
//  Parms:     Book
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddAuthor(char *pcAuthor)
{
   return( DB_AddAuthor(pcAuthor, FALSE) );
}

//
//  Function:  DB_AddAuthor
//  Purpose:   Add this author to the linked list
//
//  Parms:     CString book
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddAuthor(CString *pclAuthor)
{
   return( DB_AddAuthor(pclAuthor->GetBuffer(), FALSE) );
}

//
//  Function:  DB_AddAuthor
//  Purpose:   Add this author to the linked list
//
//  Parms:     Bookname, Add duplicated
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddAuthor(CString *pclAuthor, BOOL fAddDups)
{
   return( DB_AddAuthor(pclAuthor->GetBuffer(), fAddDups) );
}

//
//  Function:  DB_AddAuthor
//  Purpose:   Add this author to the linked list
//
//  Parms:     Author, add duplicates
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddAuthor(char *pcAuthor, BOOL fAddDups)
{
   int      iCmp;
   CBOOK   *pclNew, *pclPrv=NULL, *pclA=pclAuthorList;

   pclNew = new(CBOOK);
   //
  *pclNew->pclName      = pcAuthor;
   pclNew->tAuthorTitle = CALIBRE_AUTHOR;
   //
   while(pclA)
   {
      iCmp = pclA->pclName->CompareNoCase(pcAuthor);
      if(iCmp == 0)
      {
         // This one already exists: use it if required
         if(fAddDups)
         {
            iCmp = 1;
            pclNew->tStatus |= CALIBRE_STATUS_DUP;
         }
         else
         {
            delete(pclNew);
            return(pclA);
         }
      }
      if(iCmp > 0)
      {
         //
         // This is the insertion point (new and/or duplicates)
         //
         if(pclPrv)  pclPrv->pclNext = pclNew;
         else        pclAuthorList   = pclNew;
         //
         pclNew->pclNext = pclA;
         pclNew->pclPrev = pclPrv;
         pclA->pclPrev   = pclNew;
         iNumAuthors++;
         break;
      }
      pclPrv = pclA;
      pclA   = pclA->pclNext;
   }
   //
   if(pclAuthorList == NULL)
   {
      // Linked list still empty
      pclAuthorList = pclNew;
   }
   else if(pclA == NULL)
   {
      // Need to add the new one to the end of the list
      if(pclPrv) pclPrv->pclNext = pclNew;
      pclNew->pclPrev = pclPrv;
   }
   //
   // During the search for this author in the authors known list, we have build up a
   // linked list of other potential candidates. Save this list for later retrieval, 
   // then empty the current list
   //
   pclNew->pstCandidates = pstAltSolList;
   pstAltSolList         = NULL;
   return(pclNew);
}

//
//  Function:  DB_AddTitle
//  Purpose:   Add this title to the linked list of this author
//
//  Parms:     Title, Add Duplicates YESNO
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddTitle(char *pcTitle, BOOL fAddDups)
{
   return( DB_AddTitle(pcTitle, this, fAddDups) );
}

//
//  Function:  DB_AddTitle
//  Purpose:   Add this title to the linked list of this author
//
//  Parms:     Title, Add Duplicates YESNO
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddTitle(CString *pclTitle, BOOL fAddDups)
{
   return( DB_AddTitle(pclTitle->GetBuffer(), this, fAddDups) );
}

//
//  Function:  DB_AddTitle
//  Purpose:   Add this title to the linked list of the author
//
//  Parms:     Title, Author, Add Duplicates YESNO
//  Returns:   CBOOK  *
//
CBOOK *CBook::DB_AddTitle(CString *pclTitle, CBOOK *pclA, BOOL fAddDups)
{
   return( DB_AddTitle(pclTitle->GetBuffer(), pclA, fAddDups) );
}

//
//  Function:  DB_AddTitle
//  Purpose:   Add this title to the linked list of the author
//
//  Parms:     Title, Author, Add Duplicates YESNO
//  Returns:   The newly added CBOOK
//
CBOOK *CBook::DB_AddTitle(char *pcTitle, CBOOK *pclA, BOOL fAddDups)
{
   int      iCmp;
   CAVAL    tCaval;
   CBOOK   *pclNew, *pclLast=NULL, *pclTop=pclA->pclXref;

   pclNew = new(CBOOK);
   //
  *pclNew = *pclA;
   //
  *pclNew->pclName      = pcTitle;
  *pclNew->pclDir       = *pclDir;
  *pclNew->pclFile      = *pclFile;
   pclNew->pclXref      = pclA;
   pclNew->tAuthorTitle = CALIBRE_TITLE;

   while(pclTop)
   {
      tCaval = DB_ValidateTarget(pclNew, CALIBRE_TITLE, CALIBRE_DB_SENTENCE);
      //
      // Check if we already have this title in the list
      // 
      switch(tCaval)
      {
         case CALIBRE_VAL_PERFECT:
            iCmp = 0;
            break;

         case CALIBRE_VAL_NOMATCH:
            //
            // Mark title new for CSV library titles if not in previous csv libs
            //
            if(iCurLib > 1) pclNew->tStatus |= CALIBRE_STATUS_NEW;
            // FALL THROUGH
         default:
         case CALIBRE_VAL_OKEEISH:
            iCmp = pclTop->pclName->CompareNoCase(pcTitle);
            break;
      }
      //
      if(iCmp == 0)
      {
         // This one already exists: use it if required
         if(fAddDups)
         {
            iCmp = 1;
            pclNew->tStatus |= CALIBRE_STATUS_DUP;
            iNumDuplicates++;
         }
         else
         {
            delete(pclNew);
            return(pclTop);
         }
      }
      //
      // This is the insertion point (new and/or duplicates)
      //
      if(iCmp > 0)
      {
         // Insert Title here
         iNumTitles++;
         if(pclLast)  pclLast->pclNext = pclNew;
         else         pclA->pclXref    = pclNew;
         pclNew->pclNext = pclTop;
         pclTop->pclPrev = pclNew;
         break;
      }
      pclLast = pclTop;
      pclTop  = pclTop->pclNext;
   }
   //
   if(pclA->pclXref == NULL)
   {
      // Linked list still empty
      pclA->pclXref = pclNew;
   }
   else if(pclTop == NULL)
   {
      // Need to add the new one to the end of the list
      if(pclLast) pclLast->pclNext = pclNew;
      pclNew->pclPrev = pclLast;
   }
   //
   // During the search for this title in the authors known list of titles, we have build 
   // up a linked list of other potential candidates. Save this list for later retrieval, 
   // then empty the current list
   //
   pclNew->pstCandidates = pstAltSolList;
   pstAltSolList         = NULL;
   return(pclNew);
}

//
//  Function:  DB_CalibreCsvAddBooks
//  Purpose:   Add all the books in the current linked list marked with tStatus from the database
//
//  Parms:     Actual Calibre Database DIR pathname
//  Returns:   Number added of -1 if fail
//
//  Note:      =======================================================================
//                THIS COMMAND ACTUALLY ADDS THESE BOOKS TO THE CALIBRE DATABASE !!
//             =======================================================================
//             We use the Calibredb.exe tool to handle this maintenance. 
//
//             $ calibredb.exe add --duplicates --library-path x:\aaa\bbb book1, book2, book3, ....
//
int CBook::DB_CalibreCsvAddBooks(CString *pclDbDir, CASTAT tStatus)
{
   BOOL     fExecReal, fDontContinue=TRUE;
   int      iRet, iNum=0;
   DWORD    lExitCode=0;
   HANDLE   hProcess;
   CString  clStr, clShell, clBooks;
   CBOOK   *pclA = pclAuthorList;
   CBOOK   *pclT;

   //
   // We have the option to test the actual command:
   // NVM_CALIBRE_EXECMODE == TRUE  calibredb ...
   //                         FALSE calibredbdummy ...
   //
   // This is a commandline overrule.
   //
   const char *pcCalibreDbCmd;
   const char *pcCalibreReal = "calibredb add --duplicates --library-path \"%s\" \"%s\"";
   const char *pcCalibreFake = "calibredbdummy add --library-path '%s' \"%s\"";


   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Add Books by title to ", LOG_NONE);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclDbDir->GetBuffer(), LOG_CRLF);

   pclNvm->NvmGet(NVM_CALIBRE_EXECMODE, &fExecReal);
   //
   while(fDontContinue)
   {
      if(fExecReal) clStr.Format(_T("You are about to REALLY add titles to\n\n%s\n\n"), pclDbDir->GetBuffer());
      else          clStr.Format(_T("You are about to FAKE add titles to\n\n%s\n\n"), pclDbDir->GetBuffer());
      clStr += "Are you sure ?\n\n";
      clStr += "  <No>:Show books to be added | <Cancel>:Abort";
      //
      iRet = AfxMessageBox(clStr, MB_YESNOCANCEL|MB_ICONEXCLAMATION);
      switch(iRet)
      {
         case IDYES:
            fDontContinue = FALSE;
            break;

         default:
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Abort!", LOG_CRLF);
            return(0);
            break;

         case IDNO:
            pclNvm->NvmGet(NVM_NAME_USER_LIST, &clShell);
            clStr.Format(_T("notepad.exe %s"), clShell);
            hProcess = CVB_ExecuteCommand(&clStr);
            CVB_CheckProcessCompletion(hProcess, TRUE);
            break;
      }
   }
   //
   // Pass xfer to CalibreDB.exe to handle the action
   // iRet = +1 OKee  completion
   // iRet = -1 Error completion
   //
   // fExecReal (_NO_ cmd-line /R) uses CalibreDbDummy.exe to test/simulate Calibre access.
   //
   if(fExecReal)
   {
      CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, "    Using CalibreDb");
      pcCalibreDbCmd = pcCalibreReal;
   }
   else
   {
      CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, "    Using CalibreDbDummy");
      pcCalibreDbCmd = pcCalibreFake;
   }
   //
   iRet = 1;
   while(pclA && (iRet == 1) )
   {
      pclT = pclA->pclXref;
      while(pclT && (iRet == 1) )
      {
         if( (pclT->tStatus & CASTAT_MASK_ATTR) == tStatus)
         {
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Adding ", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclT->pclFile->GetBuffer(), LOG_CRLF);
            //
            // Add this book to the list
            //
            db_AddPathToBook(pclT, &clBooks);
            iNum++;
            clStr.Format(_T("    Adding %s"), clBooks);
            CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
            // Execute the Calibre DB commandline tool for Database maintenance
            //
            //    Test  : clShell.Format(_T("calibredb export --to-dir F:\\eBooks\\TestDest --library-path %s %s"), pclDbDir->GetBuffer(), clIds );
            //    Dummy : clShell.Format(_T("calibredummydb remove --library-path "%s" %s"), pclDbDir->GetBuffer(), clIds);
            //    Real  : clShell.Format(_T("calibredb remove --library-path "%s" %s"), pclDbDir->GetBuffer(), clIds);
            //
            clShell.Format(pcCalibreDbCmd, pclDbDir->GetBuffer(), clBooks);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clShell, LOG_CRLF);
            hProcess = CVB_ExecuteCommand(&clShell);
            iRet     = CVB_CheckProcessCompletion(hProcess, TRUE);
            clBooks.Empty();
            DB_UpdateProgressBar((iNum*CALIBRE_PROGRESS_RES)/iNumTitles);
         }
         pclT = pclT->pclNext;
      }
      pclA = pclA->pclNext;
   }
   //
   // Check if OKee completion
   //
   if(iRet == -1)       lExitCode = 2;
   else if(iRet ==  1)  GetExitCodeProcess(hProcess, &lExitCode);
   //
   switch(lExitCode)
   {
      case 0:
         iRet = iNum;
         clStr.Format(_T("    OK completion!"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;

      default:
      case 1:
         iRet = CALIBREDB_CC_GEN_ERROR;
         clStr.Format(_T("    ERROR completion: Generic error !"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;

      case 2:
         iRet = CALIBREDB_CC_NO_CSV;
         clStr.Format(_T("    ERROR completion: No (CSV) database found !"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;

      case 3:
         iRet = iNum;
         clStr.Format(_T("    OK completion: (But no such ID)"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;
   }
   //
   Sleep(500);
   DB_UpdateProgressBar(0);
   return(iRet);
}

//
//  Function:  DB_CalibreCsvRead
//  Purpose:   Read the Calibre CSV database and create a linked list of all authors
//             found in the Calibre CSV database
//
//  Parms:     CSV Pathname
//  Returns:   Total number of authors Added
//
int CBook::DB_CalibreCsvRead(CString *pclPath)
{
   int      iRet, iSize, iNumAdded=0;
   CString  clStr;
   u_int64  llSize, llSum;

   //
   // Get Author file info first
   //
   if( DB_GetFileInfo(pclPath->GetBuffer(), NULL, NULL, &llSize) == FALSE)
   {
      return(0);
   }
   //
   // Library number 0    contains books from various sources
   // Library number 1..3 are actual Calibre DBs imported from a CSV file
   //
   if(pclAuthorList)
   {
      //
      // We have another Calibre database on board: check if we can discard that one
      //
      iRet = AfxMessageBox("Discard existing Calibre data ?", MB_YESNOCANCEL|MB_ICONEXCLAMATION);
      switch(iRet)
      {
         case IDYES:
            //
            // Throw away previous CSV linked list !
            //
            DB_FreeDB();
            break;

         case IDNO:
         default:
            if(iCurLib >= CALIBRE_MAXLIBS-1)
            {
               AfxMessageBox("No additional Calibre DBs can be added !");
               return(0);
            }
            break;
      }
   }
   //
   // Save library credentials
   //
   iCurLib++;
   *pclLibrary[iCurLib] = *pclPath;
   //
   // Allocate file buffer record
   // Read and update CSV record header
   //
   iSize = DB_ReadRecord(pclPath);
   llSum = (u_int64) iSize;
   //
   // Skip the first 3 bytes from the field header
   //
   iSize = pclRaw->Delete(0, 3);
   //
   iCvsAuthor     = db_ParseExports(CALIBRE_EXPORT_AUTHOR);
   iCvsAuthorSort = db_ParseExports(CALIBRE_EXPORT_AUTHOR_SORT);
   iCvsTitle      = db_ParseExports(CALIBRE_EXPORT_TITLE);
   iCvsTitleSort  = db_ParseExports(CALIBRE_EXPORT_TITLE_SORT);
   iCvsFormats    = db_ParseExports(CALIBRE_EXPORT_FORMATS);
   iCvsIds        = db_ParseExports(CALIBRE_EXPORT_IDS);
   iCvsBookSize   = db_ParseExports(CALIBRE_EXPORT_SIZE);
   //
   if( ((iCvsAuthor==0) && (iCvsAuthorSort==0)) || ((iCvsTitle==0) && (iCvsTitleSort==0)) )
   {
      // We have not detected a correct DB header: abort
      const char *pcMsg = "  Parse CSV Database not possible: DB field IDS not available!";
      CCalCmd::TextMessageToApp(WM_CALIBRE_DB_READY, TRUE, (char *)pcMsg);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcMsg, LOG_CRLF);
   }
   else
   {
      //
      // Read all other records and subtract the authors from them
      //
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
      clStr.Format(_T("   Calibre CSV Database %d [%s]"), iCurLib, pclLibrary[iCurLib]->GetBuffer());
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
      //
      do
      {
         iSize = DB_ReadRecord(pclPath);
         if(iSize > 0)
         {
            //
            // Attempt tp extract the Author from the record and try to match this with
            // authors already in the CSV database
            //
            // ToDo: Unicode chars in pclRaw
            CVB_ReplaceUnicodeChars(pclRaw->GetBuffer());
            llSum += (u_int64)iSize;
            db_LookupAuthors();
         }
         DB_UpdateProgressBar((int)((llSum*CALIBRE_PROGRESS_RES)/llSize));
      }
      while(fFileOpen);
   }
   //
   iNumAdded = DB_ReIndexAuthors(pclAuthorList);
   //
   Sleep(500);
   DB_UpdateProgressBar(0);
   DB_ReadRecord((char *) NULL);

   return(iNumAdded);
}

//
//  Function:  DB_CalibreDeleteById
//  Purpose:   Delete all the titles in the current linked list marked with tStatus from the database
//
//  Parms:     Actual Calibre Database DIR pathname
//  Returns:   Number deleted of -1 if fail
//
//  Note:      =======================================================================
//             THIS COMMAND ACTUALLY REMOVES THESE TITLES FROM THE CALIBRE DATABASE !!
//             =======================================================================
//             We use the Calibredb.exe tool to handle this maintenance. The CVS export needs
//             to contain the ID field to be able to find the correct title to delete:
//
//             $ calibredb.exe remove id-1, id-2, id-3, ... , id-n --library-path x:\aaa\bbb
//
int CBook::DB_CalibreDeleteById(CString *pclDbDir, CASTAT tStatus)
{
   BOOL     fExecReal, fDontContinue=TRUE;
   int      iRet, iNum=0, iNumIds=0;
   DWORD    lExitCode=0;
   char     cBuffer[16];
   HANDLE   hProcess;
   CString  clStr, clShell, clIds;
   CBOOK   *pclA = pclAuthorList;
   CBOOK   *pclT;

   //
   // We have the option to test the actual command:
   // NVM_CALIBRE_EXECMODE == TRUE  calibredb ...
   //                         FALSE calibredbdummy ...
   //
   // This is a commandline overrule.
   //
   const char *pcCalibreDbCmd;
   const char *pcCalibreReal = "calibredb remove --library-path \"%s\" \"%s\"";
   const char *pcCalibreFake = "calibredbdummy remove --library-path '%s' \"%s\"";


   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Delete Books by ID from ", LOG_NONE);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclDbDir, LOG_CRLF);

   if(iCvsIds == 0)
   {
      const char *pcMsg = "  Delete Books by ID not possible: Book-IDS not available!";
      CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, TRUE, (char *)pcMsg);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcMsg, LOG_CRLF);
      return(0);
   }

   pclNvm->NvmGet(NVM_CALIBRE_EXECMODE, &fExecReal);
   //
   while(fDontContinue)
   {
      if(fExecReal) clStr.Format(_T("You are about to REALLY delete titles from\n\n%s\n\n"), pclDbDir->GetBuffer());
      else          clStr.Format(_T("You are about to FAKE delete titles from\n\n%s\n\n"), pclDbDir->GetBuffer());
      clStr += "Are you sure ?\n\n";
      clStr += "  <No>:Show books to be deleted | <Cancel>:Abort";
      //
      iRet = AfxMessageBox(clStr, MB_YESNOCANCEL|MB_ICONEXCLAMATION);
      switch(iRet)
      {
         case IDYES:
            fDontContinue = FALSE;
            break;

         default:
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Abort!", LOG_CRLF);
            return(0);
            break;

         case IDNO:
            pclNvm->NvmGet(NVM_NAME_USER_LIST, &clShell);
            clStr.Format(_T("notepad.exe %s"), clShell);
            hProcess = CVB_ExecuteCommand(&clStr);
            CVB_CheckProcessCompletion(hProcess, TRUE);
            break;
      }
   }
   //
   // Pass xfer to CalibreDB.exe to handle the action
   // iRet = +1 OKee  completion
   // iRet = -1 Error completion
   //
   // fExecReal (_NO_ cmd-line /R) uses CalibreDbDummy.exe to test/simulate Calibre access.
   //
   if(fExecReal)
   {
      CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, "    Using CalibreDb");
      pcCalibreDbCmd = pcCalibreReal;
   }
   else
   {
      CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, "    Using CalibreDbDummy");
      pcCalibreDbCmd = pcCalibreFake;
   }
   //
   iRet = 1;
   while(pclA && (iRet == 1) )
   {
      pclT = pclA->pclXref;
      while(pclT && (iRet == 1) )
      {
         if( (pclT->tStatus & CASTAT_MASK_ATTR) == tStatus)
         {
            if(pclT->iCalibreId)
            {
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Deleting ", LOG_NONE);
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclT->pclRaw->GetBuffer(), LOG_CRLF);
               //
               if(iNumIds++) clIds += ",";
               clIds += _itoa(pclT->iCalibreId, cBuffer, 10);
               iNum++;
               if(iNumIds == 10)
               {
                  clStr.Format(_T("    Deleting IDs %s"), clIds);
                  CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
                  // Execute the Calibre DB commandline tool for Database maintenance
                  //
                  //    Test  : clShell.Format(_T("calibredb export --to-dir F:\\eBooks\\TestDest --library-path %s %s"), pclDbDir->GetBuffer(), clIds );
                  //    Dummy : clShell.Format(_T("calibredummydb remove --library-path "%s" %s"), pclDbDir->GetBuffer(), clIds);
                  //    Real  : clShell.Format(_T("calibredb remove --library-path "%s" %s"), pclDbDir->GetBuffer(), clIds);
                  //
                  clShell.Format(pcCalibreDbCmd, pclDbDir->GetBuffer(), clIds);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clShell, LOG_CRLF);
                  hProcess = CVB_ExecuteCommand(&clShell);
                  iRet     = CVB_CheckProcessCompletion(hProcess, TRUE);
                  iNumIds  = 0;
                  clIds.Empty();
               }
               DB_UpdateProgressBar((iNum*CALIBRE_PROGRESS_RES)/iNumTitles);
            }
            else if(pclT->tStatus & CALIBRE_STATUS_CSV)
            {
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Delete by ID:[", LOG_NONE);
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclT->pclRaw, LOG_NONE);
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "] not possible: Book-Id=0! ", LOG_CRLF);
            }
            else
            {
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Delete by ID:[", LOG_NONE);
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclT->pclRaw, LOG_NONE);
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "]: not available in CSV Database !", LOG_CRLF);
               lExitCode = 2;
               iRet      = 2;
            }
         }
         pclT = pclT->pclNext;
      }
      pclA = pclA->pclNext;
   }
   //
   // Execute last ones too
   //
   if(iNumIds && (iRet == 1) )
   {
      clStr.Format(_T("    Deleting IDs %s"), clIds);
      CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
      //
      clShell.Format(pcCalibreDbCmd, pclDbDir->GetBuffer(), clIds);
      //
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clShell, LOG_CRLF);
      //
      hProcess = CVB_ExecuteCommand(&clShell);
      iRet     = CVB_CheckProcessCompletion(hProcess, TRUE);
   }
   //
   // Check if OKee completion
   //
   if(iRet == -1)  lExitCode = 2;
   else 
   {
      if(iRet ==  1)  GetExitCodeProcess(hProcess, &lExitCode);
   }
   //
   switch(lExitCode)
   {
      case 0:
         iRet = iNum;
         clStr.Format(_T("    OK completion!"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;

      default:
      case 1:
         iRet = CALIBREDB_CC_GEN_ERROR;
         clStr.Format(_T("    ERROR completion: Generic error !"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;

      case 2:
         iRet = CALIBREDB_CC_NO_CSV;
         clStr.Format(_T("    ERROR completion: No (CSV) database found !"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;

      case 3:
         iRet = CALIBREDB_CC_NO_IDS;
         clStr.Format(_T("    ERROR completion: No such ID !"));
         CCalCmd::TextMessageToApp(WM_CALIBRE_MESSAGE, clStr.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         break;
   }
   //
   DB_UpdateProgressBar(0);
   return(iRet);
}

//
//  Function:  DB_CalibreCsvExport
//  Purpose:   Export all the books in the database to a CSV file
//
//  Parms:     ActualCalibre DB pathname
//  Returns:
//
//  Note:      =======================================================================
//             THIS COMMAND ACTUALLY ACCESSES THE CALIBRE DATABASE !!
//             =======================================================================
//             We use the Calibredb.exe tool to handle this maintenance. 
//
//             calibredb.exe catalog  f:\dir\destination.csv --library-path= f:\inputfolder --fields="...." --sort_by=author_sort
//
int CBook::DB_CalibreCsvExport(CString *pclIn, CString *pclOut)
{
   BOOL     fExecReal, fDontContinue=TRUE;
   int      iRet;
   HANDLE   hProcess;
   CString  clStr, clShell;
   //
   const char *pcFld = "--fields=title,title_sort,formats,authors,author_sort,id,size";
   const char *pcLib = "--library-path";
   const char *pcSrt = "--sort-by=author_sort";

   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Import all Books from ", LOG_NONE);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclIn, LOG_CRLF);

   pclNvm->NvmGet(NVM_CALIBRE_EXECMODE, &fExecReal);

   while(fDontContinue)
   {
      clStr.Format(_T("About to export all titles as CSV\n\n  from:%s\n\n  into:%s\n\n"), pclIn->GetBuffer(), pclOut->GetBuffer());
      clStr += "Are you sure ?\n\n";
      //
      iRet = AfxMessageBox(clStr, MB_YESNOCANCEL|MB_ICONEXCLAMATION);
      switch(iRet)
      {
         case IDYES:
            fDontContinue = FALSE;
            break;

         default:
            return(0);
            break;
      }
   }
   //
   // To run the DIR command on the Calibre database content:
   //
   clStr = *pclOut + ".txt";
   clShell.Format(_T("  /C dir /s %s >%s"), *pclIn, clStr);
   CCalCmd::TextMessageToApp(WM_CALIBRE_DB_READY, TRUE, "  Write DIR info: cmd");
   CCalCmd::TextMessageToApp(WM_CALIBRE_DB_READY, TRUE, clShell.GetBuffer());
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clShell, LOG_CRLF);
   CVB_ExecuteShell("cmd", clShell.GetBuffer());
   //
   // Let CalibreDB handle the execution
   //
   if(fExecReal)
   {
      clShell.Format(_T("calibredb catalog \"%s\" \"%s\"=\"%s\" \"%s\" \"%s\""), pclOut->GetBuffer(), pcLib, pclIn->GetBuffer(), pcFld, pcSrt);
   }
   else
   {
      clShell.Format(_T("calibredbdummy catalog \"%s\" \"%s\"=\"%s\" \"%s\" \"%s\""), pclOut->GetBuffer(), pcLib, pclIn->GetBuffer(), pcFld, pcSrt);
   }
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clShell, LOG_CRLF);
   hProcess = CVB_ExecuteCommand(&clShell);
   iRet     = CVB_CheckProcessCompletion(hProcess, TRUE);

   DB_UpdateProgressBar(0);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Done!", LOG_CRLF);
   return(iRet);
}

//
//  Function:  DB_CheckMemory
//  Purpose:   Check the memorypool using AFX calls
//
//  Parms:
//  Returns:   0=Bad
//
BOOL CBook::DB_CheckMemory(void)
{
   BOOL fCc;

   fCc = AfxCheckMemory();
   if(!fCc)
   {
      AfxMessageBox("Memory Check problem !");
   }
   return(fCc);
}

//
//  Function:  DB_CleanupDB
//  Purpose:   Cleanup the current Calibre CSV databasefrom deleted books
//
//  Parms:
//  Returns:
//
void CBook::DB_CleanupDB(void)
{
   CBOOK   *pclA=pclAuthorList;
   CBOOK   *pclTmp;

   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Cleanup deleted books:", LOG_CRLF);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
   //
   while(pclA)
   {
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   A:", LOG_NONE);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclA->pclName, LOG_NONE);

      if( (pclA->tStatus & CALIBRE_STATUS_DELETE) || (pclA->tStatus & CALIBRE_STATUS_DELETED) )
      {
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, ":Deleted]", LOG_CRLF);
         pclA->pclXref = db_CleanupBooks(pclA->pclXref, TRUE);
         // take care of next bookptr
         if(pclA->pclPrev) pclA->pclPrev->pclNext = pclA->pclNext;
         else              pclAuthorList          = pclA->pclNext;
         // take care of prev bookptr
         if(pclA->pclNext) pclA->pclNext->pclPrev = pclA->pclPrev;
         //
         // Remove this entry permamently
         //
         pclTmp   = pclA;
         pclA     = pclA->pclNext;
         delete(pclTmp);
       }
       else
       {
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "]", LOG_CRLF);
         pclA->pclXref = db_CleanupBooks(pclA->pclXref, FALSE);
         pclA          = pclA->pclNext;
      }
   }
   DB_ReIndexAuthors();
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Cleanup deleted done.", LOG_CRLF);
}

//
//  Function:  DB_FreeAllCandidates
//  Purpose:   Free All current Calibre CSV database
//
//  Parms:
//  Returns:
//
void CBook::DB_FreeAllCandidates(void)
{
   //
   // Free the candidate lists
   //
   DB_FreeCandidates(pstAltSolList);
   pstAltSolList = NULL;
   DB_FreeCandidates(pstCandidates);
   pstCandidates = NULL;
}

//
//  Function:  DB_FreeCandidates
//  Purpose:   Free the linked list of potential DB candidates for this title
//
//  Parms:     Current book list
//  Returns:   number freed
//
int CBook::DB_FreeCandidates(TLIST *pstCand)
{
   int      iNr=0;
   TLIST   *pstTmp;

   while(pstCand)
   {
      //ToDo if(pstCand->iScore == 0xfeeefeee)
      //ToDo {
      //ToDo    pclLog->LOG_Write(CALIBRE_LOG_ERRORS, "Error FreeCandidates() at 0x", (u_int32)&pstCand, LOG_CRLF);
      //ToDo    break;
      //ToDo }

      pstTmp = pstCand->pstNext;
      delete(pstCand);
      pstCand = pstTmp;
      iNr++;
   }
   return(iNr);
}

//
//  Function:  DB_FreeDB
//  Purpose:   Free the current Calibre CSV database
//
//  Parms:
//  Returns:
//
void CBook::DB_FreeDB(void)
{
   CBOOK   *pclA = pclAuthorList;
   CBOOK   *pclT, *pclTmpT, *pclTmpA;
   //
   // Free the candidate list
   //
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Cleanup A-T list:", LOG_CRLF);
   //
   DB_FreeAllCandidates();
   //
   while(pclA)
   {
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   A:", LOG_NONE);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclA->pclName, LOG_CRLF);
      pclT = pclA->pclXref;
      while(pclT)
      {
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "      T:", LOG_NONE);
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclT->pclName, LOG_CRLF);
         pclTmpT = pclT;
         pclT    = pclT->pclNext;
         delete(pclTmpT);
      }
      pclTmpA = pclA;
      pclA    = pclA->pclNext;
      delete(pclTmpA);
   }
   //
   // Clear all the library names
   //
   for(int i=0; i<CALIBRE_MAXLIBS; i++)
   {
      pclLibrary[i]->Empty();
   }
   iCurLib        = 0;
   pclAuthorList  = NULL;
   iNumAuthors    = iNumTitles = iNumBadFormat = iNumDuplicates = 0;
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Cleanup done.", LOG_CRLF);
}

//
//  Function:   DB_GetAuthorInfo
//  Purpose:    Retrieve an author from the list
//
//  Parms:      Index 0..?, CString *
//  Returns:    Struct Author^  or NULL if not found
//
CBOOK *CBook::DB_GetAuthorInfo(int iIndex, CString *pclStr)
{
   const char *pcStatus;
   CBOOK      *pclA = pclAuthorList;

   while(pclA)
   {
      if(pclA->iBoxIndex == iIndex)
      {
         if((pclA->tStatus & CALIBRE_STATUS_DELETE) || (pclA->tStatus & CALIBRE_STATUS_DELETED)) pclA = NULL;
         break;
      }
      pclA = pclA->pclNext;
   }
   //
   // Pass Author status info if required
   //
   if(pclStr && pclA)
   {
      pcStatus = db_GetBookStatus(pclA);
      *pclStr  = pcStatus;
   }
   return(pclA);
}

//
//  Function:   DB_GetAuthorList
//  Purpose:    Retrieve the author linked list TOP
//
//  Parms:
//  Returns:    Author TOP^  or NULL if not found
//
CBOOK *CBook::DB_GetAuthorList(void)
{
   return(pclAuthorList);
}

//
//  Function:   DB_GetAsciiBookFormat
//  Purpose:    Get this book eBook type
//
//  Parms:      EBOOK
//  Returns:    pcFormat
//
char *CBook::DB_GetAsciiBookFormat(EBOOK tFormat)
{
   int   i;
   char *pcFormat=stBookTypes[0].pcFormat;
   //

   for(i=0; i<iNumBookTypes; i++)
   {
      if(stBookTypes[i].tFormat == tFormat)
      {
         pcFormat = stBookTypes[i].pcFormat;
         break;
      }
   }
   return(pcFormat);
}

//
//  Function:   DB_GetBinaryBookFormat
//  Purpose:    Translate this book eBook format
//
//  Parms:      CString *clFormat
//  Returns:    EBOOK tFormat
//
EBOOK CBook::DB_GetBinaryBookFormat(CString *pclFormat)
{
   for(int j=0; j<iNumBookTypes; j++)
   {
      if(pclFormat->CompareNoCase(stBookTypes[j].pcFormat) == 0) 
      {
         return(stBookTypes[j].tFormat);
      }
   }
   return(CALIBRE_EBOOK_NONE);
}

//
//  Function:  DB_GetFileInfo
//  Purpose:   Get misc file info:
//                Exists YESNO
//                Timestamp
//                Size
//  Parms:     pcPath, pclDate, pclTime, llSize
//  Returns:   TRUE if name is OKee
//
BOOL CBook::DB_GetFileInfo(char *pcPath, CString *pclDate, CString *pclTime, u_int64 *pllSize)
{
   BOOL        fCc = FALSE;
   u_int32     ulCreate;
   //
   // convert the file date&time stamp
   //
   if( CVB_GetFileStatus(pcPath, &ulCreate, pllSize) )
   {
      //
      // We have file D&T stamp: build date and time stamps
      //
      if(pclDate) CVB_SecsToDate(pclDate, ulCreate);
      if(pclTime) CVB_SecsToTime(pclTime, ulCreate);
      fCc = TRUE;
   }
   return(fCc);
}

//
//  Function:   DB_GetTitle
//  Purpose:    Retrieve a title from the list
//
//  Parms:      Index 0..?, CString *
//  Returns:    Title^  or NULL if not found
//
char *CBook::DB_GetTitle(int iIndex, CBOOK  *pclA)
{
   char    *pcTitle = NULL;
   CBOOK   *pclT;

   pclT = pclA->pclXref;

   while(pclT)
   {
      if(pclT->iBoxIndex == iIndex)
      {
         if((pclT->tStatus & CALIBRE_STATUS_DELETE) || (pclT->tStatus & CALIBRE_STATUS_DELETED))  ;
         else pcTitle = pclT->pclName->GetBuffer();
         break;
      }
      pclT = pclT->pclNext;
   }
   return(pcTitle);
}

//
//  Function:   DB_GetTitleInfo
//  Purpose:    Retrieve title info from the list
//
//  Parms:      Index 0..?, Author, CString *
//  Returns:    Struct Author^  or NULL if not found
//
CBOOK *CBook::DB_GetTitleInfo(int iIndex, CBOOK  *pclA, CString *pclStr)
{
   const char *pcStatus;
   CBOOK      *pclT = pclA->pclXref;

   while(pclT)
   {
      if(pclT->iBoxIndex == iIndex)
      {
         if((pclT->tStatus & CALIBRE_STATUS_DELETE) || (pclT->tStatus & CALIBRE_STATUS_DELETED))
         {
            pclT = NULL;
            if(pclStr) *pclStr = "(Deleted)";
         }
         break;
      }
      pclT = pclT->pclNext;
   }
   //
   // Pass title status info if required
   //
   if(pclStr && pclT)
   {
      pcStatus = db_GetBookStatus(pclT);
      *pclStr  = pcStatus;
   }
   return(pclT);
}

//
//  Function:  DB_IsRecognized
//  Purpose:   Check if AUTHOR or TITLE has positively been recognized
//
//  Parms:     CBOOK, AUTHOR or TITLE
//  Returns:   TRUE if recognized
//
BOOL CBook::DB_IsRecognized(CATA tTarget)
{
   return(DB_IsRecognized(this, tTarget));
}

//
//  Function:  DB_IsRecognized
//  Purpose:   Check if AUTHOR or TITLE has positively been recognized
//
//  Parms:     AUTHOR or TITLE
//  Returns:   TRUE if recognized
//
BOOL CBook::DB_IsRecognized(CBOOK *pclBook, CATA tTarget)
{
   BOOL  fRecognized=FALSE;
   int   iMyScore=0;
   int   iScoreTholdTarget;
   int   iScoreTholdWord, iScoreTholdLetter;

   if(pclBook->tAuthorTitle != tTarget)
   {
      // We are comparing apples and pears: abort
      return(FALSE);
   }
   //
   switch(tTarget)
   {
      default:
      case CALIBRE_AUTHOR:
         pclNvm->NvmGet(NVM_AUTHOR_THOLD, &iScoreTholdTarget);
         break;

      case CALIBRE_TITLE:
         pclNvm->NvmGet(NVM_TITLE_THOLD, &iScoreTholdTarget);
         break;

   }
   //
   pclNvm->NvmGet(NVM_WORD_THOLD,   &iScoreTholdWord);
   pclNvm->NvmGet(NVM_LETTER_THOLD, &iScoreTholdLetter);
   //
   //  (TotalScore > threshold)
   //    AND
   //  (WordScore  > threshold) OR (LetterScore > threshold)
   //
   if(iScoreTholdTarget)
   {
      if(pclBook->iScore >= iScoreTholdTarget)           iMyScore++;
   }
   else iMyScore++;
   //
   //
   if(iScoreTholdWord)
   {
      if(iScoreTholdLetter)
      {
         if((pclBook->iScoreLetter >= iScoreTholdLetter) 
              ||
            (pclBook->iScoreWord >= iScoreTholdWord))    iMyScore++;
      }
      else
      {
         if(pclBook->iScoreWord >= iScoreTholdWord)      iMyScore++;
      }
   }
   else
   {
      if(iScoreTholdLetter)
      {
         if(pclBook->iScoreLetter >= iScoreTholdLetter)  iMyScore++;
      }
      else                                               iMyScore++;
   }
   //
   return( (iMyScore == 2) );
}

//
//  Function:  DB_ReadRecord
//  Purpose:   Read a single record from the CSV database into the this->pclRaw
//
//  Parms:     Filename, buffer
//  Returns:   Record length or -1 if error
//
int CBook::DB_ReadRecord(CString *pclFile, CString *pclStr)
{
   return( DB_ReadRecord(pclFile->GetBuffer(), pclStr) );
}

//
//  Function:  DB_ReadRecord
//  Purpose:   Read a single record from the CSV database into the this->pclRaw
//
//  Parms:     Filename
//  Returns:   Record length or -1 if error
//
int CBook::DB_ReadRecord(CString *pclFile)
{
   return( DB_ReadRecord(pclFile->GetBuffer(), pclRaw) );
}

//
//  Function:  DB_ReadRecord
//  Purpose:   Read a single record from the CSV database into the this->pclRaw
//
//  Parms:     Filename
//  Returns:   Record length or -1 if error
//
int CBook::DB_ReadRecord(char *pcFile)
{
   return( DB_ReadRecord(pcFile, pclRaw) );
}

//
//  Function:   DB_ReadRecord
//  Purpose:    Read a single record from the CSV database
//
//  Parms:     Filename, dest *
//  Returns:   Record length or -1 if error
//
int CBook::DB_ReadRecord(char *pcFile, CString *pclStr)
{
   BOOL        fRead=FALSE;
   int         iAct=0;

   if(pcFile == NULL)
   {
      // Request to close the file for sure
      if(fFileOpen == TRUE)
      {
         pclInFile->Close();
         fFileOpen = FALSE;
      }
      return(0);
   }

   if(fFileOpen == FALSE)
   {
      // File is not yet open
      if(!pclInFile->Open(pcFile, CFile::modeRead, NULL) )
      {
         // File is bad
         return(-1);
      }
      fFileOpen = TRUE;
   }
   //
   // Read a string from the file
   //
   fRead = pclInFile->ReadString(*pclStr);
   if(!fRead)
   {
      pclInFile->Close();
      fFileOpen = FALSE;
   }
   else
   {
      // Include \n terminator
      iAct = pclStr->GetLength() + 1;
   }
   return(iAct);
}

//
//  Function:  DB_ReIndexAuthors
//  Purpose:   Re-index the linked Author list
//
//  Parms:     
//  Returns:   Number of records
//
int CBook::DB_ReIndexAuthors(void)
{
   return( DB_ReIndexAuthors(pclAuthorList) );}

//
//  Function:   DB_ReIndexAuthors
//  Purpose:    Re-index the linked Author list
//
//  Parms:     Top ptr
//  Returns:   Number of records
//
int CBook::DB_ReIndexAuthors(CBook *pclTop)
{
   int   iNum=0;

   iNumAuthors = iNumTitles = 0;

   while(pclTop)
   {
      pclTop->iBoxIndex   = iNum++;
      pclTop->iWildIndex  = -1;
      pclTop->fBoxChecked = FALSE;
      iNumTitles         += db_ReIndexTitles(pclTop->pclXref);
      pclTop              = pclTop->pclNext;
   }
   iNumAuthors = iNum;
   return(iNumAuthors);
}

//
//  Function:  DB_ResetAllIndex
//  Purpose:   Clear all listbox list-indeces and checkmarkers in all title linked lists
//
//  Parms:
//  Returns:
//
void CBook::DB_ResetAllIndex(void)
{
   CBOOK   *pclA = pclAuthorList;
   CBOOK   *pclT;

   while(pclA)
   {
      pclT = pclA->pclXref;
      while(pclT)
      {
         pclT->fBoxChecked = 0;
         pclT->iWildIndex  = -1;
         pclT              = pclT->pclNext;
      }
      pclA = pclA->pclNext;
   }
}

//
//  Function:  DB_RetrieveCandidates
//  Purpose:   Retrieve all potential DB candidates for this target
//
//  Parms:     Current book list, dest
//  Returns:   Number of candidates
//
int CBook::DB_RetrieveCandidates(TLIST *pstCand, CString *pclDest)
{
   int      iCand=0;
   CString  clStr;

   pclDest->Empty();
   //
   while(pstCand)
   {
      iCand++;
      clStr.Format(_T("   Candidate[%2d]=(%3d%%):"), iCand, pstCand->iScore);
      *pclDest += clStr;
      clStr.Format(_T("[%s]\n"), pstCand->pclBook->pclName->GetBuffer());
      *pclDest += clStr;
      pstCand = pstCand->pstNext;
   }
   return(iCand);
}

//
//  Function:  DB_UpdateProgressBar
//  Purpose:   Update the GUI progress bar
//
//  Parms:     0-100%
//  Returns:   0
//
void CBook::DB_UpdateProgressBar(int iPercentage)
{
   static   int iPercentageLast=-1;

   if(iPercentage > CALIBRE_PROGRESS_RES)
   {
      iPercentage = CALIBRE_PROGRESS_RES;
   }
   //
   if(iPercentage != iPercentageLast)
   {
      CCalCmd::MessageToApp(WM_CALIBRE_SHOW_PROGRESS, iPercentage);
      iPercentageLast = iPercentage;
   }
}

//
//  Function:  DB_ValidateTarget
//  Purpose:   Validate if this author/title is already in the csv database
//
//  Parms:     CBOOK *, Author/Title, DB_WORDS/DB_SENTENCE
//  Returns:   CAVAL validation match enum:
//                CALIBRE_VAL_NOMATCH
//                CALIBRE_VAL_OKEEISH
//                CALIBRE_VAL_PERFECT
//
// OUT:        pstAltSolList           = linked list of alternative solutions
//             pclBook->iScore         = new score
//             pclBook->iScoreWord     =  "
//             pclBook->iScoreLetter   =  "
//
CAVAL CBook::DB_ValidateTarget(CBOOK *pclBook, CATA tTarget, CATG tDb)
{
   CAVAL       tCaval=CALIBRE_VAL_NOMATCH;
   int         iTmpScore, iWordScore=0, iLetterScore=0;
   BOOL        fDeepScan;
   int         iMatchWords;
   int         iMatchLetters;
   int         iNumWords, iNumLetters, iMyScore, iThreshold;
   char       *pcSubstitute;
   CBOOK      *pclList;
   CString     clStr;
   CMyString   clMyBook, clMyList;

   pclNvm->NvmGet(NVM_CHECKBOX_DEEPSCAN, &fDeepScan);
   iMyScore = pclBook->iScore;
   //
   // Check if we are looking for Authors or Titles.
   //
   if(tTarget == CALIBRE_AUTHOR)
   {
      //
      // This is a Author search: the list of authors is @pclAuthorList
      //
      pclList      = pclAuthorList;
      pcSubstitute = (char *)pcAuthorSubstitute;
      pclNvm->NvmGet(NVM_AUTHOR_THOLD, &iThreshold);
   }
   else
   {
      //
      // This is a Title search: The author of this title is @pclXref. 
      // The list of other titles of the author @pclXref.
      //
      pclList      = pclBook->pclXref->pclXref;
      pcSubstitute = (char *)pcTitleSubstitute;
      pclNvm->NvmGet(NVM_TITLE_THOLD, &iThreshold);
   }
   if(pclList == NULL) return(CALIBRE_VAL_NOMATCH);
   //
   // Check if we compare apples with apples
   //
   if(pclList->tAuthorTitle != tTarget)
   {
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   PROBLEMS Validating target: comparing Authors vrs Titles!", LOG_CRLF);
      return(CALIBRE_VAL_NOMATCH);
   }
   //
   // Validate Sentence against words:
   //
   //    List: DB Sentence     ->  "CCC DDDD EEEE"
   //    Book: Target Words    ->  "AAA BBB CCC DDDD EEEE"
   //
   //    --> Match 3 out of 5 = 60%
   //
   //    List: DB Words        ->  "CCC DDDD EEEE"
   //    Book: Target Sentence ->  "AAA BBB CCC DDDD EEEE"
   //
   //    --> Match 3 out of 3 = 100%
   //

   clMyBook = *pclBook->pclName;
   clMyBook.RemoveAll(pcSubstitute);
   clMyBook.MakeUpper();

   while(pclList)
   {
      //
      // Traverse all authors or titles
      //
      clMyList = *pclList->pclName;
      clMyList.RemoveAll(pcSubstitute);
      clMyList.MakeUpper();
      //
      switch(tDb)
      {
         default:
         case CALIBRE_DB_WORDS:
            // Check if all DB author/title entries match words in this new book author/title
            iNumWords   = clMyList.GetWordCount(iWordLength);
            iNumLetters = clMyList.GetLetterCount(iNumWords, iWordLength);
            iMyScore    = db_VerifyWords(&clMyBook, &clMyList, iNumWords, &iMatchWords, &iMatchLetters);
            break;

         case CALIBRE_DB_SENTENCE:
            // Check if all words in this book author/title match words in the DB author/title entry
            iNumWords   = clMyBook.GetWordCount(iWordLength);
            iNumLetters = clMyBook.GetLetterCount(iNumWords, iWordLength);
            iMyScore    = db_VerifyWords(&clMyList, &clMyBook, iNumWords, &iMatchWords, &iMatchLetters);
            break;
      }

      if(iMyScore >= iThreshold)
      {
         clStr.Format(_T("   Found DB entry [%s]"), clMyList.GetBuffer());
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
         //
         // We have some kind of score on the Database check.
         // Determine the partial scores of this validation:
         //
         //                   MatchWords * 100
         //    WordScore   = -------------------
         //                     TotalWords
         //
         //                  MatchLetters * 100
         //    LetterScore = -------------------
         //                     TotalLetters
         //
         iWordScore   = (iMatchWords   * 100) / iNumWords;
         iLetterScore = (iMatchLetters * 100) / iNumLetters;
         //
         // Check if this string of words is the best we have seen so far
         // If the score = 100% we cannot get any better now, can we. So stop searching if no deepscan is enabled.
         //
         if(iMyScore >= 100) 
         {
            // 100% score: check if compare other way also produces 100% score
            iTmpScore = iMyScore;
            iNumWords = clMyList.GetWordCount(iWordLength);
            iMyScore  = db_VerifyWords(&clMyBook, &clMyList, iNumWords, &iMatchWords, &iMatchLetters);
            if(iMyScore >= 100) 
            {
               iMyScore = (iMyScore + iTmpScore)/2;
               clStr.Format(_T("   Perfect score: Total MyScore=%d%%"), iMyScore);
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
               //
               // Add this solution to the linked list of alternative solutions
               //
               clStr.Format(_T("   Add [%s] to the alternative solutions list"), pclList->pclName->GetBuffer());
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
               db_UpdateCandidates(pclList, iMyScore);
               //
               tCaval = CALIBRE_VAL_PERFECT;
               if(fDeepScan)  pclList = pclList->pclNext; 
               else           pclList = NULL;
            }
            else
            {
               iMyScore = (iMyScore + iTmpScore)/2;
               clStr.Format(_T("   Partly perfect score: Total MyScore=%d%%"), iMyScore);
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
               //
               // Add this solution to the linked list of alternative solutions
               //
               clStr.Format(_T("   Add [%s] to the alternative solutions list"), pclList->pclName->GetBuffer());
               pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
               db_UpdateCandidates(pclList, iMyScore);
               //
               tCaval  = CALIBRE_VAL_OKEEISH;
               pclList = pclList->pclNext;
            }
         }
         else
         {
            clStr.Format(_T("   No perfect score: Total MyScore=%d%%"), iMyScore);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
            //
            // Add this solution to the linked list of alternative solutions
            //
            clStr.Format(_T("   Add [%s] to the alternative solutions list"), pclList->pclName->GetBuffer());
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
            db_UpdateCandidates(pclList, iMyScore);
            //
            tCaval  = CALIBRE_VAL_OKEEISH;
            pclList = pclList->pclNext;
         }
         //
         // Update score
         //
         if(iMyScore > pclBook->iScore)
         {
            clStr.Format(_T("   MyScore improved from %d%% to %d%%"), pclBook->iScore, iMyScore);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
            //
            pclBook->iScore         = iMyScore;
            pclBook->iScoreWord     = iWordScore;
            pclBook->iScoreLetter   = iLetterScore;
         }
      }
      else
      {
         pclList = pclList->pclNext;
      }
   }  // end while(pclList)
   //
   return(tCaval);
}


/* ======   Local   Functions separator ===========================================
void ____Local_Methods____(){}
==============================================================================*/

//
//  Function:  db_AddPathToBook
//  Purpose:   Construct the book pathname from its components
//
//  Parms:     Book, dest
//  Returns:   TRUE if OKee
//
BOOL CBook::db_AddPathToBook(CBOOK *pclT, CString *pclStr)
{
   char *pcType=DB_GetAsciiBookFormat(pclT->tFormat);

   pclStr->Format(_T("%s\\%s.%s"), pclT->pclDir->GetBuffer(), pclT->pclFile->GetBuffer(), pcType);

   return(TRUE);
}

//
//  Function:  db_CopyItem
//  Purpose:   Copy database item
//
//  Parms:     Dest, Src
//  Returns:   TRUE if OKee
//
BOOL CBook::db_CopyItem(CString *pclDest, char *pcSrc)
{
   int   iLen = db_GetItemLength(pcSrc);
   //
   // Copy the CSV field into the destination
   //
   CString clStr(pcSrc);
   *pclDest = clStr.Mid(1, iLen);
   return(TRUE);
}

//
//  Function:  db_ExtractItem
//  Purpose:   Extract a database field from the record
//
//  Parms:     Destination, Export type
//             ["Clancy, Tom","epub,pdf","Sum of all Fears, the"]
//                            ^
//                            |pcRecord
//  Returns:   TRUE if okee, FALSE if not found or zero length
//
BOOL CBook::db_ExtractItem(CString *pclDest, CAEXP tExp)
{
   BOOL  fCC=FALSE;
   int   iIdx;
   char *pcRecord = pclRaw->GetBuffer();

   switch(tExp)
   {
      default:
      case CALIBRE_EXPORT_NONE:
         iIdx=0;
         break;

      case CALIBRE_EXPORT_AUTHOR:
         iIdx = iCvsAuthor;
         break;

      case CALIBRE_EXPORT_AUTHOR_SORT:
         iIdx = iCvsAuthorSort;
         break;

      case CALIBRE_EXPORT_TITLE:
         iIdx = iCvsTitle;
         break;

      case CALIBRE_EXPORT_TITLE_SORT:
         iIdx = iCvsTitleSort;
         break;

      case CALIBRE_EXPORT_FORMATS:
         iIdx = iCvsFormats;
         break;

      case CALIBRE_EXPORT_IDS:
         iIdx = iCvsIds;
         break;

      case CALIBRE_EXPORT_SIZE:
         iIdx = iCvsBookSize;
         break;
   }
   //
   // Extract the item of choice from the database record
   //
   while(iIdx--)
   {
      if(iIdx == 0)
      {
         db_CopyItem(pclDest, pcRecord);
         fCC = (pclDest->GetLength() != 0);
      }
      else
      {
         pcRecord = db_SkipItem(pcRecord);
         if(pcRecord == NULL) return(FALSE);
      }
   }
   return(fCC);
}

//
//  Function:  db_GetBookStatus
//  Purpose:   Get the status text
//  Parms:     pclBook (Author or title)
//  Returns:   pcStatus
//
const char *CBook::db_GetBookStatus(CBOOK *pclBook)
{
   const char *pcStatus;

   if(pclBook)
   {
      if(pclBook->tStatus == CALIBRE_STATUS_NONE)     pcStatus = "--------- ";
      //
      if(pclBook->tStatus & CALIBRE_STATUS_CSV)       pcStatus = "CSV DB    ";
      if(pclBook->tStatus & CALIBRE_STATUS_NEW)       pcStatus = "NEW       ";
      if(pclBook->tStatus & CALIBRE_STATUS_ADDED)     pcStatus = "ADDED     ";
      if(pclBook->tStatus & CALIBRE_STATUS_BAD)       pcStatus = "BAD       ";
      if(pclBook->tStatus & CALIBRE_STATUS_DUP)       pcStatus = "DUPLICATE ";
      if(pclBook->tStatus & CALIBRE_STATUS_DELETE)    pcStatus = "DELETED   ";
      if(pclBook->tStatus & CALIBRE_STATUS_DELETED)   pcStatus = "DELETED   ";
   }
   return(pcStatus);
}

//
//  Function:  db_GetItemLength
//  Purpose:   Get the size of an CSV database field item
//
//  Parms:      Buffer^
//             ["Clancy, Tom","epub,pdf","Sum of "all" Fears, the"]
//  Returns:   Size or 0 if bad formatted
//
int CBook::db_GetItemLength(char *pcRecord)
{
   BOOL  fAccent, fDone = FALSE;
   char *pcStart, *pcEnd;
   int   iLen=0;

   if( (pcRecord == NULL) || (*pcRecord != '"') )
   {
      return(0);
   }

   fAccent = TRUE;
   pcStart = pcEnd = pcRecord;

   while(fDone == FALSE)
   {
      switch(*pcRecord++)
      {
         default:
            fAccent = FALSE;
            break;

         case '"':
            fAccent = TRUE;
            pcEnd   = pcRecord;
            break;

         case 0x0a:
         case ',':
         case ';':
            if(fAccent)
            {
               // OKee: Last char was a '"' : this is a CSV separator
               fDone = TRUE;
            }
            break;

         case 0:
            if(fAccent)
            {
               // OKee: Last char was a '"' : this is a CSV separator
            }
            else
            {
               // Bad format ! Record length >>>
               iLen = 0;
            }
            fDone = TRUE;
            break;
      }
   }
   iLen = (int) (pcEnd-pcStart-2);
   return(iLen);
}

//
//  Function:   db_LookupAuthors
//  Purpose:    Lookup the author in the CSV database linked list. Add the title to the author.
//
//  Parms:      Raw CSV record
//              this->pclRaw = "Clancy, Tom","Tom Clancy","epub,pdf","Sum of all Fears, the"
//                    iScore = 0
//  Returns:    TRUE if OKee added
//
BOOL CBook::db_LookupAuthors(void)
{
   BOOL     fCc=FALSE;
   CString  clTitle;
   CString  clAuthor;
   CString  clStr;
   CBOOK   *pclT, *pclA;
   int      iAuthors, iAlternatives;

   //
   // Get author: "Clancy, Tom"
   //
   if( db_ExtractItem(pclName, CALIBRE_EXPORT_AUTHOR_SORT) == FALSE) return(FALSE);
   pclName->Replace('.',' ');
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Authors/title:", LOG_NONE);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclRaw, LOG_CRLF);
   //
   // Extract the title or title_sort from the raw CSV record
   //
   if( db_ExtractItem(&clTitle, CALIBRE_EXPORT_TITLE) == FALSE)
   {
      // OOps, empty title: try sorted title too
      pclLog->LOG_Write(CALIBRE_LOG_BAD_FORMAT, "No unsorted title in CSV:", LOG_NONE);
      pclLog->LOG_Write(CALIBRE_LOG_BAD_FORMAT, pclRaw, LOG_CRLF);
      if( db_ExtractItem(&clTitle, CALIBRE_EXPORT_TITLE_SORT) == FALSE)
      {
         pclLog->LOG_Write(CALIBRE_LOG_BAD_FORMAT, " .. and no sorted title: abort.", LOG_CRLF);
         clTitle = "No valid title !";
      }
   }
   //
   // Check the number of co-authors
   //
   pclName->SetDelimiter("&");
   iAuthors = pclName->GetWordCount();
   //
   // We are looking for AUTHORS
   // Add all co-authors:
   //
   for(int i=1; i<=iAuthors; i++)
   {
      if( pclName->GetWord(&clAuthor, i) )
      {
         clAuthor.Trim();
         pclA = db_LookupAuthor(&clAuthor);
         //
         if(pclA)
         {
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   (co)Author: ", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclA->pclName, LOG_CRLF);
            iAlternatives = DB_RetrieveCandidates(pclA->pstCandidates, &clStr);
            DB_FreeAllCandidates();
            //
            pclT = DB_AddTitle(&clTitle, pclA, TRUE);
            pclT->tStatus |= CALIBRE_STATUS_CSV;
            // Overrule with current CSV library id
            pclT->iLibNum = iCurLib;
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "       Title : ", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclT->pclName, LOG_CRLF);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Alternative author list: ", LOG_CRLF);
            if(iAlternatives) pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
            else              pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   None.", LOG_CRLF);
            //
            // Retrieve Calibre ID
            //
            db_ExtractItem(&clStr, CALIBRE_EXPORT_IDS);
            pclT->iCalibreId = atoi(clStr.GetBuffer());
            //
            // Retrieve Calibre book size
            //
            db_ExtractItem(&clStr, CALIBRE_EXPORT_SIZE);
            pclT->iCalibreSize = atoi(clStr.GetBuffer());
            //
            // Retrieve all book formats available in the Calibre DB
            //
            tAllFormats = db_GetFormats();
         }
      }
   }
   return(fCc);
}

//
//  Function:  db_LookupAuthor
//  Purpose:   Check this recordif this author is already in the CSV database linked list.
//             Add it if it is new.
//
//  Parms:     Author
// 
//  Returns:   CBOOK * (new or existing) author
// 
//  IN:        this->pclRaw = "Clancy, Tom","Tom Clancy","epub,pdf","Sum of all Fears, the"
//             this->iScore = 0
//
CBOOK *CBook::db_LookupAuthor(CString *pclAuthor)
{
   BOOL     fDoAdd=TRUE;
   int      iValidate=2;
   CAVAL    tCaval=CALIBRE_VAL_INIT;
   CBOOK   *pclA=NULL;
   CBOOK    clTemp;

   //
   // Check any close match
   //
    clTemp = *this;
   *clTemp.pclName = *pclAuthor;
   //
   //
   // Handle the correct action
   //    CALIBRE_VAL_NOMATCH: Author not yet in the list: add a new author
   //    CALIBRE_VAL_OKEEISH: Author maybe in the list: check if beyond user threshold
   //    CALIBRE_VAL_PERFECT: Author already in the list: ignore this new author entry
   //    
   while(iValidate)
   {
      switch(tCaval)
      {
         default:
         case CALIBRE_VAL_INIT:
            iValidate--;
            tCaval = DB_ValidateTarget(&clTemp, CALIBRE_AUTHOR, CALIBRE_DB_SENTENCE);
            break;

         case CALIBRE_VAL_NOMATCH:
            iValidate--;
            break;

         case CALIBRE_VAL_OKEEISH:
            iValidate--;
            fDoAdd = FALSE;
            break;

         case CALIBRE_VAL_PERFECT:
            iValidate = 0;
            fDoAdd    = FALSE;
            break;
      } 
   }

   if(fDoAdd == FALSE)
   {
      //
      // We found someting: check if it's good enough
      //
      if( DB_IsRecognized(&clTemp, CALIBRE_AUTHOR) )
      {
         //
         // Author already in the linked list: use this one if the score is better
         //
         if( (clTemp.pstAltSolList) && (clTemp.pstAltSolList->iScore >= clTemp.iScore) )
         {
            pclA = clTemp.pstAltSolList->pclBook;
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   ...Recognized, alternative scores higher: ", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclA->pclName, LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, " = ", pclA->iScore, LOG_CRLF);
         }
         else 
         {
            pclA              = DB_AddAuthor(pclAuthor, TRUE);
            pclA->tStatus    |= CALIBRE_STATUS_CSV;
            pclA->iLibNum     = iCurLib;
            pclA->iScore      = clTemp.iScore;
            pclA->iScoreLetter= clTemp.iScoreLetter;
            pclA->iScoreWord  = clTemp.iScoreWord;
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   ...Recognized, but new author scores higher: ", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, clTemp.pclName, LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, " = ", clTemp.iScore, LOG_CRLF);
         }
      }
      else
      {
         //
         // Author stays below the threshold: add it afterall
         //
         pclA              = DB_AddAuthor(pclAuthor, TRUE);
         pclA->tStatus    |= CALIBRE_STATUS_CSV;
         pclA->iLibNum     = iCurLib;
         pclA->iScore      = clTemp.iScore;
         pclA->iScoreLetter= clTemp.iScoreLetter;
         pclA->iScoreWord  = clTemp.iScoreWord;
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   ...Unlikely to be: ", LOG_NONE);
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, clTemp.pclName, LOG_CRLF);
      }
   }
   else
   {
      // Author not yet in the list: add it
      pclA              = DB_AddAuthor(pclAuthor, TRUE);
      pclA->tStatus    |= CALIBRE_STATUS_CSV;
      pclA->iLibNum     = iCurLib;
      pclA->iScore      = clTemp.iScore;
      pclA->iScoreLetter= clTemp.iScoreLetter;
      pclA->iScoreWord  = clTemp.iScoreWord;
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   ...New author has been added.", LOG_CRLF);
   }
   return(pclA);
}

//
//  Function:  db_ParseExports
//  Purpose:   Parse this record for an export header
//
//  Parms:     Export header enum
//  Returns:   Index of the field into the header (1...?) or 0 if field not found
//  Note:      pclRaw: [author_sort, author, format, title, title_sort]
//
int CBook::db_ParseExports(CAEXP tExp)
{
   int      x, iExp=0;
   int      iLen;
   char    *pcExp=NULL;
   CString  clStr;

   switch(tExp)
   {
      default:
      case CALIBRE_EXPORT_NONE:
         break;

      case CALIBRE_EXPORT_AUTHOR:
         pcExp = "authors";
         break;

      case CALIBRE_EXPORT_AUTHOR_SORT:
         pcExp = "author_sort";
         break;

      case CALIBRE_EXPORT_TITLE:
         pcExp = "title";
         break;

      case CALIBRE_EXPORT_TITLE_SORT:
         pcExp = "title_sort";
         break;

      case CALIBRE_EXPORT_FORMATS:
         pcExp = "formats";
         break;

      case CALIBRE_EXPORT_IDS:
         pcExp = "id";
         break;

      case CALIBRE_EXPORT_SIZE:
         pcExp = "size";
         break;
   }

   if(pcExp)
   {
      // ==============================================================
      // Cstring *
      // Find the position of the export string in the header
      iLen = pclRaw->GetWordCount();
      for(x=1; x<=iLen; x++)
      {
         pclRaw->GetWord(&clStr, x);
         if(clStr.CompareNoCase(pcExp) == 0)
         {
            // found
            iExp = x;
            break;
         }
      }

   }
   return(iExp);
}

//
//  Function:   db_ReIndexTitles
//  Purpose:    Re-index the linked titles list
//
//  Parms:     Top ptr
//  Returns:   Number of records
//
int CBook::db_ReIndexTitles(CBook *pclTop)
{
   int   iIndex=0;

   while(pclTop)
   {
      pclTop->iBoxIndex   = iIndex++;
      pclTop->iWildIndex  = -1;
      pclTop->fBoxChecked = FALSE;
      pclTop              = pclTop->pclNext;
   }
   return(iIndex);
}

//
//  Function:  db_SkipItem
//  Purpose:   Skip an database field item
//
//  Parms:      Buffer^
//             ["Clancy, Tom","epub,pdf","Sum of all Fears, the"]
//  Returns:   Buffer^ or NULL
//
char *CBook::db_SkipItem(char *pcRecord)
{
   char *pcRes = NULL;
   int   iLen;

   if(pcRecord == NULL)
   {
      return(NULL);
   }

   iLen = db_GetItemLength(pcRecord);
   if(pcRecord[iLen+3] == '"')
   {
      // We do have a next field
      pcRes = &pcRecord[iLen+3];
   }
   return(pcRes);
}

//
//  Function:  db_UpdateCandidates
//  Purpose:   Update the linked list of potential DB candidates with a book we want to add
//
//  Parms:     CSV database entry, current score
//  Returns:   TRUE
//
BOOL CBook::db_UpdateCandidates(CBOOK *pclCsv, int iBookScore)
{
   BOOL     fInserted=FALSE;
   TLIST   *pstList=pstAltSolList;
   TLIST   *pstPrev=NULL;
   //
   TLIST   *pstCand = new(TLIST);
   //
   pstCand->iScore   = iBookScore;
   pstCand->pclBook  = pclCsv;
   //
   if(pstList == NULL)
   {
      pstAltSolList    = pstCand;
      pstCand->pstNext = NULL;
      return(TRUE);
   }
   //
   // Insert higher score, drop duplicate entries
   //
   do
   {
      if(iBookScore > pstList->iScore)
      {
         if(fInserted == FALSE)
         {
            // Insert new candidate here
            if(pstPrev) pstPrev->pstNext = pstCand;
            else        pstAltSolList    = pstCand;
            // 
            pstCand->pstNext = pstList;
            pstPrev          = pstCand;
            fInserted        = TRUE;
         }
      }
      //
      // Drop duplicate entries (the lowest score)
      //
      if( pstCand->pclBook->pclName->CompareNoCase(pstList->pclBook->pclName->GetBuffer()) == 0)
      {
         // This alternative book is already in the list:
         if(fInserted == TRUE)
         {
            // Remove the one already in the list since it has a lower score
            pstPrev->pstNext = pstList->pstNext;
            delete(pstList);
            pstList = pstPrev;
         }
         else
         {
            // Remove this new one again since it has a lower score
            //
            delete(pstCand);
            return(TRUE);
         }
      }
      pstPrev = pstList;
      pstList = pstList->pstNext;
   }
   while(pstList);
   //
   if(fInserted == FALSE)
   {
      //
      // Lowest score in the list, no duplicate entries: insert at the bottom
      //
      pstPrev->pstNext = pstCand;
      pstCand->pstNext = NULL;
   }
   return(TRUE);
}

//
//  Function:  db_VerifyWords
//  Purpose:   Verify the existance of all words in a sentence
//
//  Parms:     pclSentence, pclWords, number of words in plcWords
//  Returns:   Score 0..100%
//  Changes:   *piWords    = number of matching words
//             *piLetters  = number of matching letters
//
//       pclSentence --> "AAAAAA B C DDDDDDD"
//       pclWords    --> "B DDDDDDD"
//       iWords = 2
//
int CBook::db_VerifyWords(CMyString *pclSentence, CMyString *pclWords, int iNumWords, int *piWords, int *piLetters)
{
   int      w, iLen, iScore=0;
   int      iTotalLetters=0, iTotalMatched=0, iNumMatched=0;
   CString  clW;

   //
   // Count letters in sentence
   //
   iTotalLetters = pclSentence->GetLetterCount(iNumWords, iWordLength);
   //
   // Count matching letters in word
   //
   for(w=1; w<=iNumWords; w++)
   {
      pclWords->GetWord(&clW, w, iWordLength);
      iLen           = clW.GetLength();
      iTotalLetters += iLen;
      if( pclSentence->Find(clW) != -1)
      {
         // This word exists in the whole sentence: count it (twice: in src and dest)
         iNumMatched++;  
         iTotalMatched += (2*iLen);
      }
   }
   if(iTotalLetters)
   {
      iScore = (iTotalMatched * 100)/iTotalLetters;
      if(piWords)   *piWords   = iNumMatched;
      if(piLetters) *piLetters = iTotalMatched/2;
   }
   return(iScore);
}

//
//  Function:  db_CleanupBooks
//  Purpose:   Clean up all books marked for deletion
//
//  Parms:     BOOK *, fDeleteAll
//  Returns:   Top book or NULL
//
CBOOK *CBook::db_CleanupBooks(CBOOK *pclB, BOOL fDeleteAll)
{
   CBOOK   *pclTop=pclB, *pclTmp;
   CASTAT   tStatus;

   while(pclB)
   {
      if(fDeleteAll) tStatus = pclB->tStatus | CALIBRE_STATUS_DELETED;
      else           tStatus = pclB->tStatus;
      //
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "      T:[", LOG_NONE);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclB->pclName, LOG_NONE);
      //
      if( (tStatus & CALIBRE_STATUS_DELETE) || (tStatus & CALIBRE_STATUS_DELETED) )
      {
         // take care of next bookptr
         if(pclB->pclPrev) pclB->pclPrev->pclNext = pclB->pclNext;
         else              pclTop = pclB->pclNext;
         // take care of prev bookptr
         if(pclB->pclNext) pclB->pclNext->pclPrev = pclB->pclPrev;
         //
         // Remove this entry permamently
         //
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, ":Deleted]", LOG_CRLF);
         pclTmp = pclB;
         pclB   = pclB->pclNext;
         delete(pclTmp);
      }
      else
      {
         pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "]", LOG_CRLF);
         pclB = pclB->pclNext;
      }
   }
   return(pclTop);
}

//
//  Function:  db_GetFormats
//  Purpose:   Retrieve all the formats from this title
//
//  Parms:     
//  Returns:   All formats (OR-ed)
//
EBOOK CBook::db_GetFormats(void)
{
   int         iFormats;
   EBOOK       tBooks = CALIBRE_EBOOK_NONE;
   CMyString   clFormat;
   CString     clStr;

   if( db_ExtractItem(&clFormat, CALIBRE_EXPORT_FORMATS) )
   {
      clFormat.SetDelimiter(",");
      iFormats = clFormat.GetWordCount();
      //
      // We are looking for AUTHORS
      // Add all co-authors:
      //
      for(int i=1; i<=iFormats; i++)
      {
         if( clFormat.GetWord(&clStr, i) )
         {
            clStr.Trim();
            tBooks |= DB_GetBinaryBookFormat(&clStr);
         }
      }
   }
   return(tBooks);
}

