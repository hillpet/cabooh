/*  (c) Copyright:  2011  PATRN.NL, Confidential Data
**
**  $Workfile:          CalSheet.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Setup the property pages for the dialogs
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       22Aug2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"
#include "CalSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


IMPLEMENT_DYNAMIC(CCalSheet, CPropertySheet)

//
//  Function:  Constructor()
//  Purpose:
//  Parms:
//  Returns:
//
CCalSheet::CCalSheet(UINT nIDCaption, CWnd* pParentWnd, UINT iSelectPage)
   :CPropertySheet(nIDCaption, pParentWnd, iSelectPage)
{
    AddPage(&clCalPageGeneric);
    AddPage(&clCalPageWildcard);
}

//
//  Function:  Constructor()
//  Purpose:
//  Parms:
//  Returns:
//
CCalSheet::CCalSheet(LPCTSTR pszCaption, CWnd* pParentWnd, UINT iSelectPage)
   :CPropertySheet(pszCaption, pParentWnd, iSelectPage)
{
    AddPage(&clCalPageGeneric);
    AddPage(&clCalPageWildcard);
}

//
//  Function:  Destructor()
//  Purpose:
//  Parms:
//  Returns:
//
CCalSheet::~CCalSheet()
{
}


BEGIN_MESSAGE_MAP(CCalSheet, CPropertySheet)
   //{{AFX_MSG_MAP(CCalSheet)
      // NOTE - the ClassWizard will add and remove mapping macros here.
   //}}AFX_MSG_MAP
END_MESSAGE_MAP()


//
//  Function:  OnInitDialog()
//  Purpose:
//  Parms:
//  Returns:
//
BOOL CCalSheet::OnInitDialog()
{
   BOOL bResult = CPropertySheet::OnInitDialog();

   return bResult;
}

//
//  Function:  OnNotify()
//  Purpose:   Called on every change/redraw of the window
//  Parms:
//  Returns:
//
BOOL CCalSheet::OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult)
{
   return CPropertySheet::OnNotify(wParam, lParam, pResult);
}

