/*  Copyright 2011-2012 Patrn.nl
**
**  $Workfile:   Authors.cpp $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:     Author class implementation
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10 Mar 2012
**
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"

extern const char *pcLogon;
extern const char *pcPatrn;


//
//  Function:  CAuthor()
//  Purpose:   constructor
//  Parms:
//  Returns:
//
CAuthor::CAuthor()
{
}

//
//  Function:  CAuthor
//  Purpose:   Destructor
//  Parms:
//  Returns:
//
//  Note:
//
//
CAuthor::~CAuthor()
{
}

/* ======   Local   Functions separator ===========================================
void ____Public_Methods____(){}
==============================================================================*/

//
//  Function:  operator =
//  Purpose:   Copy class data
//  Parms:
//  Returns:   Copied class
//
//  Note:
//
//
CAuthor& CAuthor::operator=(const CAuthor& clAut)
{

   if (this != &clAut)
   {
   }
   return(*this);
}

//
//  Function:  AUTHOR_CreateDatabaseList
//  Purpose:   Write the whole database to a listfile
//
//  Parms:     Base name text, (Take input from NVM storage)
//  Returns:
//
void CAuthor::AUTHOR_CreateDatabaseList(const char *pcName)
{
   int   iStatusMask=0;
   BOOL  fEnabled;

   pclNvm->NvmGet(NVM_CHECKBOX_CVS, &fEnabled);
   if(fEnabled) iStatusMask |= CALIBRE_STATUS_CSV;
   //
   pclNvm->NvmGet(NVM_CHECKBOX_NEW, &fEnabled);
   if(fEnabled) iStatusMask |= CALIBRE_STATUS_NEW;
   //
   pclNvm->NvmGet(NVM_CHECKBOX_DUP, &fEnabled);
   if(fEnabled) iStatusMask |= CALIBRE_STATUS_DUP;
   //
   pclNvm->NvmGet(NVM_CHECKBOX_BAD, &fEnabled);
   if(fEnabled) iStatusMask |= CALIBRE_STATUS_BAD;
   //
   pclNvm->NvmGet(NVM_CHECKBOX_DEL, &fEnabled);
   if(fEnabled) iStatusMask |= CALIBRE_STATUS_DELETE;
   //
   pclNvm->NvmGet(NVM_CHECKBOX_ADD, &fEnabled);
   if(fEnabled) iStatusMask |= CALIBRE_STATUS_ADD;
   
   AUTHOR_CreateDatabaseList(pcName, iStatusMask);
}

//
//  Function:  AUTHOR_CreateDatabaseList
//  Purpose:   Write the whole database to a listfile
//
//  Parms:     Base name text, Status flags
//  Returns:
//
void CAuthor::AUTHOR_CreateDatabaseList(const char *pcName, int iStatusMask)
{
   CBOOK      *pclA;
   BOOL        fEnabled;
   CString     clName, clTmp, clStr;
   CStdioFile  clFile;
   int         iAuthor=0;


   pclNvm->NvmGet(NVM_NAME_USER_DIR,  &clStr);
   clName.Format(_T("%s\\%s-%s.txt"), clStr, CVB_GetTimeDateFilename(), pcName);
   //
   pclNvm->NvmPut(NVM_NAME_USER_LIST, clName);

   if( clFile.Open(clName, CFile::modeNoTruncate|CFile::modeWrite|CFile::modeCreate, NULL) )
   {
      clTmp.Format(_T("%s  %s (%s) - %s\n\n"), CVB_GetTimeDateStamp(), pcLogon, CALIBRE_VERSION, pcPatrn);
      clFile.WriteString(clTmp);
      //
      pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clStr);
      clTmp.Format(_T("Calibre DB = %s\n"), clStr);
      clFile.WriteString(clTmp);
      //
      pclNvm->NvmGet(NVM_CHECKBOX_DIRECTORY, &fEnabled);
      if(fEnabled) pclNvm->NvmGet(NVM_NAME_INPUT_DIR,  &clStr);
      else         pclNvm->NvmGet(NVM_NAME_INPUT_FILE, &clStr);
      clTmp.Format(_T("Input      = %s\n"), clStr);
      clFile.WriteString(clTmp);
      //
      clStr = "Filters    = ";
      if((iStatusMask & CALIBRE_STATUS_CSV) == CALIBRE_STATUS_CSV)         clStr += "CSV ";
      if((iStatusMask & CALIBRE_STATUS_NEW) == CALIBRE_STATUS_NEW)         clStr += "NEW ";
      if((iStatusMask & CALIBRE_STATUS_DUP) == CALIBRE_STATUS_DUP)         clStr += "DUP ";
      if((iStatusMask & CALIBRE_STATUS_BAD) == CALIBRE_STATUS_BAD)         clStr += "BAD ";
      if((iStatusMask & CALIBRE_STATUS_DELETE) == CALIBRE_STATUS_DELETE)   clStr += "DEL ";
      if((iStatusMask & CALIBRE_STATUS_ADD) == CALIBRE_STATUS_ADD)         clStr += "ADD ";

      clStr += "\n\n";
      clFile.WriteString(clStr);
      //
      do
      {
         pclA = DB_GetAuthorInfo(iAuthor, &clStr);
         if(pclA)
         {
            if( StatusDisplayAuthor(pclA, &clTmp, &clStr, iStatusMask) )
            {
               clFile.WriteString(clTmp);
               clFile.WriteString("\n");
            }
         }
         iAuthor++;
      }
      while(pclA);
      //
      clFile.Close();
   }
}

//
//  Function:   AUTHOR_GetAuthor
//  Purpose:    Retrieve an author from the list
//
//  Parms:      Index 0..?
//  Returns:    Author^  or NULL if not found (or marked for deletion)
//
char *CAuthor::AUTHOR_GetAuthor(int iIndex)
{
   char    *pcAuthor = NULL;
   CBOOK   *pclA     = pclAuthorList;

   while(pclA)
   {
      if(pclA->iBoxIndex == iIndex)
      {
         if( (pclA->tStatus & CALIBRE_STATUS_DELETE) || (pclA->tStatus & CALIBRE_STATUS_DELETED) )  ;
         else pcAuthor = pclA->pclName->GetBuffer();

         break;
      }
      pclA = pclA->pclNext;
   }
   return(pcAuthor);
}

/* ======   Local   Functions separator =======================================
void ____Methods_Database____(){}
==============================================================================*/
//
//  Function:  StatusDisplayAuthor
//  Purpose:   Display the Author plus title info to the status window
//
//  Parms:     Author, CString *, info, mask
//  Returns:   TRUE if we have someting to display/List
//
BOOL CAuthor::StatusDisplayAuthor(CBOOK *pclA, CString *pclStr, CString *pclInfo, int iMask)
{
   BOOL     fShow = FALSE;
   int      iIndex=0;
   CBOOK   *pclTitle;

   if(pclA)
   {
      *pclStr = *pclInfo;
      *pclStr += pclA->pclName->GetBuffer();
      *pclStr += "\n";
      //
      pclTitle = pclA->pclXref;
      //
      if(pclTitle)
      {
         do
         {
            if(StatusBookEnabled(pclTitle, iMask))
            {
               fShow = TRUE;
               DB_GetTitleInfo(iIndex, pclA, pclInfo);
               *pclStr += "         ";
               *pclStr += *pclInfo;
               *pclStr += pclTitle->pclName->GetBuffer();
               *pclStr += "\n";
            }
            iIndex++;
            pclTitle = pclTitle->pclNext;
         }
         while(pclTitle);
      }
   }
   return(fShow);
}

//
//  Function:  StatusBookEnabled
//  Purpose:   Check if the status of this book is enabled for display
//
//  Parms:     Book^, status mask
//  Returns:   TRUE if OKee
//
BOOL CAuthor::StatusBookEnabled(CBOOK *pstBook, int iMask)
{
   return( StatusEnabled(pstBook->tStatus, iMask) );
}

//
//  Function:  StatusEnabled
//  Purpose:   Check if the status of this book is enabled for display
//
//  Parms:     Status, mask
//  Returns:   TRUE if OKee
//
BOOL CAuthor::StatusEnabled(CASTAT tStatus, int iMask)
{
   BOOL  fEnabled=FALSE;

   fEnabled = ((iMask & tStatus) == tStatus);

   return(fEnabled);
}


/* ======   Local   Functions separator =======================================
void ____Methods_Books_Database____(){}
==============================================================================*/


