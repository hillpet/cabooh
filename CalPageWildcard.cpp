/*  (c) Copyright:  2011  CVB, Confidential Data
**
**  $Workfile:          CalPageWildcard.cpp $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for generic issues
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       25nov2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"
#include "CalPageWildcard.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

extern const char *pcLogon;
extern const char *pcPatrn;

//
// Define all button status
//
const WCBTNS stWcBtns[WC_NUM_FILTERS][3][WC_NUM_BUTTONS] =
{
   // WC_CVS
   {
      // IDC_RADIO_DEL_FILE 0
      {
         {IDC_WC_BUTTON_COPY,        0 }, 
         {IDC_WC_BUTTON_DELETE,      0 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        0 } 
      },
      // IDC_RADIO_DEL_DIR  1
      {
         {IDC_WC_BUTTON_COPY,        0 }, 
         {IDC_WC_BUTTON_DELETE,      0 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        0 } 
      },
      // IDC_RADIO_DEL_DB   2
      {
         {IDC_WC_BUTTON_COPY,        0 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        0 } 
      }
   },
   // WC_NEW
   {
      // IDC_RADIO_DEL_FILE 0
      {
         {IDC_WC_BUTTON_COPY,        1 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        1 } 
      },
      // IDC_RADIO_DEL_DIR  1
      {
         {IDC_WC_BUTTON_COPY,        1 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        1 } 
      },
      // IDC_RADIO_DEL_DB   2
      {
         {IDC_WC_BUTTON_COPY,        0 }, 
         {IDC_WC_BUTTON_DELETE,      0 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 1 }, 
         {IDC_WC_BUTTON_MOVE,        0 } 
      }
   },
   // WC_DUP   
   {
      // IDC_RADIO_DEL_FILE 0
      {
         {IDC_WC_BUTTON_COPY,        1 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        1 } 
      },
      // IDC_RADIO_DEL_DIR  1
      {
         {IDC_WC_BUTTON_COPY,        1 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        1 } 
      },
      // IDC_RADIO_DEL_DB   2
      {
         {IDC_WC_BUTTON_COPY,        0 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 1 }, 
         {IDC_WC_BUTTON_MOVE,        0 } 
      }
   },
   // WC_BAD   
   {
      // IDC_RADIO_DEL_FILE 0
      {
         {IDC_WC_BUTTON_COPY,        1 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        1 } 
      },
      // IDC_RADIO_DEL_DIR  1
      {
         {IDC_WC_BUTTON_COPY,        1 }, 
         {IDC_WC_BUTTON_DELETE,      1 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 0 }, 
         {IDC_WC_BUTTON_MOVE,        1 } 
      },
      // IDC_RADIO_DEL_DB   2
      {
         {IDC_WC_BUTTON_COPY,        0 }, 
         {IDC_WC_BUTTON_DELETE,      0 }, 
         {IDC_WC_BUTTON_CALIBRE_ADD, 1 }, 
         {IDC_WC_BUTTON_MOVE,        0 }
      }
   }
};

IMPLEMENT_DYNCREATE(CCalPageWildcard, CPropertyPage)

//
//  Function:  CCalPageWildcard()
//  Purpose:   Constructor
//  Parms:
//  Returns:
//
CCalPageWildcard::CCalPageWildcard() : CPropertyPage(CCalPageWildcard::IDD)
{
   //
   // To get access to theApp:
   //
   pclApp = (CCalibreApp *) AfxGetApp();

   m_hIcon    = AfxGetApp()->LoadIcon(IDR_MAINFRAME);

   pclNvm     = new(CNvmStorage);
   pclCmd     = new(CCalCmd);
   pclBook    = new(CBOOK);
   pclLog     = new(CLog);

   pclFontNum = new(CAutoFont)("Courier New");
   pclFontSta = new(CAutoFont)("Courier New");
   pclFontTxt = new(CAutoFont)("Tahoma");
   pclFontCmd = new(CAutoFont)("Tahoma");

   pclFontNum->SetHeight(CALIBRE_NUM_FONT_SIZE);
   pclFontTxt->SetHeight(CALIBRE_TXT_FONT_SIZE);
   pclFontSta->SetHeight(CALIBRE_TXT_FONT_SIZE);
   pclFontCmd->SetHeight(CALIBRE_CMD_FONT_SIZE);

   iRadioDelete         = 0;
   iChecklistTotalNr    = 0;
   iChecklistSelectedNr = 0;
   iChecklistIndex      = 0;
   tCurrentList         = CALIBRE_STATUS_NONE;
   tDeleteStatus        = CALIBRE_STATUS_NONE;
}

//
//  Function:  ~CCalPageWildcard()
//  Purpose:   Destructor
//  Parms:
//  Returns:
//
CCalPageWildcard::~CCalPageWildcard()
{
   delete(pclNvm);
   delete(pclCmd);
   delete(pclBook);
   delete(pclLog);
   //
   delete(pclFontNum);
   delete(pclFontTxt);
   delete(pclFontCmd);
   delete(pclFontSta);
}

//
//  Function:
//  Purpose:
//  Parms:
//  Returns:
//
void CCalPageWildcard::DoDataExchange(CDataExchange* pDX)
{
   CPropertyPage::DoDataExchange(pDX);

   DDX_Control(pDX, IDC_WC_EDIT_STATUS,      clChecklistStatus);
   DDX_Control(pDX, IDC_WC_CHECK_LISTBOX,    clListbox);
   DDX_Control(pDX, IDC_WC_EDIT_IMPORT_NAME, clListImportName);
   DDX_Control(pDX, IDC_WC_EDIT_DB_NAME,     clListDbName);
   DDX_Control(pDX, IDC_WC_EDIT_DEST_NAME,   clListDestName);
   DDX_Control(pDX, IDC_WC_SHOW_CMD,         clShowCommand);
   DDX_Control(pDX, IDC_WC_SHOW_SELECTED,    clShowSelected);
   DDX_Control(pDX, IDC_WC_SHOW_TOTAL,       clShowTotal);
   DDX_Control(pDX, IDC_WC_EDIT_WILDCARD,    clWildcard);
   //
   DDX_Radio(pDX,   IDC_WC_RADIO_DEL_FILE,   iRadioDelete);
   //                                        IDC_RADIO_DEL_FILE 0
   //                                        IDC_RADIO_DEL_DIR  1
   //                                        IDC_RADIO_DEL_DB   2
}


BEGIN_MESSAGE_MAP(CCalPageWildcard, CPropertyPage)
   //{{AFX_MSG_MAP(CCalPageWildcard)
   //}}AFX_MSG_MAP
   ON_LBN_SELCHANGE  (IDC_WC_CHECK_LISTBOX,        OnLbnSelchangeCheckListbox)
   //
   ON_BN_CLICKED     (IDC_WC_BUTTON_ADD_AUTHOR,    OnBnClickedButtonAddAuthor)
   ON_BN_CLICKED     (IDC_WC_BUTTON_DB,            OnBnClickedButtonDb)
   ON_BN_CLICKED     (IDC_WC_BUTTON_NEW,           OnBnClickedButtonNew)
   ON_BN_CLICKED     (IDC_WC_BUTTON_DUP,           OnBnClickedButtonDup)
   ON_BN_CLICKED     (IDC_WC_BUTTON_BAD,           OnBnClickedButtonBad)
   ON_BN_CLICKED     (IDC_WC_BUTTON_BROWSE,        OnBnClickedButtonBrowse)
   ON_BN_CLICKED     (IDC_WC_BUTTON_COPY,          OnBnClickedButtonCopy)
   ON_BN_CLICKED     (IDC_WC_BUTTON_DELETE,        OnBnClickedButtonDelete)
   ON_BN_CLICKED     (IDC_WC_BUTTON_CALIBRE_ADD,   OnBnClickedButtonAdd)
   ON_BN_CLICKED     (IDC_WC_BUTTON_MOVE,          OnBnClickedButtonMove)
   //
   ON_BN_CLICKED     (IDC_WC_BUTTON_SELECT_ALL,    OnBnClickedButtonSelectAll)
   ON_BN_CLICKED     (IDC_WC_BUTTON_SELECT_NONE,   OnBnClickedButtonSelectNone)
   ON_BN_CLICKED     (IDC_WC_RADIO_DEL_FILE,       OnBnClickedWcRadioDelFile)
   ON_BN_CLICKED     (IDC_WC_RADIO_DEL_DIR,        OnBnClickedWcRadioDelDir)
   ON_BN_CLICKED     (IDC_WC_RADIO_DEL_DB,         OnBnClickedWcRadioDelDb)
   //
   ON_EN_CHANGE      (IDC_WC_EDIT_WILDCARD,        OnEnChangeWcEditWildcard)
   //
   //  Private WM Messages
   //
   ON_MESSAGE        (WM_CALIBRE_ADD_READY,        OnRcvCalibreAddReady)
   ON_MESSAGE        (WM_CALIBRE_DELETE_READY,     OnRcvCalibreDeleteReady)
   ON_MESSAGE        (WM_CALIBRE_MESSAGE,          OnRcvCalibreMessage)
   //
END_MESSAGE_MAP()

//
//  Function:   OnInitDialog
//  Purpose:    Init upon window entry
//  Parms:
//  Returns:
//
BOOL CCalPageWildcard::OnInitDialog()
{
   CString  clStr;

   CPropertyPage::OnInitDialog();

   pclCmd->tMyDlgWnd = this->GetSafeHwnd();
   //
   clChecklistStatus.SetFont(pclFontSta);
   clChecklistStatus.SetBackColor(WHITE);
   clChecklistStatus.SetTextColor(BLUE);
   //
   clListImportName.SetFont(pclFontTxt);
   clListDbName.SetFont(pclFontTxt);
   clListDestName.SetFont(pclFontTxt);

   clShowSelected.SetBackColor(WHITE);
   clShowSelected.SetTextColor(BLUE);
   clShowSelected.SetFont(pclFontNum);

   clShowTotal.SetFont(pclFontNum);
   clShowCommand.SetFont(pclFontCmd);
   clShowCommand.SetBackColor(LTGRAY);
   clShowCommand.SetTextColor(BLUE);
   clShowCommand.SetWindowText("New");
   //
   clWildcard.SetWindowText("*");
   //
   UpdateCheckListBox(CALIBRE_STATUS_NEW);
   //
   return TRUE;  // return TRUE unless you set the focus to a control
                 // EXCEPTION: OCX Property Pages should return FALSE
}

//
//  Function:  OnPaint
//  Purpose:   If you add a minimize button to your dialog, you will need the code below
//             to draw the icon.  For MFC applications using the document/view model,
//             this is automatically done for you by the framework.
//
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnPaint()
{
   if (IsIconic())
   {
      CPaintDC dc(this); // device context for painting

      SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

      // Center icon in client rectangle
      int cxIcon = GetSystemMetrics(SM_CXICON);
      int cyIcon = GetSystemMetrics(SM_CYICON);
      CRect rect;
      GetClientRect(&rect);
      int x = (rect.Width() - cxIcon + 1) / 2;
      int y = (rect.Height() - cyIcon + 1) / 2;

      // Draw the icon
      dc.DrawIcon(x, y, m_hIcon);
   }
   else
   {
      CPropertyPage::OnPaint();
   }
}

//
//  Function:  OnQueryDragIcon
//  Purpose:   The system calls this function to obtain the cursor to display while the user drags
//             the minimized window.
//
//  Parms:
//  Returns:
//
HCURSOR CCalPageWildcard::OnQueryDragIcon()
{
   return static_cast<HCURSOR>(m_hIcon);
}

//
//  Function:  OnSysCommand
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnSysCommand(UINT nID, LPARAM lParam)
{
   CPropertyPage::OnSysCommand(nID, lParam);
}

//
//  Function:  OnOK
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnOK()
{
   CPropertyPage::OnOK();
}

//
//  Function:  OnCancel
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnCancel()
{
   CPropertyPage::OnCancel();
}

/* ====== Functions separator ===========================================
void ____Button_Handlers____(){}
=========================================================================*/


//
//  Function:  OnLbnSelchangeCheckListbox
//  Purpose:   Handle selection of one of the list members
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnLbnSelchangeCheckListbox()
{
   BOOL     fChecked;
   int      i, iIndex, iCand;
   CTitle   clTitle;
   CBOOK   *pclT;
   CBOOK   *pclA;
   TLIST   *pstCand;
   CString  clStr;
   char    *pcFormat;

   //
   // Find index in Title list. 
   // Take into account that the SPACEBAR could select and move ahead (VS 2010)
   // Get current index 0...?
   //
   iIndex = clListbox.GetCurSel();
   for(i=iIndex-1; i<=iIndex; i++)
   {
      if(i >= 0)
      {
         fChecked = clListbox.GetCheck(i);
         //
         pclT = clTitle.TITLE_GetTitleByWildcardIndex(i);
         if(pclT)
         {
            if( (pclT->fBoxChecked == TRUE)   && (fChecked == FALSE) ) iChecklistSelectedNr--;
            if( (pclT->fBoxChecked == FALSE)  && (fChecked == TRUE) )  iChecklistSelectedNr++;
            //
            pclT->fBoxChecked = fChecked;
            //
            // Show the selection criteria in the status area
            //
            pclA = pclT->pclXref;
            if((i == iIndex) && (pclA != NULL) )
            {
               clStr.Empty();
               clChecklistStatus.SetWindowText(clStr);
               //
               // Display Author and title info
               //
               clStr.Format(_T("Author Name     : %s" CRLF), pclA->pclName->GetBuffer());
               StatusAddText(clStr.GetBuffer());
               //
               // Display AUTHOR candidate titles
               //
               pstCand = pclA->pstCandidates;
               iCand   = 1;
               while(pstCand)
               {                                   
                  clStr.Format(_T("Author Alternative %2d (%3d%%):"), iCand, pstCand->iScore);
                  StatusAddText(clStr.GetBuffer());
                  if(pstCand->pclBook)
                  {
                     clStr.Format(_T(" %s" CRLF), pstCand->pclBook->pclName->GetBuffer());
                     StatusAddText(clStr.GetBuffer());
                  }
                  else
                  {
                     clStr.Format(_T("No Author alternatives" CRLF)); 
                     StatusAddText(clStr.GetBuffer());
                  }
                  pstCand = pstCand->pstNext;
                  iCand++;
               }                                
               clStr.Format(_T("Book   Name     : %s" CRLF), pclT->pclName->GetBuffer());
               StatusAddText(clStr.GetBuffer());
               //
               if(pclT->tStatus & CALIBRE_STATUS_CSV)
               {
                  //
                  // CSV entries do not have a dir/file structure
                  //
                  if(pclT->iCalibreId)
                  {
                     clStr.Format(_T("Book   ID       : %d" CRLF), pclT->iCalibreId);
                     StatusAddText(clStr.GetBuffer());
                  }
                  else
                  {
                     StatusAddText("Book   ID       : Not found!" CRLF);
                  }
               }
               else
               {
                  //
                  // Duplicate CSV entries have no dir/file as well
                  //
                  if(pclT->pclDir->GetLength())
                  {
                     clStr.Format(_T("Book   Directory: %s" CRLF), pclT->pclDir->GetBuffer());
                     StatusAddText(clStr.GetBuffer());
                     pcFormat = pclT->DB_GetAsciiBookFormat(pclT->tFormat);
                     clStr.Format(_T("Book   File     : %s.%s" CRLF), pclT->pclFile->GetBuffer(), pcFormat);
                     StatusAddText(clStr.GetBuffer());
                  }
               }
               //
               // Display TITLE candidate titles
               //
               pstCand = pclT->pstCandidates;
               iCand   = DisplayAlternatives(pstCand, 1);
            }
         }
      }
   }
   UpdateDialogScreen();            // Update the fields in the dialog
}


//
//  Function:  OnBnClickedButtonAdd
//  Purpose:   Add selection to CalibreDB-XX database
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonAdd()
{
   BOOL     fCc=TRUE, fExecReal;
   int      iIndex;
   CString  clStr;
   CTitle   clTitle;
   CBOOK   *pclT;
   const char *pcAdd = "ChBoxAdd";

   UpdateVariables();
   //
   // Get all index 0...?
   // Find index in Title list
   //
   pclNvm->NvmGet(NVM_CALIBRE_EXECMODE, &fExecReal);
   //
   pclNvm->NvmGet(NVM_NAME_DATABASE_DIR, &clStr);
   fCc = pclApp->BrowseFolder(&clStr, "Select Calibre eBook Database to ADD books ...");
   //
   if(fCc)
   {
      for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
      {
         pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
         if(pclT)
         {
            if(pclT->fBoxChecked)
            {
               tDeleteStatus  = pclT->tStatus;
               pclT->tStatus  = CALIBRE_STATUS_ADD;
            }
         }
         else MessageBox("Title not found !");
      }
      clTitle.AUTHOR_CreateDatabaseList(pcAdd, CALIBRE_STATUS_ADD);
      AddBooks(&clStr, CALIBRE_STATUS_ADD);
   }
   else
   {
      UpdateCheckListBox(tCurrentList);
   }
}

//
//  Function:  OnBnClickedButtonAddAuthor
//  Purpose:   Add selected authors to the list, redo book scan
//
//  Parms:      
//  Returns:   void
//  Note:      All existing NEW books will be marked DELETED
//
void CCalPageWildcard::OnBnClickedButtonAddAuthor()
{
   int      iIndex, iAuthor=0;
   CTitle   clTitle;
   CBOOK   *pclA;
   CBOOK   *pclT;
   CString  clStr;

   if( !(tCurrentList & CALIBRE_STATUS_NEW) )
   {
      MessageBox("Add Authors only for NEW books !");
      return;
   }

   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Add new author(s) to the list", LOG_CRLF);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);

   //
   // Add the author of all selected books to the list
   //
   for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
   {
      pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
      if(pclT)
      {
         if(pclT->fBoxChecked)
         {
            clStr.Format(_T("Add Author [%s]"), pclT->pclXref->pclName->GetBuffer());
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
            pclA = pclT->DB_AddAuthor(pclT->pclXref->pclName->GetBuffer());
            pclA->tStatus |= (CALIBRE_STATUS_NEW | CALIBRE_STATUS_ADDED);
         }
      }
      else MessageBox("Title not found !");
   }
   //
   // Delete all NEW books
   //
   do
   {
      pclA = pclBook->DB_GetAuthorInfo(iAuthor++, NULL);
      if(pclA)
      {
         if(pclA->tStatus & CALIBRE_STATUS_NEW) pclA->tStatus |= CALIBRE_STATUS_DELETED;

         pclT = pclA->pclXref;
         while(pclT)
         {
            if(pclT->tStatus & CALIBRE_STATUS_NEW) pclT->tStatus |= CALIBRE_STATUS_DELETED;

            pclT->fBoxChecked = 0;
            pclT              = pclT->pclNext;
         }
      }
   }
   while(iAuthor < pclBook->iNumAuthors);
   //
   // Cleanup booklist
   //
   pclBook->DB_CleanupDB();
   //
   CCalCmd::MessageToApp(WM_CALIBRE_BOOKS_RESCAN, 0);
   //
   clWildcard.SetWindowText("*");
   UpdateCheckListBox(tCurrentList);
}

//
//  Function:  OnBnClickedBrowse
//  Purpose:   Handle the browse to directory
//
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonBrowse()
{
   BOOL     fEnabled;
   CString  clStr;

   UpdateVariables();               // dialog window --> vars

   pclNvm->NvmGet(NVM_NAME_MOVETO_DIR, &clStr);
   if( pclApp->BrowseFolder(&clStr, "Select Folder for eBooks to Move/Copy to ...") )
   {
      // Move or copy all files in the input
      pclNvm->NvmPut(NVM_NAME_MOVETO_DIR,  clStr);
      pclNvm->NvmGet(NVM_CHECKBOX_DIRECTORY, &fEnabled);

      if(fEnabled) pclNvm->NvmGet(NVM_NAME_INPUT_DIR,  &clStr);
      else         pclNvm->NvmGet(NVM_NAME_INPUT_FILE, &clStr);
   }
   UpdateDialogScreen();            // Update the fields in the dialog
}

//
//  Function:  OnBnClickedButtonSelectAll
//  Purpose:   Handle the select all button
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonSelectAll()
{
   int      iIndex;
   CTitle   clTitle;
   CBOOK   *pclT;

   //
   // Get all index 0...?
   // Find wildcard index in Title list
   //
   for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
   {
      pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
      if(pclT)
      {
         pclT->fBoxChecked = 1;
         clListbox.SetCheck(iIndex, 1);
      }
      else MessageBox("Title not found !");
   }
   iChecklistSelectedNr = iChecklistTotalNr;
   UpdateDialogScreen();            // Update the fields in the dialog
}

//
//  Function:  OnBnClickedButtonSelectNone
//  Purpose:   Handle the select none button
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonSelectNone()
{
   int      iIndex;
   CTitle   clTitle;
   CBOOK   *pclT;

   //
   // Get all index 0...?
   // Find index in Title list
   //
   for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
   {
      pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
      if(pclT)
      {
         pclT->fBoxChecked = 0;
         clListbox.SetCheck(iIndex, 0);
         //Test: CString  clStr;
         //Test: clListbox.GetText(iIndex, clStr);
         //Test: MessageBox(clStr);
      }
      else MessageBox("Title not found !");
   }
   iChecklistSelectedNr = 0;
   UpdateDialogScreen();            // Update the fields in the dialog
}

//
//  Function:  OnBnClickedButtonDb
//  Purpose:   Show all original database entries
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonDb()
{
   clShowCommand.SetWindowText("CalibreDB");
   UpdateCheckListBox(CALIBRE_STATUS_CSV);
}

//
//  Function:  OnBnClickedButtonNew
//  Purpose:   Show all OKee new database entries
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonNew()
{
   clShowCommand.SetWindowText("New");
   UpdateCheckListBox(CALIBRE_STATUS_NEW);
}

//
//  Function:  OnBnClickedButtonDup
//  Purpose:   Show all duplicate new entries
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonDup()
{
   clShowCommand.SetWindowText("Duplicate");
   UpdateCheckListBox(CALIBRE_STATUS_DUP);
}

//
//  Function:  OnBnClickedButtonBad
//  Purpose:   Show all bad new entries
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonBad()
{
   clShowCommand.SetWindowText("Bad");
   UpdateCheckListBox(CALIBRE_STATUS_BAD);
}

//
//  Function:  OnBnClickedButtonMove
//  Purpose:   Handle the MOVE operation
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonMove()
{
   int      iCc;
   int      iIndex;
   CTitle   clTitle;
   CBOOK   *pclT;

   //
   // Get all index 0...?
   // Find index in Title list
   //
   for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
   {
      pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
      if(pclT)
      {
         if(pclT->fBoxChecked)
         {
            iCc = HandleCommand(pclT, CALIBRE_CMD_MOVE);
            if(iCc == -1) return;
            // OKee completion:
         }
      }
      else MessageBox("Title not found !");
   }
   // Clear all listbox list-indeces and checkmarkers in all title linked lists
   pclBook->DB_ResetAllIndex();
   UpdateCheckListBox(tCurrentList);
}

//
//  Function:  OnBnClickedButtonCopy
//  Purpose:   Handle the COPY operation
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonCopy()
{
   int      iCc;
   int      iIndex;
   CTitle   clTitle;
   CBOOK   *pclT;

   //
   // Get all index 0...?
   // Find index in Title list
   //
   for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
   {
      pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
      if(pclT)
      {
         if(pclT->fBoxChecked)
         {
            iCc = HandleCommand(pclT, CALIBRE_CMD_COPY);
            if(iCc == -1) return;
            // OKee completion:
            pclT->fBoxChecked = FALSE;
         }
      }
      else MessageBox("Title not found !");
   }
   UpdateCheckListBox(tCurrentList);
}

//
//  Function:  OnBnClickedButtonDelete
//  Purpose:   Handle the DELETE operation
//  Parms:
//  Returns:
//
void CCalPageWildcard::OnBnClickedButtonDelete()
{
   int      iCc;
   BOOL     fCc=TRUE, fExecReal;
   int      iIndex;
   CString  clStr;
   CTitle   clTitle;
   CBOOK   *pclT;
   const char *pcDelete = "ChBoxDel";

   UpdateVariables();
   //
   switch(iRadioDelete)
   {
      case 0:   //IDC_RADIO_DEL_FILE
         //
         // Get all index 0...?
         // Find index in Title list
         //
         for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
         {
            pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
            if(pclT)
            {
               if(pclT->fBoxChecked)
               {
                  iCc = HandleCommand(pclT, CALIBRE_CMD_DELETE);
                  if(iCc == -1) return;
                  // OKee completion:
                  pclT->fBoxChecked = FALSE;
                  pclT->tStatus    |= CALIBRE_STATUS_DELETE;
               }
            }
            else MessageBox("Title not found !");
         }
         UpdateCheckListBox(tCurrentList);
         break;

      case 1:   //IDC_RADIO_DEL_DIR
         MessageBox("Directory delete not implemented yet !");
         UpdateCheckListBox(tCurrentList);
         break;

      case 2:   //IDC_RADIO_DEL_DB
         //
         // Get all index 0...?
         // Find index in Title list
         //
         pclNvm->NvmGet(NVM_CALIBRE_EXECMODE, &fExecReal);
         //
         if(fExecReal)
         {
            pclNvm->NvmGet(NVM_NAME_DATABASE_DIR, &clStr);
            fCc = pclApp->BrowseFolder(&clStr, "Select Calibre eBook Database to DELETE from ...");
         }
         else
         {
            pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clStr);
         }
         //
         if(fCc)
         {
            for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
            {
               pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
               if(pclT)
               {
                  if(pclT->fBoxChecked)
                  {
                     tDeleteStatus  = pclT->tStatus;
                     pclT->tStatus |= CALIBRE_STATUS_DELETE;
                  }
               }
               else MessageBox("Title not found !");
            }
            clTitle.AUTHOR_CreateDatabaseList(pcDelete, CALIBRE_STATUS_DELETE);
            DeleteBooksById(&clStr, CALIBRE_STATUS_DELETE);
         }
         else
         {
            UpdateCheckListBox(tCurrentList);
         }
         break;
   }
}

//
//  Function:  OnBnClickedWcRadioDelFile
//  Purpose:   Handle the File/Directory/Calibre DB radio buttons
//
//  Parms:     
//  Returns:   
//
void CCalPageWildcard::OnBnClickedWcRadioDelFile()
{
   UpdateButtonStatus(tCurrentList);
}


//
//  Function:  OnBnClickedWcRadioDelDir
//  Purpose:   Handle the File/Directory/Calibre DB radio buttons
//
//  Parms:     
//  Returns:   
//
void CCalPageWildcard::OnBnClickedWcRadioDelDir()
{
   UpdateButtonStatus(tCurrentList);
}


//
//  Function:  OnBnClickedWcRadioDelDb
//  Purpose:   Handle the File/Directory/Calibre DB radio buttons
//
//  Parms:     
//  Returns:   
//
void CCalPageWildcard::OnBnClickedWcRadioDelDb()
{
   UpdateButtonStatus(tCurrentList);
}

//
//  Function:  OnEnChangeWcEditWildcard
//  Purpose:   Wildcard has changed.
//
//  Parms:     
//  Returns:
//
void CCalPageWildcard::OnEnChangeWcEditWildcard()
{
   CASTAT   tStatus = tCurrentList;

   //
   // Force update of the checkbox list
   //
   tCurrentList = CALIBRE_STATUS_NONE;
   UpdateCheckListBox(tStatus);
}

/* ====== Functions separator ===========================================
void ____Private_Msgs____(){}
=========================================================================*/

//
//  Function:  OnRcvCalibreMessage
//  Purpose:   Message from the Calibre Thread: display it
//
//  Parms:     WPARAM =
//             TPARAM =
//  Returns:   0
//
LRESULT CCalPageWildcard::OnRcvCalibreMessage(WPARAM tWp, LPARAM tLp)
{
   char *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvCalibreAddReady
//  Purpose:   Message from the Calibre Thread: Books add ready
//
//  Parms:     WPARAM =
//             TPARAM = iNum
//  Returns:   0
//
LRESULT CCalPageWildcard::OnRcvCalibreAddReady(WPARAM tWp, LPARAM tLp)
{
   CString  clStr;
   char    *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   clStr.Format(_T("%s  %d added to Calibre database" CRLF), CVB_GetTimeDateStamp(), tLp);
   StatusAddText(clStr.GetBuffer());
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvCalibreDeleteReady
//  Purpose:   Message from the Calibre Thread: Books deletion ready
//
//  Parms:     WPARAM =
//             TPARAM = iNum
//  Returns:   0
//
LRESULT CCalPageWildcard::OnRcvCalibreDeleteReady(WPARAM tWp, LPARAM tLp)
{
   CString clStr;
   int   iCompl = (int) tLp;
   char *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   if(iCompl < 0)
   {
      clStr.Format(_T("%s  Error completion on deleting books from the DB" CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
      UndeleteBooks(tDeleteStatus);
   }
   else if(iCompl > 0)
   {
      clStr.Format(_T("%s  %d Books deleted from the DB" CRLF), CVB_GetTimeDateStamp(), iCompl);
      StatusAddText(clStr.GetBuffer());
   }
   else
   {
      clStr.Format(_T("%s  Cancel completion on deleting books from the DB" CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
      UndeleteBooks(tDeleteStatus);
   }
   UpdateCheckListBox(tDeleteStatus);
   return(0);
}


/* ====== Functions separator ===========================================
void ____Local_functions____(){}
=========================================================================*/

//
//  Function:  CreateAuthorList
//  Purpose:   Create listbox
//
//  Parms:     pstAuthor, criteria
//  Returns:   number added
//
int CCalPageWildcard::CreateAuthorList(CASTAT tStatus)
{
   int      iAuthor=0, iNum=0;
   CBOOK   *pclA;

   do
   {
      pclA  = pclBook->DB_GetAuthorInfo(iAuthor++, NULL);
      iNum += CreateTitleList(pclA, tStatus);
      //
   }
   while(iAuthor < pclBook->iNumAuthors);
   //
   return(iNum);
}

//
//  Function:  CreateTitleList
//  Purpose:   Create listbox
//
//  Parms:     pstAuthor, criteria
//  Returns:   number added
//
int CCalPageWildcard::CreateTitleList(CBOOK *pclA, CASTAT tStatus)
{
   int      iNum=0, iCount=0;
   CString  clStr, clWild;
   CBOOK   *pclT;

   if(pclA)
   {
      pclT = pclA->pclXref;
      clWildcard.GetWindowText(clWild);
      while(pclT)
      {
         if(pclT->tStatus & tStatus)
         {
            //
            // Check if the author is not yet listed !
            //
            if( (pclA->tStatus & CALIBRE_STATUS_NEW) && pclT->pclFile->GetLength() )
            {
               if(pclT->iLibNum) clStr.Format(_T("%d-NewA:%s"), pclT->iLibNum, pclT->pclFile->GetBuffer());
               else              clStr.Format(_T("NewA:%s"), pclT->pclFile->GetBuffer());
            }
            else
            {
               // Existing author
               if(pclT->tStatus & CALIBRE_STATUS_NEW)
               {
                  if(pclT->iLibNum) clStr.Format(_T("%d-NewT:%s-%s"), pclT->iLibNum, pclA->pclName->GetBuffer(), pclT->pclName->GetBuffer());
                  else              clStr.Format(_T("NewT:%s-%s"), pclA->pclName->GetBuffer(), pclT->pclName->GetBuffer());
               }
               else
               {
                  if(pclT->iLibNum) clStr.Format(_T("%d-%s-%s"), pclT->iLibNum, pclA->pclName->GetBuffer(), pclT->pclName->GetBuffer());
                  else              clStr.Format(_T("%s-%s"), pclA->pclName->GetBuffer(), pclT->pclName->GetBuffer());
               }
            }
            
            //
            // Check if the result matches the user wildcards
            //
            if( WildMatch(clWild, clStr, TRUE) == TRUE)
            {
               pclT->iWildIndex = iChecklistIndex;
               clListbox.AddString(clStr);
               clListbox.SetCheck(iChecklistIndex, pclT->fBoxChecked);
               iChecklistIndex++;
               iCount++;
            }
         }
         pclT = pclT->pclNext;
      }
   }
   return(iCount);
}

//
//  Function:  AddBooks
//  Purpose:   Add a list of books to a Calibre DB
//
//  Parms:     DB name and new status
//  Returns:
//
void CCalPageWildcard::AddBooks(CString *pclDb, CASTAT tStatus)
{
   CString  clStr;

   if(pclCmd->IssueCommand(CMD_ADD_BOOKS, pclDb, tStatus))
   {
      clStr.Format(_T("%s  Adding books to the CSV database...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
   else
   {
      clStr.Format(_T("%s  Calibre is still busy...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
}

//
//  Function:  ParseBooks
//  Purpose:   Parse the book/titles list
//
//  Parms:     Filename
//  Returns:
//
void CCalPageWildcard::ParseBooks(CString *pclFile)
{
   CString  clStr;

   //
   // Parse a book list
   //
   if(pclCmd->IssueCommand(CMD_PARSE_BOOKS, pclFile))
   {
      clStr.Format(_T("%s  Parsing new books...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
   else
   {
      clStr.Format(_T("%s  Calibre is still busy...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
}

//
//  Function:  DeleteBooksById
//  Purpose:   Delete a list of books from a Calibre DB
//
//  Parms:     DB name and the field
//  Returns:
//
void CCalPageWildcard::DeleteBooksById(CString *pclDb, CASTAT tStatus)
{
   CString  clStr;

   if(pclCmd->IssueCommand(CMD_DELETE_BOOKS, pclDb, tStatus))
   {
      clStr.Format(_T("%s  Deleting books from the CSV database...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
   else
   {
      clStr.Format(_T("%s  Calibre is still busy...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
}

//
//  Function:  HandleCommand
//  Purpose:   Handle the move/copy/delete of selected titles
//
//  Parms:     pstTitle, Move/copy/delete
//  Returns:    0 = Start Explorer and retry here
//             +1 = Continue rest of files
//             -1 = Abort
//
int CCalPageWildcard::HandleCommand(CBOOK  *pclT, CACMD tCmd)
{
   int      iRet=1;
   CString  clNewDir, clIn, clNewPath;
   char    *pcFormat = pclBook->DB_GetAsciiBookFormat(pclT->tFormat);

   if(pclT->pclDir && pclT->pclFile)
   {
      clIn.Format (_T("%s\\%s.%s"), pclT->pclDir->GetBuffer(), pclT->pclFile->GetBuffer(), pcFormat);
      pclNvm->NvmGet(NVM_NAME_MOVETO_DIR, &clNewDir);
      clNewPath.Format(_T("%s\\%s.%s"), clNewDir, pclT->pclFile->GetBuffer(), pcFormat);
   }
   else
   {
      // No path info from the input: assume fixed path
      pclNvm->NvmGet(NVM_NAME_INPUT_DIR, &clNewDir);
      clIn.Format(_T("%s\\%s.%s"), clNewDir, pclT->pclName->GetBuffer(), pcFormat);
      //
      pclNvm->NvmGet(NVM_NAME_MOVETO_DIR, &clNewDir);
      clNewPath.Format(_T("%s\\%s.%s"), clNewDir, pclT->pclName->GetBuffer(), pcFormat);
   }

   do
   {
      switch(tCmd)
      {
         case CALIBRE_CMD_ADD:
            // resolved in AddBooks()
            break;

         case CALIBRE_CMD_COPY:
            if(clNewDir.GetLength() == 0) return(-1);
            //
            // We will do the file copy immediately
            //
            if(CopyFile(clIn, clNewPath, FALSE) == 0)
            {  // error:
               iRet = HandleCommandError(&clIn, clNewDir.GetBuffer(), tCmd);
            }
            else
            {
               iChecklistSelectedNr--;
               pclT->fBoxChecked = FALSE;
               // To remove the entry from the list:
               // pstTitle->tStatus  |= CALIBRE_STATUS_COPIED;
            }
            break;

         case CALIBRE_CMD_MOVE:
            if(clNewDir.GetLength() == 0) return(-1);
            //
            // We will do the file move immediately
            //
            if(MoveFileEx(clIn, clNewPath, MOVEFILE_REPLACE_EXISTING|MOVEFILE_WRITE_THROUGH|MOVEFILE_COPY_ALLOWED) == 0)
            {  // error:
               iRet = HandleCommandError(&clIn, clNewDir.GetBuffer(), tCmd);
            }
            else
            {
               iChecklistSelectedNr--;
               pclT->fBoxChecked = FALSE;
               pclT->tStatus    |= CALIBRE_STATUS_MOVED;
            }
            break;

         case CALIBRE_CMD_DELETE:
            // We will do the file delete immediately
            if(DeleteFile(clIn) == 0)
            {  // error:
               iRet = HandleCommandError(&clIn, clNewDir.GetBuffer(), tCmd);
            }
            else
            {
               iChecklistSelectedNr--;
               pclT->fBoxChecked = FALSE;
               pclT->tStatus    |= CALIBRE_STATUS_DELETED;
            }
            break;

         default:
            iRet = -1;
            break;
      }
   }
   while(iRet==0);
   //
   UpdateDialogScreenForced(IDC_WC_SHOW_SELECTED);
   //
   return(iRet);
}

//
//  Function:  HandleCommandError
//  Purpose:   Handle the move/copy/delete error of titles
//
//  Parms:     Src path, dir, Cmd
//  Returns:    0 = Start Explorer and retry here
//             +1 = Continue rest of files
//             -1 = Abort
//
int CCalPageWildcard::HandleCommandError(CString *pclIn, char *pcNew, CACMD tCmd)
{
   int      iRet;
   HANDLE   hProcess;
   CString  clStr;

   switch(tCmd)
   {
      case CALIBRE_CMD_ADD:
         clStr.Format(_T("Problems Adding\n[%s]\n\n"), pclIn);
         break;

      case CALIBRE_CMD_COPY:
         clStr.Format(_T("Problems Copying\n[%s]\n"), *pclIn);
         clStr += "to\n[";
         clStr += pcNew;
         clStr += "]\n\n";
         break;

      case CALIBRE_CMD_MOVE:
         clStr.Format(_T("Problems Moving\n[%s]\n"), *pclIn);
         clStr += "to\n[";
         clStr += pcNew;
         clStr += "]\n\n";
         break;

      case CALIBRE_CMD_DELETE:
         clStr.Format(_T("Problems Deleting\n[%s]\n\n"), *pclIn);
         break;
   }
   //
   clStr += "Fix it now ?\n";
   clStr += "  <YES> Start Explorer to help resolve the issue.\n";
   clStr += "  <NO> Continue with rest of the selected titles.\n";
   clStr += "  <CANCEL> Abort.";
   //
   iRet = AfxMessageBox(clStr, MB_YESNOCANCEL|MB_ICONEXCLAMATION);
   switch(iRet)
   {
      case IDYES:
         clStr.Format(_T("explorer.exe %s"), pcNew);
         hProcess = CVB_ExecuteCommand(&clStr);
         CVB_CheckProcessCompletion(hProcess, TRUE);
         iRet = 0;
         break;

      case IDNO:
         iRet = +1;
         break;

     default:
         iRet = -1;
         break;
   }
   return(iRet);
}

//
//  Function:  StatusAddText
//  Purpose:   Add text to the status window
//
//  Parms:     Text^
//  Returns:   void
//
void CCalPageWildcard::StatusAddText(char *pcText)
{
   CString  clStr;
   int      iLines;

   clChecklistStatus.GetWindowText(clStr);
   clStr += pcText;

   iLines = clChecklistStatus.GetLineCount();

   clChecklistStatus.SetWindowText(clStr);
   clChecklistStatus.LineScroll(iLines);
}

//
//  Function:  UndeleteBooks
//  Purpose:   Undelete the books marked CALIBRE_STATUS_DELETE
//  Parms:     Old status
//  Returns:
//
void CCalPageWildcard::UndeleteBooks(CASTAT tOldStatus)
{
   int      iIndex;
   CTitle   clTitle;
   CBOOK   *pclT;

   for(iIndex=0; iIndex<iChecklistTotalNr; iIndex++)
   {
      pclT = clTitle.TITLE_GetTitleByWildcardIndex(iIndex);
      if(pclT)
      {
         if(pclT->fBoxChecked)
         {
            //pclT->fBoxChecked = FALSE;
            pclT->tStatus  = tOldStatus;
         }
      }
   }
}

//
//  Function:  UpdateButtonStatus
//  Purpose:   Update the action button status (enable/disable
//  Parms:     Button action:
//                CALIBRE_STATUS_CSV --> WC_CSV      Calibre-CSV Database 
//                CALIBRE_STATUS_NEW --> WC_NEW      New books            
//                CALIBRE_STATUS_DUP --> WC_DUP      Duplicate books      
//                CALIBRE_STATUS_BAD --> WC_BAD      Bad formatted books  
//  Returns:
// 
//             WCBTNS stWcBtns[WC_NUM_FILTERS][3][WC_NUM_BUTTONS] =
//
void CCalPageWildcard::UpdateButtonStatus(CASTAT tButton)
{
   BOOL  fIdc;
   int   j, iIdc, iBtnCmd;

   UpdateVariables();
   //
   if     (tButton & CALIBRE_STATUS_CSV)    iBtnCmd = WC_CSV;
   else if(tButton & CALIBRE_STATUS_NEW)    iBtnCmd = WC_NEW;
   else if(tButton & CALIBRE_STATUS_BAD)    iBtnCmd = WC_BAD;
   else if(tButton & CALIBRE_STATUS_DUP)    iBtnCmd = WC_DUP;
   else                                     return;  
   //
   if(iChecklistSelectedNr == 0)
   {
      for(j=0; j<WC_NUM_BUTTONS; j++)
      {
         iIdc = stWcBtns[iBtnCmd][iRadioDelete][j].iWcIdc;
         GetDlgItem(iIdc)->EnableWindow(FALSE);
      }
   }
   else
   {
      for(j=0; j<WC_NUM_BUTTONS; j++)
      {
         iIdc = stWcBtns[iBtnCmd][iRadioDelete][j].iWcIdc;
         fIdc = stWcBtns[iBtnCmd][iRadioDelete][j].fWcBtn;
         //
         GetDlgItem(iIdc)->EnableWindow(fIdc);
      }
   }
}

//
//  Function:  UpdateCheckListBox
//  Purpose:   Show Checklist box entries
//  Parms:     View
//  Returns:
//
void CCalPageWildcard::UpdateCheckListBox(CASTAT tStatus)
{
   CString clStr;

   if((tCurrentList & CASTAT_MASK_ATTR) != tStatus)
   {
      // Clear all listbox list-indeces and checkmarkers in all title linked lists
      tCurrentList = tStatus;
      pclBook->DB_ResetAllIndex();
      clStr.Empty();
      clChecklistStatus.SetWindowText(clStr);
   }
   clListbox.ResetContent();
   iChecklistIndex      = 0;
   iChecklistSelectedNr = 0;
   iChecklistTotalNr    = CreateAuthorList(tStatus);
   //
   UpdateDialogScreen();            // Update the fields in the dialog
}

//
//  Function:  UpdateDialogScreen
//  Purpose:   Update the variables in the dialog screen
//
//  Parms:     void
//  Returns:   void
//
void CCalPageWildcard::UpdateDialogScreen()
{
   BOOL     fEnabled;
   CString  clStr;

   pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clStr);
   clListDbName.SetWindowText(clStr);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_DIRECTORY, &fEnabled);
   if(fEnabled)
   {
      // Operation on directories:
      pclNvm->NvmGet(NVM_NAME_INPUT_DIR, &clStr);
   }
   else
   {
      // Operation on files:
      pclNvm->NvmGet(NVM_NAME_INPUT_FILE, &clStr);
   }
   clListImportName.SetWindowText(clStr);
   //
   pclNvm->NvmGet(NVM_NAME_MOVETO_DIR, &clStr);
   clListDestName.SetWindowText(clStr);
   //
   clStr.Format(_T("%d"), iChecklistTotalNr);
   clShowTotal.SetWindowText(clStr);
   //
   clStr.Format(_T("%d"), iChecklistSelectedNr);
   clShowSelected.SetTextColor(BLACK);
   clShowSelected.SetWindowText(clStr);
   //
   UpdateButtonStatus(tCurrentList);
   //
   UpdateData(FALSE);            // dialog variables --> window
}

//
//  Function:  UpdateDialogScreenForced
//  Purpose:   Handle DLG events to update dialog windows
//
//  Parms:     Dlg ID
//  Returns:   void
//
void CCalPageWildcard::UpdateDialogScreenForced(int iDlgId)
{
   CString  clStr;
   MSG      stMsg;

   switch(iDlgId)
   {
      case IDC_WC_SHOW_SELECTED:
         clStr.Format(_T("%d"), iChecklistSelectedNr);
         clShowSelected.SetTextColor(RED);
         clShowSelected.SetWindowText(clStr);
         break;

      default:
         break;
   }
   //
   // Handle Win Msg pump
   //
   while(::PeekMessage (&stMsg, NULL, 0, 0, PM_REMOVE) )
   {
      ::TranslateMessage(&stMsg);
      ::DispatchMessage(&stMsg);
   }
}

//
//  Function:  UpdateVariables
//  Purpose:   Update all the clEdit variables from screen --> class variables
//
//  Parms:
//  Returns:   
//
void CCalPageWildcard::UpdateVariables(void)
{
   CString  clStr;

   UpdateData(TRUE);                // dialog window --> variables
   //
   clListDbName.GetWindowText(clStr);
   pclNvm->NvmPut(NVM_NAME_DATABASE_FILE, clStr);
   //
   clListImportName.GetWindowText(clStr);
   pclNvm->NvmPut(NVM_NAME_INPUT_FILE, clStr);
   //
   clListDestName.GetWindowText(clStr);
   pclNvm->NvmPut(NVM_NAME_MOVETO_DIR, clStr);
}


//
//  Function:  WildMatch
//  Purpose:   Check if the string matches the wildcard search
//
//  Parms:     Wildcard string, Destination string, NoCase YESNO
//  Returns:   
//
BOOL CCalPageWildcard::WildMatch(CString sWild, CString sString, BOOL fIgnoreCase)
{
   CString  clWild, clString;
   char    *pcDestCopy = NULL, *pcWildCopy = NULL;
   char    *pcWild;
   char    *pcDest;

   clWild   = sWild;
   clString = sString;
   //
   if(fIgnoreCase)
   {
      // Ignore case
      clWild.MakeUpper();
      clString.MakeUpper();
   }
   pcWild = clWild.GetBuffer();
   pcDest = clString.GetBuffer();

   while ((*pcDest) && (*pcWild != '*')) 
   {
      if ((*pcWild != *pcDest) && (*pcWild != '?')) 
      {
         return(FALSE);
      }
      pcWild++;
      pcDest++;
   }

   while (*pcDest) 
   {
      if (*pcWild == '*') 
      {
         if (!*++pcWild) 
         {
             return(TRUE);
         }
         pcWildCopy = pcWild;
         pcDestCopy = pcDest+1;
      } 
      else if ((*pcWild == *pcDest) || (*pcWild == '?')) 
      {
         pcWild++;
         pcDest++;
      } 
      else 
      {
         pcWild = pcWildCopy;
         pcDest = pcDestCopy++;
      }
   }

   while(*pcWild == '*') 
   {
      pcWild++;
   }
   return(!*pcWild);
}



//
//  Function:  PreTranslateMessage
//  Purpose:   Intercept raw keyboard messages
//
//  Parms:     Message^
//  Returns:   TRUE if the message has been handled and needs no further action
//
BOOL CCalPageWildcard::PreTranslateMessage(MSG *pMsg)
{
   BOOL     fCc = FALSE;
   POINT    stPoint;
   CRect    clCell;

   switch(pMsg->message)
   {
      case WM_MOUSEMOVE:
         break;

      case WM_MOUSELEAVE:
         break;

      case WM_LBUTTONDOWN:
         break;

      case WM_LBUTTONUP:
         break;

      case WM_LBUTTONDBLCLK:
         break;

      case WM_KEYDOWN:
         //
         // See #include "WinUser.h"
         // If mouse is within the wildcard edit window, reroute the keyboard entries 
         //
         GetCursorPos(&stPoint);
         CWnd *pclDlg = GetDlgItem(IDC_WC_EDIT_WILDCARD);
         pclDlg->GetWindowRect(&clCell);
         if(clCell.PtInRect(stPoint))
         {
            //
            // all keyboard entries need to go to the wildcard edit box
            //
         }
         else
         {
            //
            // all keyboard entries go to the listbox
            //
            fCc = HandleKeys((char)pMsg->wParam);
         }
         break;
   }

   //
   // Ignore the event if the message has been handled here !
   //
   if(fCc == FALSE)
   {
      //
      // Key has NOT been handled !
      //
      fCc = CDialog::PreTranslateMessage(pMsg);
   }
   return(fCc);
}

//
//  Function:   HandleKeys
//  Purpose:    Handle the manual keyboard entry
//
//  Parms:      fValidateKey, key
//  Returns:    TRUE if key does something
//
BOOL CCalPageWildcard::HandleKeys(char cKey)
{
   BOOL  fCc;

   switch(cKey)
   {
      case ' ':
         fCc = HandleSpaceKey();
         break;

      default:
      case VK_UP:
      case VK_DOWN:
      case VK_LEFT:
      case VK_RIGHT:
      case VK_HOME:
      case VK_END:
      case VK_DELETE:
         fCc = FALSE;
         break;
   }
   return(fCc);
}

//
//  Function:  HandleSpaceKey
//  Purpose:   Handle the manual keyboard entry: Toggle the checkbox
//
//  Parms:      
//  Returns:   TRUE if key handled
//  Note:      The listbox behavior already toggles the checkbox: 
//
BOOL CCalPageWildcard::HandleSpaceKey(void)
{
   BOOL     fChecked;
   CTitle   clTitle;
   CBOOK   *pclT;
   int      i;

   i = clListbox.GetCurSel();
   //
   fChecked = !clListbox.GetCheck(i);
   clListbox.SetCheck(i, fChecked);
   //
   pclT = clTitle.TITLE_GetTitleByWildcardIndex(i);
   if(pclT)
   {
      if( (pclT->fBoxChecked == TRUE)   && (fChecked == FALSE) ) iChecklistSelectedNr--;
      if( (pclT->fBoxChecked == FALSE)  && (fChecked == TRUE) )  iChecklistSelectedNr++;
      //
      pclT->fBoxChecked = fChecked;
   }
   UpdateDialogScreen();
   return(TRUE);
}

//
//  Function:  DisplayAlternatives
//  Purpose:   Display all alternatives (recursive)
//
//  Parms:     TLIST *, number
//  Returns:   Alternatives last number 
//
int CCalPageWildcard::DisplayAlternatives(TLIST *pstCand, int iCand)
{
   CString  clStr;

   while(pstCand)
   {
      clStr.Format(_T("Alternative %2d (%03d%%):"), iCand, pstCand->iScore);
      StatusAddText(clStr.GetBuffer());
      if(pstCand->pclBook)
      {
         clStr.Format(_T(" Db:%d-ID:%5d %s" CRLF), pstCand->pclBook->iLibNum, pstCand->pclBook->iCalibreId, pstCand->pclBook->pclName->GetBuffer());
         StatusAddText(clStr.GetBuffer());
         //
         // Check if more alternatives present themselves
         //
         iCand++;
         iCand = DisplayAlternatives(pstCand->pclBook->pstCandidates, iCand);
      }
      else
      {
         clStr.Format(_T("No Title alternatives" CRLF)); 
         StatusAddText(clStr.GetBuffer());
      }
      pstCand = pstCand->pstNext;
   }
   return(iCand);
}

