/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  Commands.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Header file for the *.cpp
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       07 Aug 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_COMMANDS_H_)
#define _COMMANDS_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

typedef enum CCMDS
{
   CMD_DONE = 0,
   CMD_PARSE_DB,
   CMD_PARSE_BOOKS,
   CMD_ADD_BOOKS,
   CMD_MOVE_BOOKS,
   CMD_DELETE_BOOKS,
   CMD_EXPORT_BOOKS,
   CMD_EXIT,

   NUM_CCMDS
}  CCMDS;

//
// The Class for handling Calibre commands
//
class CCalCmd : public CDialog
{
public:
            CCalCmd();
   virtual ~CCalCmd();

public:
   BOOL     ExecuteCmd           (CTitle *);
   BOOL     IssueCommand         (CCMDS);
   BOOL     IssueCommand         (CCMDS, CString *);
   BOOL     IssueCommand         (CCMDS, CString *, CString *);
   BOOL     IssueCommand         (CCMDS, CString *, int);
   //
   void     CommandDone          (int);
   void     EventHandlePut       (HANDLE);

public:
   CString       *pclMyParm1;
   CString       *pclMyParm2;
   int            iMyParm1;
   int            iMyParm2;
   HWND           tMyDlgWnd;

public:
   static void MessageToApp      (int, int);
   static void TextMessageToApp  (int, char *);
   static void TextMessageToApp  (int, BOOL, char *);
   static void TextMessageToApp  (int, BOOL, char *, int);

private:
   static HANDLE  tGlobalEvtHandle;
   static int     iGlobalCommandNr;
   static int     iGlobalCc;
   //
   static CCMDS   tGlobalCmd;
   static CString clGlobalParm1;
   static CString clGlobalParm2;
   static int     iGlobalParm1;
   static int     iGlobalParm2;
   static HWND    tGlobalDlgWnd;
};

#endif // !defined(_COMMANDS_H_)
