/*  (c)2011 Patrn.nl
**
**  $Workfile:  CalibreFiles.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define the Calibre helper functions
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       06 Jun 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_CALIBREFILES_H_)
#define _CALIBREFILES_H_



#ifdef  FEATURE_CHECK_MEMORY
   #define CHECKMEMORY()         DB_CheckMemory()
   #define new                   DEBUG_NEW
#else
   #define CHECKMEMORY()
#endif

#define CRLF                     "\r\n"

#define CALIBRE_MAXLIBS          4
//
#define CALIBRE_NAME_LEN         256
#define CALIBRE_LOGFILE_LEN      512
//
#define CALIBRE_NUM_FONT_SIZE    18
#define CALIBRE_TXT_FONT_SIZE    14
#define CALIBRE_CMD_FONT_SIZE    18
//
#define CALIBRE_RECORD_LEN       8192
#define CALIBRE_MAX_TEXT_LEN     256
#define CALIBRE_FILEPATH_LEN     256
#define CALIBRE_MAX_WORD_LEN     32
#define CALIBRE_PROGRESS_RES     100
#define CALIBRE_TITLE_TRUNCATE   35

#define MIN_WORD_LENGTH          2

const COLORREF CLOUDBLUE   = RGB(128, 184, 223);
const COLORREF WHITE       = RGB(255, 255, 255);
const COLORREF BLACK       = RGB(1, 1, 1);
const COLORREF DKGRAY      = RGB(128, 128, 128);
const COLORREF LTGRAY      = RGB(192, 192, 192);
const COLORREF YELLOW      = RGB(255, 255, 0);
const COLORREF DKYELLOW    = RGB(128, 128, 0);
const COLORREF LTRED       = RGB(255, 128, 128);
const COLORREF RED         = RGB(255, 0, 0);
const COLORREF DKRED       = RGB(128, 0, 0);
const COLORREF BLUE        = RGB(0, 0, 255);
const COLORREF DKBLUE      = RGB(0, 0, 128);
const COLORREF CYAN        = RGB(0, 255, 255);
const COLORREF DKCYAN      = RGB(0, 128, 128);
const COLORREF LTGREEN     = RGB(128, 255, 128);
const COLORREF GREEN       = RGB(128, 255, 128);
const COLORREF DKGREEN     = RGB(0, 128, 0);
const COLORREF MAGENTA     = RGB(255, 0, 255);
const COLORREF DKMAGENTA   = RGB(128, 0, 128);

typedef enum CALIST
{
   CALIBRE_STATUS_INIT = 0,
   CALIBRE_STATUS_DONE,

   NUM_CALIBRE_STATUS
}  CALIST;

typedef enum CAVAL
{
   CALIBRE_VAL_INIT = 0,
   CALIBRE_VAL_NOMATCH,
   CALIBRE_VAL_OKEEISH,
   CALIBRE_VAL_PERFECT,

   NUM_CALIBRE_VALS
}  CAVAL;

typedef enum CADET
{
   CALIBRE_DETERMINE_INIT = 0,
   CALIBRE_DETERMINE_MATCH,
   CALIBRE_DETERMINE_NEW,
   CALIBRE_DETERMINE_BAD,

   NUM_CALIBRE_DETERMINE
}  CADET;

typedef int EBOOK;

#define  CALIBRE_EBOOK_NONE         0x0000 
#define  CALIBRE_EBOOK_EPUB         0x0001 
#define  CALIBRE_EBOOK_LIT          0x0002 
#define  CALIBRE_EBOOK_PDF          0x0004 
#define  CALIBRE_EBOOK_TXT          0x0008 
#define  CALIBRE_EBOOK_PDB          0x0010 
#define  CALIBRE_EBOOK_MOBI         0x0020 
#define  CALIBRE_EBOOK_DOC          0x0040 
#define  CALIBRE_EBOOK_RTF          0x0080 


typedef enum CAEXP
{
   CALIBRE_EXPORT_NONE = 0,
   CALIBRE_EXPORT_AUTHOR,
   CALIBRE_EXPORT_AUTHOR_SORT,
   CALIBRE_EXPORT_TITLE,
   CALIBRE_EXPORT_TITLE_SORT,
   CALIBRE_EXPORT_FORMATS,
   CALIBRE_EXPORT_IDS,
   CALIBRE_EXPORT_SIZE,

   NUM_CALIBRE_EXPORTS
}  CAEXP;

typedef enum SPLIT
{
   CALIBRE_SPLIT_NONE = 0,
   CALIBRE_SPLIT_TITLE_AUTHOR,
   CALIBRE_SPLIT_AUTHOR_TITLE,
   CALIBRE_SPLIT_DONE,

   NUM_CALIBRE_SPLITS
}  SPLIT;

typedef int CASTAT;

#define  CALIBRE_STATUS_NONE        0x0000
#define  CALIBRE_STATUS_CSV         0x0001
#define  CALIBRE_STATUS_NEW         0x0002
#define  CALIBRE_STATUS_BAD         0x0004
#define  CALIBRE_STATUS_ADD         0x0008
//
#define  CALIBRE_STATUS_DELETE      0x0100
#define  CALIBRE_STATUS_DUP         0x0200
//                                  
#define  CALIBRE_STATUS_MOVED       0x1000
#define  CALIBRE_STATUS_COPIED      0x2000
#define  CALIBRE_STATUS_ADDED       0x4000
#define  CALIBRE_STATUS_DELETED     0x8000
//
#define  CASTAT_MASK_FLAGS          0xFF00
#define  CASTAT_MASK_ATTR           0x00FF

typedef enum CATA
{
   CALIBRE_AUTHOR = 0,
   CALIBRE_TITLE
}  CATA;

typedef enum CATG
{
   CALIBRE_DB_WORDS = 0,
   CALIBRE_DB_SENTENCE
}  CATG;

typedef enum CACMD
{
   CALIBRE_CMD_NONE = 0,
   CALIBRE_CMD_MOVE,
   CALIBRE_CMD_COPY,
   CALIBRE_CMD_DELETE,
   CALIBRE_CMD_ADD,

   NUM_CALIBRE_CMDS
}  CACMD;

typedef enum CALOG
{
   CALIBRE_LOG_ERRORS = 0,
   CALIBRE_LOG_PROGRESS,
   CALIBRE_LOG_NEW_AUTHOR,
   CALIBRE_LOG_DUPLICATES,
   CALIBRE_LOG_NEW_TITLE,
   CALIBRE_LOG_BAD_FORMAT,

   NUM_CALIBRE_LOGS
}  CALOG;

typedef enum CADB_CC
{
   CALIBREDB_CC_OKEE       =  0,       // OKee completion
   CALIBREDB_CC_GEN_ERROR  = -1,       // Generic error
   CALIBREDB_CC_NO_CSV     = -2,       // No CSV Database found
   CALIBREDB_CC_NO_IDS     = -3        // ID not found
}  CADB_CC;

typedef struct EPUB
{
   EBOOK    tFormat;
   char    *pcFormat;
}  EPUB;

typedef struct LOGLUT
{
   CALOG    tLog;
   int      iNvmIdFlag;
   int      iNvmIdPath;
}  LOGLUT;



#endif // !defined(_CALIBREFILES_H_)
