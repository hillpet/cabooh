@echo off
echo.
echo Distribute CaBooh.exe and calibredbdummy.exe

if exist Release\Cabooh.exe goto copycabooh
echo Cabooh not found
goto end

:copycabooh
echo copy Cabooh to WinApps
if exist D:\EchoStar\Almelo\*.* goto echostar

:cvb_patrn
echo Location: CVB Patrn
echo.
copy /Y Release\Cabooh.exe C:\WinApps\Calibre\Cabooh.exe >NUL

if exist CalibreDummyDb\Release\CalibreDB.exe goto copydummy
echo CalibreDB not found
goto end

:copydummy
echo copy CalibreDbDummy to CabooH Release
copy /Y CalibreDummyDb\Release\CalibreDB.exe Release\calibredbdummy.exe >NUL

echo copy CalibreDbDummy to CabooH Debug
copy /Y CalibreDummyDb\Release\CalibreDB.exe Debug\calibredbdummy.exe >NUL

echo copy CalibreDbDummy to WinApps
copy /Y CalibreDummyDb\Release\CalibreDB.exe C:\WinApps\Calibre\calibredbdummy.exe >NUL
goto end

:echostar
echo Location: EchoStar
echo.
copy /Y Release\Cabooh.exe D:\WinApps\Calibre\Cabooh.exe >NUL

if exist CalibreDummyDb\Release\CalibreDB.exe goto copydummy
echo CalibreDB not found
goto end

:copydummy
echo copy CalibreDbDummy to CabooH Release
copy /Y CalibreDummyDb\Release\CalibreDB.exe Release\calibredbdummy.exe >NUL

echo copy CalibreDbDummy to CabooH Debug
copy /Y CalibreDummyDb\Release\CalibreDB.exe Debug\calibredbdummy.exe >NUL

echo copy CalibreDbDummy to WinApps
copy /Y CalibreDummyDb\Release\CalibreDB.exe D:\WinApps\Calibre\calibredbdummy.exe >NUL


:end
echo.
echo.
pause
