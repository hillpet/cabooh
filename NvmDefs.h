/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  NvmDefs.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define the NVM storage members
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       06 Jun 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_NVMDEFS_H_)
#define _NVMDEFS_H_

enum
{
   NVM_VERSION = 0,
   NVM_INITIALIZED,              // mark if NVM was bad !
   NVM_NAME_APPS_DIR,
   NVM_NAME_DATABASE_FILE,
   NVM_NAME_DATABASE_DIR,
   NVM_NAME_INPUT_FILE,
   NVM_NAME_INPUT_DIR,
   NVM_AUTHOR_THOLD,
   NVM_TITLE_THOLD,
   NVM_WORD_THOLD,
   NVM_LETTER_THOLD,

   NVM_NAME_USER_DIR,
   NVM_NAME_USER_TEMP,
   NVM_NAME_USER_LIST,
   //
   NVM_CHECKBOX_DEEPSCAN,
   NVM_CHECKBOX_DUP,
   NVM_CHECKBOX_NEW,
   NVM_CHECKBOX_CVS,
   NVM_CHECKBOX_BAD,
   NVM_CHECKBOX_DEL,
   NVM_CHECKBOX_ADD,
   NVM_CHECKBOX_DIRECTORY,
   NVM_NAME_MOVETO_DIR,
   NVM_CHECKBOX_GEN_LOG,
   NVM_CHECKBOX_LOG_ERRORS,
   NVM_CHECKBOX_LOG_APPEND,
   NVM_CHECKBOX_LOG_PROGRESS,
   NVM_CHECKBOX_LOG_AUTHORS,
   NVM_CHECKBOX_LOG_TITLES,
   NVM_CHECKBOX_LOG_BAD,
   NVM_CHECKBOX_LOG_DUPLICATES,
   NVM_CALIBRE_QUIETMODE,
   NVM_CALIBRE_EXECMODE,
   NVM_PATH_LOG_ERRORS,
   NVM_PATH_LOG_PROGRESS,
   NVM_PATH_LOG_AUTHORS,
   NVM_PATH_LOG_DUPLICATES,
   NVM_PATH_LOG_TITLES,
   NVM_PATH_LOG_BAD,
   //
   NVM_DB_VERSION,
   NUM_WNVMS
};
#endif // !defined(_NVMDEFS_H_)
