/*  (c) Copyright:  2011  PATRN.NL, Confidential Data
**
**  $Workfile:          CalPageGeneric.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       17 Jul 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#pragma once

#define CALL_MEMBER_RFN(OBJ, MEMBERPTR)   ((OBJ).*(MEMBERPTR))
#define CALL_MEMBER_PFN(OBJ, MEMBERPTR)  (((OBJ)->*(MEMBERPTR)))

class CCalPageGeneric : public CPropertyPage
{
   DECLARE_DYNCREATE(CCalPageGeneric)

// Construction
public:
   CCalPageGeneric                        ();
  ~CCalPageGeneric                        ();

// Dialog Data
   typedef  int (CCalPageGeneric::*CallbackMemberFn)(int);
   enum { IDD = IDD_PROP_CAL_GENERIC };

protected:
   virtual void DoDataExchange            (CDataExchange* pDX);
   virtual BOOL OnInitDialog              ();

// Overrides
   //{{AFX_VIRTUAL(CCalPageWildcard)
public:
   virtual void OnOK                      ();
   virtual void OnCancel                  ();
// Implementation
protected:
   HICON m_hIcon;
   CMyCommandLineInfo         clCmdLineOptions;

   // Methods
   int               CheckCallbacks       (int);
   void              CreateAuthorList     ();
   void              CreateProgressBar    ();
   void              CreateTitlesList     (int);
   void              ExportBooks          (CString *, CString *);
   void              ParseAuthors         (CString *);
   void              ParseBooks           (CString *);
   void              SetupLogs            (void);
   void              StatusAddText        (char *);
   void              UpdateDialogScreen   ();
   void              UpdateProgressBar    (int);
   void              UpdateVariables      ();
   //
   int               CB_ParseInput        (int);
   int               CB_Exit              (int);

   // Generated message map functions
   afx_msg void      OnDropFiles          (HDROP);
   afx_msg void      OnPaint              ();
   afx_msg HCURSOR   OnQueryDragIcon      ();
   afx_msg void      OnSysCommand         (UINT, LPARAM);
   //DECLARE_MESSAGE_MAP()
public:
   afx_msg void      OnBnClickedBrowseDatabase();
   afx_msg void      OnBnClickedBrowseInput();
   afx_msg void      OnBnClickedButtonExport();
   afx_msg void      OnBnClickedButtonParseCsv();
   afx_msg void      OnBnClickedParseInput();
   afx_msg void      OnBnClickedList();
   afx_msg void      OnBnClickedCheckboxDeepScan();
   afx_msg void      OnBnClickedCheckboxDuplicate();
   afx_msg void      OnBnClickedCheckboxNew();
   afx_msg void      OnBnClickedCheckboxOld();
   afx_msg void      OnBnClickedCheckboxBad();
   afx_msg void      OnBnClickedCheckboxDirectory();
   afx_msg void      OnBnClickedCheckboxGenLog();
   afx_msg void      OnBnClickedCheckboxLogAppend();
   afx_msg void      OnBnClickedCheckboxLogProgress();
   afx_msg void      OnBnClickedCheckboxLogAuthors();
   afx_msg void      OnBnClickedCheckboxLogTitles();
   afx_msg void      OnBnClickedCheckboxLogBad();
   afx_msg void      OnBnClickedCheckboxLogDuplicates();
   afx_msg void      OnComboChangeAuthors();
   afx_msg void      OnComboChangeTitles();
   //
   afx_msg LRESULT   OnRcvCalibreOkee           (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvCalibreBooksReady     (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvCalibreDbReady        (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvCalibreFreeReady      (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvCalibreExportReady    (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvCalibreShowProgress   (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvCalibreMessage        (WPARAM, LPARAM);
   afx_msg LRESULT   OnRcvClickedParseInput     (WPARAM, LPARAM);
   DECLARE_MESSAGE_MAP()

private:
   CCalibreApp      *pclApp;
   // Storage container
   CNvmStorage      *pclNvm;
   CCalCmd          *pclCmd;
   CBOOK            *pclBook;
   CLog             *pclLog;
   //
   CallbackMemberFn  pfCallback;
   //
   BOOL              fDatabaseParsed;
   //
   CReadOnlyEdit     clEditDir;
   CReadOnlyEdit     clEditInput;
   CReadOnlyEdit     clEditParseAuthorCount;
   CReadOnlyEdit     clEditParseTitleCount;
   CReadOnlyEdit     clEditParseNoMatchCount;
   CReadOnlyEdit     clEditParseDuplicateCount;
   CReadOnlyEdit     clEditParseAuthorThold;
   CReadOnlyEdit     clEditParseTitleThold;
   CReadOnlyEdit     clEditParseWordThold;
   CReadOnlyEdit     clEditParseLetterThold;
   CReadOnlyEdit     clEditList;
   CReadOnlyEdit     clAuthorStatus;
   CReadOnlyEdit     clTitleStatus;

   CProgressCtrl     clBarProgress;
   CComboBox         clComboAuthors;
   CComboBox         clComboTitles;
   CButton           clCheckboxDeepScan;
   CButton           clCheckboxDup;
   CButton           clCheckboxNew;
   CButton           clCheckboxOld;
   CButton           clCheckboxBad;
   CButton           clCheckboxDirectory;
   CButton           clCheckboxGenLog;
   CButton           clCheckboxLogAppend;
   CButton           clCheckboxLogProgress;
   CButton           clCheckboxLogAuthors;
   CButton           clCheckboxLogTitles;
   CButton           clCheckboxLogBad;
   CButton           clCheckboxLogDuplicates;

private:
   CMyCommandLineInfo clCmdInfo;
   CAutoFont        *pclFontNum;
   CAutoFont        *pclFontTxt;
};
