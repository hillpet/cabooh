/*  (c)2011 Patrn.nl
**
**  $Workfile:   CalPageGeneric.cpp $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       06 Jun 2011
**
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"
#include "CalPageGeneric.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


extern const char *pcLogon             = "CaBooH, a Calibre eBook helper";
extern const char *pcPatrn             = "(C)Copyright 2011..2012: www.patrn.nl";
extern const char *pcAuthorSubstitute  = ".'_,";
extern const char *pcTitleSubstitute   = ".'_,:-/?\"";


IMPLEMENT_DYNCREATE(CCalPageGeneric, CPropertyPage)

//
//  Function:  CCalPageGeneric()
//  Purpose:   Constructor
//
//  Parms:
//  Returns:
//
CCalPageGeneric::CCalPageGeneric() : CPropertyPage(CCalPageGeneric::IDD)
{
   //
   // To get access to theApp:
   //
   pclApp = (CCalibreApp *) AfxGetApp();
   //
   m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
   //
   fDatabaseParsed = FALSE;
   pfCallback      = NULL;
   //
   pclNvm   = new(CNvmStorage);
   pclCmd   = new(CCalCmd);
   pclBook  = new(CBOOK);
   pclLog   = new(CLog);

   pclFontNum = new(CAutoFont)("Courier New");
   pclFontTxt = new(CAutoFont)("Tahoma");

   pclFontNum->SetHeight(CALIBRE_NUM_FONT_SIZE);
   pclFontTxt->SetHeight(CALIBRE_TXT_FONT_SIZE);
}

//
//  Function:  ~CCalPageGeneric()
//  Purpose:   Destructor
//  Parms:
//  Returns:
//
CCalPageGeneric::~CCalPageGeneric()
{
   delete(pclNvm);
   delete(pclCmd);
   delete(pclBook);
   delete(pclLog);
   //
   delete(pclFontNum);
   delete(pclFontTxt);
}

//
//  Function:  DoDataExchange
//  Purpose:   Get dialog data
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::DoDataExchange(CDataExchange* pDX)
{
   CPropertyPage::DoDataExchange(pDX);

   DDX_Control(pDX, IDC_EDIT_DIR,                  clEditDir);
   DDX_Control(pDX, IDC_EDIT_INPUT,                clEditInput);
   DDX_Control(pDX, IDC_EDIT_PARSE_AUTHOR_COUNT,   clEditParseAuthorCount);
   DDX_Control(pDX, IDC_EDIT_PARSE_TITLE_COUNT,    clEditParseTitleCount);
   DDX_Control(pDX, IDC_EDIT_PARSE_NOMATCH_COUNT,  clEditParseNoMatchCount);
   DDX_Control(pDX, IDC_EDIT_PARSE_DUPLICATE_COUNT,clEditParseDuplicateCount);
   DDX_Control(pDX, IDC_EDIT_PARSE_AUTHOR_THOLD,   clEditParseAuthorThold);
   DDX_Control(pDX, IDC_EDIT_PARSE_TITLE_THOLD,    clEditParseTitleThold);
   DDX_Control(pDX, IDC_EDIT_PARSE_WORD_THOLD,     clEditParseWordThold);
   DDX_Control(pDX, IDC_EDIT_PARSE_LETTER_THOLD,   clEditParseLetterThold);
   DDX_Control(pDX, IDC_EDIT_LIST,                 clEditList);
   DDX_Control(pDX, IDC_PARSE_PROGRESS,            clBarProgress);
   DDX_Control(pDX, IDC_COMBO_AUTHORS,             clComboAuthors);
   DDX_Control(pDX, IDC_COMBO_TITLES,              clComboTitles);
   DDX_Control(pDX, IDC_CHECK_DEEPSCAN,            clCheckboxDeepScan);
   DDX_Control(pDX, IDC_CHECK_DUP,                 clCheckboxDup);
   DDX_Control(pDX, IDC_CHECK_NEW,                 clCheckboxNew);
   DDX_Control(pDX, IDC_CHECK_OLD,                 clCheckboxOld);
   DDX_Control(pDX, IDC_CHECK_BAD,                 clCheckboxBad);
   DDX_Control(pDX, IDC_CHECK_DIRECTORY,           clCheckboxDirectory);
   DDX_Control(pDX, IDC_CHECK_GEN_LOG,             clCheckboxGenLog);
   DDX_Control(pDX, IDC_CHECK_LOG_APPEND,          clCheckboxLogAppend);
   DDX_Control(pDX, IDC_CHECK_LOG_PROGRESS,        clCheckboxLogProgress);
   DDX_Control(pDX, IDC_CHECK_LOG_AUTHORS,         clCheckboxLogAuthors);
   DDX_Control(pDX, IDC_CHECK_LOG_TITLES,          clCheckboxLogTitles);
   DDX_Control(pDX, IDC_CHECK_LOG_BAD,             clCheckboxLogBad);
   DDX_Control(pDX, IDC_CHECK_LOG_DUPLICATES,      clCheckboxLogDuplicates);
   DDX_Control(pDX, IDC_AUTHOR_STATUS,             clAuthorStatus);
   DDX_Control(pDX, IDC_TITLE_STATUS,              clTitleStatus);
}

BEGIN_MESSAGE_MAP(CCalPageGeneric, CPropertyPage)
   ON_WM_SYSCOMMAND()
   ON_WM_PAINT()
   ON_WM_DROPFILES()
   ON_WM_QUERYDRAGICON()
   //}}AFX_MSG_MAP
   ON_BN_CLICKED(IDC_BUTTON_BROWSE_AUTHOR,         OnBnClickedBrowseDatabase)
   ON_BN_CLICKED(IDC_BUTTON_BROWSE_INPUT,          OnBnClickedBrowseInput)
   ON_BN_CLICKED(IDC_BUTTON_PARSE_CSV,             OnBnClickedButtonParseCsv)
   ON_BN_CLICKED(IDC_BUTTON_PARSE_INPUT,           OnBnClickedParseInput)
   ON_BN_CLICKED(IDC_BUTTON_LIST,                  OnBnClickedList)
   ON_BN_CLICKED(IDC_BUTTON_EXPORT,                OnBnClickedButtonExport)
   //
   ON_BN_CLICKED(IDC_CHECK_DEEPSCAN,               OnBnClickedCheckboxDeepScan)
   ON_BN_CLICKED(IDC_CHECK_DUP,                    OnBnClickedCheckboxDuplicate)
   ON_BN_CLICKED(IDC_CHECK_NEW,                    OnBnClickedCheckboxNew)
   ON_BN_CLICKED(IDC_CHECK_OLD,                    OnBnClickedCheckboxOld)
   ON_BN_CLICKED(IDC_CHECK_BAD,                    OnBnClickedCheckboxBad)
   ON_BN_CLICKED(IDC_CHECK_DIRECTORY,              OnBnClickedCheckboxDirectory)
   ON_BN_CLICKED(IDC_CHECK_GEN_LOG,                OnBnClickedCheckboxGenLog)
   ON_BN_CLICKED(IDC_CHECK_LOG_APPEND,             OnBnClickedCheckboxLogAppend)
   ON_BN_CLICKED(IDC_CHECK_LOG_PROGRESS,           OnBnClickedCheckboxLogProgress)
   ON_BN_CLICKED(IDC_CHECK_LOG_AUTHORS,            OnBnClickedCheckboxLogAuthors)
   ON_BN_CLICKED(IDC_CHECK_LOG_TITLES,             OnBnClickedCheckboxLogTitles)
   ON_BN_CLICKED(IDC_CHECK_LOG_BAD,                OnBnClickedCheckboxLogBad)
   ON_BN_CLICKED(IDC_CHECK_LOG_DUPLICATES,         OnBnClickedCheckboxLogDuplicates)
   //
   ON_CBN_SELCHANGE(IDC_COMBO_AUTHORS,             OnComboChangeAuthors)
   ON_CBN_SELCHANGE(IDC_COMBO_TITLES,              OnComboChangeTitles)
   //
   //  Private WM Messages
   //
   ON_MESSAGE(WM_CALIBRE_OKEE,                     OnRcvCalibreOkee)
   ON_MESSAGE(WM_CALIBRE_DB_READY,                 OnRcvCalibreDbReady)
   ON_MESSAGE(WM_CALIBRE_BOOKS_READY,              OnRcvCalibreBooksReady)
   ON_MESSAGE(WM_CALIBRE_FREE_READY,               OnRcvCalibreFreeReady)
   ON_MESSAGE(WM_CALIBRE_EXPORT_READY,             OnRcvCalibreExportReady)
   ON_MESSAGE(WM_CALIBRE_SHOW_PROGRESS,            OnRcvCalibreShowProgress)
   ON_MESSAGE(WM_CALIBRE_MESSAGE,                  OnRcvCalibreMessage)
   //
   ON_MESSAGE(WM_CALIBRE_BOOKS_RESCAN,             OnRcvClickedParseInput)
END_MESSAGE_MAP()


/* ======   Local   Functions separator ===========================================
void ____Message_Handlers____(){}
==============================================================================*/

//
//  Function:  OnBnClickedBrowseDatabase
//  Purpose:   Handle the browse for Author input file
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedBrowseDatabase()
{
   CString  clFile, clDir;
   char    *pcExt = "Exported Calibre eBooks(*.csv)|*.csv|All Files (*.*)|*.*||";

   UpdateVariables();               // dialog window --> Store in NVM
   //
   pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clFile);
   //
#ifdef   FEATURE_PROMPT_EMPTY_CSV
   if(pclApp->BrowseFile(&clFile, &clDir, pcExt) == TRUE)
   {
      if(clFile.GetLength() > 0)
      {
         // we have a Author input file
         pclNvm->NvmPut(NVM_NAME_DATABASE_FILE, clFile);
         pclNvm->NvmPut(NVM_NAME_DATABASE_DIR,  clDir);
         pclNvm->NvmPut(NVM_NAME_USER_DIR,      clDir);
         //
         GetDlgItem(IDC_BUTTON_PARSE_INPUT)->EnableWindow(TRUE);
         ParseAuthors(&clFile);
      }
      UpdateDialogScreen();
   }
   else
   {
      clFile.Format(_T("Use an empty database ?\n\n"));
      clFile += "Are you sure ?\n\n";
      //
      int iRet = AfxMessageBox(clFile, MB_YESNOCANCEL|MB_ICONEXCLAMATION);
      switch(iRet)
      {
         case IDYES:
            clFile.Empty();
            pclNvm->NvmPut(NVM_NAME_DATABASE_FILE, clFile);
            GetDlgItem(IDC_BUTTON_PARSE_INPUT)->EnableWindow(TRUE);
            ParseAuthors(&clFile);
            break;

         default:
         case IDNO:
            break;
      }
      UpdateDialogScreen();
   }
#else    //FEATURE_PROMPT_EMPTY_CSV
   if(pclApp->BrowseFile(&clFile, &clDir, pcExt) == TRUE)
   {
      if(clFile.GetLength() > 0)
      {
         // we have a Author input file
         pclNvm->NvmPut(NVM_NAME_DATABASE_FILE, clFile);
         pclNvm->NvmPut(NVM_NAME_DATABASE_DIR,  clDir);
         pclNvm->NvmPut(NVM_NAME_USER_DIR,      clDir);
         //
         GetDlgItem(IDC_BUTTON_PARSE_INPUT)->EnableWindow(TRUE);
         ParseAuthors(&clFile);
      }
      UpdateDialogScreen();
   }
   else
   {
      // We have an empty CSV database filename
      clFile.Empty();
      pclNvm->NvmPut(NVM_NAME_DATABASE_FILE, clFile);
      GetDlgItem(IDC_BUTTON_PARSE_INPUT)->EnableWindow(TRUE);
      ParseAuthors(&clFile);
      UpdateDialogScreen();
   }
#endif   //FEATURE_PROMPT_EMPTY_CSV
}

//
//  Function:  OnBnClickedBrowseInput
//  Purpose:   Browse for eBook files, either a DIR list file or a FOLDER.
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedBrowseInput()
{
   BOOL     fEnabled;
   CString  clFile, clDir;
   char    *pcExt = "eBook DIR list (*.log;*.txt;*.lst)|*.log;*.txt;*.lst|All Files (*.*)|*.*||";

   UpdateVariables();               // dialog window --> Store in NVM

   pclNvm->NvmGet(NVM_CHECKBOX_DIRECTORY, &fEnabled);
   if(fEnabled)
   {
      // Operation on directories: prompt directory
      pclNvm->NvmGet(NVM_NAME_INPUT_DIR, &clDir);
      if( pclApp->BrowseFolder(&clDir, "Select Folder with the eBooks ...") )
      {
         pclNvm->NvmPut(NVM_NAME_INPUT_DIR,  clDir);
      }
   }
   else
   {
      // Operation on files: prompt filename
      pclNvm->NvmGet(NVM_NAME_INPUT_FILE, &clFile);
      if(pclApp->BrowseFile(&clFile, &clDir, pcExt) == TRUE)
      {
         // we have a raw input file
         pclNvm->NvmPut(NVM_NAME_INPUT_FILE, clFile);
         pclNvm->NvmPut(NVM_NAME_INPUT_DIR,  clDir);
      }
   }
   UpdateDialogScreen();
}

//
//  Function:  OnBnClickedButtonExport
//  Purpose:   Handle the Import CSV DB button
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedButtonExport()
{
   CString clIn, clOut;
   char    *pcExt = "Exported CSV file(*.csv)|*.csv|All Files (*.*)|*.*||";

   UpdateVariables();               // dialog window --> Store in NVM
   //
   // Operation on directories: prompt directories
   //
   pclNvm->NvmGet(NVM_NAME_INPUT_DIR, &clIn);
   if( pclApp->BrowseFolder(&clIn, "Select Folder with Calibre database...") )
   {
      pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clOut);
      if(pclApp->BrowseFile(&clOut, NULL, pcExt) == TRUE)
      {
         //
         // Execute calibredb command to import the CSV database
         //
         ExportBooks(&clIn, &clOut);
      }
   }
}

//
//  Function:  OnBnClickedList
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedList()
{
   HANDLE   hProcess;
   CString  clName, clStr;
   CAuthor  clAuthor;
   const char *pcList = "ListBook";

   UpdateVariables();               // dialog window --> Store in NVM
   //
   // Create a list to a temp file (name = @NVM_NAME_USER_LIST)
   //
   clAuthor.AUTHOR_CreateDatabaseList(pcList);
   pclNvm->NvmGet(NVM_NAME_USER_LIST, &clName);

   clStr.Format(_T("notepad.exe %s"), clName);
   hProcess = CVB_ExecuteCommand(&clStr);
   CVB_CheckProcessCompletion(hProcess, TRUE);
}

//
//  Function:  OnBnClickedParseInput
//  Purpose:   Handle the parse raw input file
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedParseInput()
{
   BOOL     fEnabled;
   CString  clStr;

   UpdateVariables();               // dialog window --> Store in NVM
   //
   if(fDatabaseParsed == FALSE)
   {
      pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clStr);
      ParseAuthors(&clStr);
      //
      // Wait until the background thread has completed the Calibre DB parsing
      //
      pfCallback = &CCalPageGeneric::CB_ParseInput;
   }
   else
   {
      pclNvm->NvmGet(NVM_CHECKBOX_DIRECTORY, &fEnabled);
      if(fEnabled) pclNvm->NvmGet(NVM_NAME_INPUT_DIR,  &clStr);
      else         pclNvm->NvmGet(NVM_NAME_INPUT_FILE, &clStr);
      //
      // Parse the input file for authors and titles (Books)
      //
      ParseBooks(&clStr);
   }
   UpdateDialogScreen();            // Update the fields in the dialog
}

//
//  Function:  OnBnClickedCheckboxDeepScan
//  Purpose:   Handle the DEEPSCAN checkbox
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedCheckboxDeepScan()
{
   if (clCheckboxDeepScan.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_DEEPSCAN, TRUE);
   else                                              pclNvm->NvmPut(NVM_CHECKBOX_DEEPSCAN, FALSE);
}

//
//  Function:  OnBnClickedCheckboxDuplicate
//  Purpose:   Handle the DUPLICATE checkbox
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedCheckboxDuplicate()
{
   if (clCheckboxDup.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_DUP, TRUE);
   else                                         pclNvm->NvmPut(NVM_CHECKBOX_DUP, FALSE);
}

//
//  Function:  OnBnClickedCheckboxNew
//  Purpose:   Handle the NEW checkbox
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedCheckboxNew()
{
   if (clCheckboxNew.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_NEW, TRUE);
   else                                         pclNvm->NvmPut(NVM_CHECKBOX_NEW, FALSE);
}

//
//  Function:  OnBnClickedCheckboxOld
//  Purpose:   Handle the OLD checkbox
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedCheckboxOld()
{
   if (clCheckboxOld.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_CVS, TRUE);
   else                                         pclNvm->NvmPut(NVM_CHECKBOX_CVS, FALSE);
}

//
//  Function:  OnBnClickedCheckboxBad
//  Purpose:   Handle the BAD checkbox
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedCheckboxBad()
{
   if (clCheckboxBad.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_BAD, TRUE);
   else                                         pclNvm->NvmPut(NVM_CHECKBOX_BAD, FALSE);
}

//
//  Function:  OnBnClickedCheckboxDirectory
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxDirectory()
{
   if (clCheckboxDirectory.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_DIRECTORY, TRUE);
   else                                               pclNvm->NvmPut(NVM_CHECKBOX_DIRECTORY, FALSE);
   //
   UpdateDialogScreen();
}

//
//  Function:  OnBnClickedCheckboxGenLog
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxGenLog()
{
   if (clCheckboxGenLog.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_GEN_LOG, TRUE);
   else                                            pclNvm->NvmPut(NVM_CHECKBOX_GEN_LOG, FALSE);
}

//
//  Function:  OnBnClickedCheckboxLogAppend
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxLogAppend()
{
   if (clCheckboxLogAppend.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_LOG_APPEND, TRUE);
   else                                               pclNvm->NvmPut(NVM_CHECKBOX_LOG_APPEND, FALSE);
}

//
//  Function:  OnBnClickedCheckboxLogProgress
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxLogProgress()
{
   if (clCheckboxLogProgress.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_LOG_PROGRESS, TRUE);
   else                                                 pclNvm->NvmPut(NVM_CHECKBOX_LOG_PROGRESS, FALSE);
}

//
//  Function:  OnBnClickedCheckboxLogAuthors
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxLogAuthors()
{
   if (clCheckboxLogAuthors.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_LOG_AUTHORS, TRUE);
   else                                                pclNvm->NvmPut(NVM_CHECKBOX_LOG_AUTHORS, FALSE);
}

//
//  Function:  OnBnClickedCheckboxLogTitles
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxLogTitles()
{
   if (clCheckboxLogTitles.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_LOG_TITLES, TRUE);
   else                                               pclNvm->NvmPut(NVM_CHECKBOX_LOG_TITLES, FALSE);
}

//
//  Function:  OnBnClickedCheckboxLogBad
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxLogBad()
{
   if (clCheckboxLogBad.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_LOG_BAD, TRUE);
   else                                            pclNvm->NvmPut(NVM_CHECKBOX_LOG_BAD, FALSE);
}

//
//  Function:  OnBnClickedCheckboxLogDuplicates
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnBnClickedCheckboxLogDuplicates()
{
   if (clCheckboxLogDuplicates.GetCheck() == BST_CHECKED) pclNvm->NvmPut(NVM_CHECKBOX_LOG_DUPLICATES, TRUE);
   else                                                   pclNvm->NvmPut(NVM_CHECKBOX_LOG_DUPLICATES, FALSE);
}

//
//  Function:   OnBnClickedButtonParseCsv
//  Purpose:    Read and parse the CSV Calibre export file
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnBnClickedButtonParseCsv()
{
   CString  clFile;

   UpdateVariables();               // dialog window --> Store in NVM

   pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clFile);
   if(clFile.GetLength() > 0)
   {
      // we have a Author input file
      ParseAuthors(&clFile);
   }
   UpdateDialogScreen();
}

//
//  Function:  OnComboChangeAuthors
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnComboChangeAuthors()
{
   CString  clStr;
   CBOOK   *pclA, *pclT;
   int      iAuthor = clComboAuthors.GetCurSel();

   pclA = pclBook->DB_GetAuthorInfo(iAuthor, &clStr);
   if(pclA)
   {
      clAuthorStatus.SetWindowText(clStr);
      pclT  = pclBook->DB_GetTitleInfo(0, pclA, &clStr);
      if(pclT)
      {
         clTitleStatus.SetWindowText(clStr);
      }
      else
      {
         clTitleStatus.SetWindowText("---");
      }
      CreateTitlesList(iAuthor);
   }
   else
   {
      clAuthorStatus.SetWindowText("---");
   }
}

//
//  Function:  OnComboChangeTitles
//  Purpose:
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::OnComboChangeTitles()
{
   CString  clStr;
   CBOOK   *pclA, *pclT;
   int      iAuthor = clComboAuthors.GetCurSel();
   int      iTitle  = clComboTitles.GetCurSel();

   pclA = pclBook->DB_GetAuthorInfo(iAuthor, NULL);
   if(pclA)
   {
      pclT = pclBook->DB_GetTitleInfo(iTitle, pclA, &clStr);
      if(pclT)
      {
         clTitleStatus.SetWindowText(clStr);
      }
      else
      {
         clTitleStatus.SetWindowText("---");
      }
   }
}

//
//  Function:  OnDropFiles
//  Purpose:   If you drop a file into the window, this function will handle the drop.
//
//  Parms:     HDROP
//  Returns:   void
//
void CCalPageGeneric::OnDropFiles(HDROP hDropInfo)
{
   UINT     iCh;
   char    *pcBuffer;
   CString  clStr;

   //
   // DragQueryFile(hDropInfo, iFile, lpscName, iNr)
   //
   // hDrop    [in]  Identifier of the structure that contains the file names of the dropped files.
   // iFile    [in]  Index of the file to query. If the value of this parameter is 0xFFFFFFFF, DragQueryFile returns a count of the files dropped.
   //                If the value of this parameter is between zero and the total number of files dropped, DragQueryFile copies the file name
   //                with the corresponding value to the buffer pointed to by the lpszFile parameter.
   // lpszName [out] The address of a buffer that receives the file name of a dropped file when the function returns.
   //                This file name is a null-terminated string. If this parameter is NULL, DragQueryFile returns the required size,
   //                in characters, of this buffer.
   // iCh            The size, in characters, of the lpszFile buffer.
   // Return Value   A non-zero value indicates a successful call.
   //                When the function copies a file name to the buffer, the return value is a count of the characters copied, not including the
   //                terminating null character.
   //                If the index value is 0xFFFFFFFF, the return value is a count of the dropped files.
   //                Note that the index variable itself returns unchanged, and therefore remains 0xFFFFFFFF.
   //                If the index value is between zero and the total number of dropped files, and the lpszFile buffer address is NULL, the return
   //                value is the required size, in characters, of the buffer, not including the terminating null character.
   //
   iCh = DragQueryFile(hDropInfo, 0, NULL, 0);

   if(iCh)
   {
      iCh += 2;
      pcBuffer = (char *) malloc(iCh);
      if(pcBuffer)
      {
         iCh = DragQueryFile(hDropInfo, 0, pcBuffer, iCh);
         //
         // Handle the file loading
         //
         free(pcBuffer);
      }
   }
}

//
//  Function:  OnInitDialog
//  Purpose:   Init all dialog structures
//
//  Parms:     void
//  Returns:   void
//
BOOL CCalPageGeneric::OnInitDialog()
{
   CString  clStr, clCmd;
   BOOL     fEnabled;

   CPropertyPage::OnInitDialog();

   pclCmd->tMyDlgWnd = this->GetSafeHwnd();
   //
   AfxBeginThread(CalibreThread, 0, THREAD_PRIORITY_NORMAL);
   //
   DragAcceptFiles(TRUE);
   CreateProgressBar();
   //
   // Add "About..." menu item to system menu.
   // IDM_ABOUTBOX must be in the system command range.
   //
   ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
   ASSERT(IDM_ABOUTBOX < 0xF000);

   CMenu* pSysMenu = GetSystemMenu(FALSE);
   if (pSysMenu != NULL)
   {
      CString strAboutMenu;
      strAboutMenu.LoadString(IDS_ABOUTBOX);
      if (!strAboutMenu.IsEmpty())
      {
         pSysMenu->AppendMenu(MF_SEPARATOR);
         pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
      }
   }
   //
   // Set the icon for this dialog.  The framework does this automatically
   //  when the application's main window is not a dialog
   //
   SetIcon(m_hIcon, TRUE);       // Set big icon
   SetIcon(m_hIcon, FALSE);      // Set small icon

   clStr.Format(_T("%s  %s (%s) - %s" CRLF), CVB_GetTimeDateStamp(), pcLogon, CALIBRE_VERSION, pcPatrn);
   StatusAddText(clStr.GetBuffer());
   clCmdInfo.GetCommandline(&clCmd);
   if(clCmd.GetLength())   clStr.Format(_T("%s  Commandline:%s" CRLF), CVB_GetTimeDateStamp(), clCmd);
   else                    clStr.Format(_T("%s  No commandline parameters" CRLF), CVB_GetTimeDateStamp());
   StatusAddText(clStr.GetBuffer());
   //
   pclNvm->NvmGet(NVM_INITIALIZED, &fEnabled);
   if(fEnabled)
   {
      // NVM was bad: defaults were reloaded !
      clStr.Format(_T("%s  Configuration has been reset !" CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }

   //
   // Check Commandline stuff
   //
   pclNvm->NvmPut(NVM_CALIBRE_QUIETMODE, clCmdInfo.IsSwitchPresent(CMDLINE_QUIET));
   pclNvm->NvmPut(NVM_CALIBRE_EXECMODE,  clCmdInfo.IsSwitchPresent(CMDLINE_REAL));
   //
   if( clCmdInfo.IsSwitchPresent(CMDLINE_AUTHOR) )
   {
      clCmdInfo.GetSwitchValue(CMDLINE_AUTHOR, &clStr);
      pclNvm->NvmPut(NVM_AUTHOR_THOLD, clStr);
   }
   if( clCmdInfo.IsSwitchPresent(CMDLINE_TITLE) )
   {
      clCmdInfo.GetSwitchValue(CMDLINE_TITLE, &clStr);
      pclNvm->NvmPut(NVM_TITLE_THOLD, clStr);
   }
   if( clCmdInfo.IsSwitchPresent(CMDLINE_WORDS) )
   {
      clCmdInfo.GetSwitchValue(CMDLINE_WORDS, &clStr);
      pclNvm->NvmPut(NVM_WORD_THOLD, clStr);
   }
   if( clCmdInfo.IsSwitchPresent(CMDLINE_LETTERS) )
   {
      clCmdInfo.GetSwitchValue(CMDLINE_LETTERS, &clStr);
      pclNvm->NvmPut(NVM_LETTER_THOLD, clStr);
   }

   if( clCmdInfo.IsSwitchPresent(CMDLINE_DATABASE) )
   {
      clCmdInfo.GetSwitchValue(CMDLINE_DATABASE, &clStr);
      pclNvm->NvmPut(NVM_NAME_DATABASE_FILE, clStr);
   }

   //
   // Enable buttons where necessary
   //
   pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clStr);
   if(clStr.GetLength() > 0)
   {
      GetDlgItem(IDC_BUTTON_PARSE_INPUT)->EnableWindow(TRUE);
   }
   else
   {
      GetDlgItem(IDC_BUTTON_PARSE_INPUT)->EnableWindow(FALSE);
   }
   //
   pclNvm->NvmGet(NVM_CHECKBOX_DUP, &fEnabled);
   if(fEnabled)   clCheckboxDup.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_NEW, &fEnabled);
   if(fEnabled)   clCheckboxNew.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_CVS, &fEnabled);
   if(fEnabled)   clCheckboxOld.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_BAD, &fEnabled);
   if(fEnabled)   clCheckboxBad.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_DIRECTORY, &fEnabled);
   if(fEnabled)   clCheckboxDirectory.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_GEN_LOG, &fEnabled);
   if(fEnabled)   clCheckboxGenLog.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_LOG_APPEND, &fEnabled);
   if(fEnabled)   clCheckboxLogAppend.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_LOG_PROGRESS, &fEnabled);
   if(fEnabled)   clCheckboxLogProgress.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_LOG_AUTHORS, &fEnabled);
   if(fEnabled)   clCheckboxLogAuthors.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_LOG_TITLES, &fEnabled);
   if(fEnabled)   clCheckboxLogTitles.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_LOG_BAD, &fEnabled);
   if(fEnabled)   clCheckboxLogBad.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_LOG_DUPLICATES, &fEnabled);
   if(fEnabled)   clCheckboxLogDuplicates.SetCheck(TRUE);
   //
   pclNvm->NvmGet(NVM_CHECKBOX_DEEPSCAN, &fEnabled);
   if(fEnabled)   clCheckboxDeepScan.SetCheck(TRUE);
   //
   clEditDir.SetFont(pclFontTxt);
   clEditInput.SetFont(pclFontTxt);
   clEditParseAuthorCount.SetFont(pclFontNum);
   clEditParseTitleCount.SetFont(pclFontNum);
   clEditParseNoMatchCount.SetFont(pclFontNum);
   clEditParseDuplicateCount.SetFont(pclFontNum);
   //
   clEditParseAuthorThold.SetFont(pclFontNum);
   clEditParseTitleThold.SetFont(pclFontNum);
   clEditParseWordThold.SetFont(pclFontNum);
   clEditParseLetterThold.SetFont(pclFontNum);
   //
   clEditList.SetFont(pclFontTxt);
   clEditList.SetBackColor(WHITE);
   clEditList.SetTextColor(BLUE);
   //
   clComboAuthors.ResetContent();
   clComboAuthors.AddString("No authors available yet");
   clComboAuthors.SetCurSel(0);
   //
   clComboTitles.ResetContent();
   clComboTitles.AddString("No book titles available");
   clComboTitles.SetCurSel(0);
   //
   clAuthorStatus.SetFont(pclFontTxt);
   clAuthorStatus.SetWindowText("---");
   clTitleStatus.SetFont(pclFontTxt);
   clTitleStatus.SetWindowText("---");
   //
   // Update LOG file checkboxes for this instance
   //
   SetupLogs();
   //
   UpdateDialogScreen();         // Update the fields in the dialog
   //
   return(TRUE);                 // return TRUE  unless you set the focus to a control
}

//
//  Function:  OnPaint
//  Purpose:   If you add a minimize button to your dialog, you will need the code below
//             to draw the icon.  For MFC applications using the document/view model,
//             this is automatically done for you by the framework.
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnPaint()
{
   if (IsIconic())
   {
      CPaintDC dc(this); // device context for painting

      SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

      // Center icon in client rectangle
      int cxIcon = GetSystemMetrics(SM_CXICON);
      int cyIcon = GetSystemMetrics(SM_CYICON);
      CRect rect;
      GetClientRect(&rect);
      int x = (rect.Width() - cxIcon + 1) / 2;
      int y = (rect.Height() - cyIcon + 1) / 2;

      // Draw the icon
      dc.DrawIcon(x, y, m_hIcon);
   }
   else
   {
      CPropertyPage::OnPaint();
   }
}

//
//  Function:  OnQueryDragIcon
//  Purpose:   The system calls this function to obtain the cursor to display while the user drags
//             the minimized window.
//
//  Parms:
//  Returns:
//
HCURSOR CCalPageGeneric::OnQueryDragIcon()
{
   return static_cast<HCURSOR>(m_hIcon);
}

//
//  Function:  OnSysCommand
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnSysCommand(UINT nID, LPARAM lParam)
{
   CPropertyPage::OnSysCommand(nID, lParam);
}

//
//  Function:  OnOK
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnOK()
{
   UpdateVariables();               // dialog window --> Store in NVM
   CPropertyPage::OnOK();
}

//
//  Function:  OnCancel
//  Purpose:
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::OnCancel()
{
   CPropertyPage::OnCancel();
}



/* ======   Local   Functions separator ===========================================
void ____Private_Msgs____(){}
==============================================================================*/


//
//  Function:  OnRcvCalibreOkee
//  Purpose:   Message from the Calibre Thread
//
//  Parms:     WPARAM =
//             TPARAM =
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvCalibreOkee(WPARAM tWp, LPARAM tLp)
{
   char *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvCalibreDbReady
//  Purpose:   Message from the Calibre Thread: DB acquisition ready
//
//  Parms:     WPARAM =
//             TPARAM = iNum
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvCalibreDbReady(WPARAM tWp, LPARAM tLp)
{
   CString  clStr;
   int      iParm=(int)tLp;
   char    *pcParm=(char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   clStr.Format(_T("%s  %d Authors added from the CSV database." CRLF), CVB_GetTimeDateStamp(), iParm);
   StatusAddText(clStr.GetBuffer());
   //
   CreateAuthorList();
   CreateTitlesList(0);
   OnComboChangeAuthors();
   fDatabaseParsed = TRUE;
   CheckCallbacks(iParm);
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvCalibreBooksReady
//  Purpose:   Message from the Calibre Thread: Books acquisition ready
//
//  Parms:     WPARAM =
//             TPARAM = iNum
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvCalibreBooksReady(WPARAM tWp, LPARAM tLp)
{
   CString  clStr;
   char    *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   clStr.Format(_T("%s  %d Titles added." CRLF), CVB_GetTimeDateStamp(), tLp);
   StatusAddText(clStr.GetBuffer());
   CreateAuthorList();
   CreateTitlesList(0);
   OnComboChangeAuthors();
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvCalibreExportReady
//  Purpose:   Message from the Calibre Thread: CSV Export through CalibreDB ready
//
//  Parms:     WPARAM =
//             TPARAM =
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvCalibreExportReady(WPARAM tWp, LPARAM tLp)
{
   CString clStr;
   char *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   clStr.Format(_T("%s  CSV Database export ready" CRLF), CVB_GetTimeDateStamp());
   StatusAddText(clStr.GetBuffer());
   CheckCallbacks(0);
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvCalibreFreeReady
//  Purpose:   Message from the Calibre Thread: Linked list freeed
//
//  Parms:     WPARAM =
//             TPARAM =
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvCalibreFreeReady(WPARAM tWp, LPARAM tLp)
{
   CString clStr;
   char *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   clStr.Format(_T("%s  Database empty" CRLF), CVB_GetTimeDateStamp());
   StatusAddText(clStr.GetBuffer());
   CheckCallbacks(0);
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvCalibreShowProgress
//  Purpose:   Message from the Calibre Thread: Update progress bar
//
//  Parms:     WPARAM = percentage 0--100%
//             TPARAM =
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvCalibreShowProgress(WPARAM tWp, LPARAM tLp)
{
   char *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   UpdateProgressBar((int)tLp);
   return(0);
}

//
//  Function:  OnRcvCalibreMessage
//  Purpose:   Message from the Calibre Thread: display it
//
//  Parms:     WPARAM =
//             TPARAM =
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvCalibreMessage(WPARAM tWp, LPARAM tLp)
{
   char *pcParm = (char *) tWp;

   if(pcParm)
   {
      StatusAddText(pcParm);
      StatusAddText(CRLF);
      delete [] pcParm;
   }
   UpdateDialogScreen();
   return(0);
}

//
//  Function:  OnRcvClickedParseInput
//  Purpose:   Message from the Calibre Thread: display it
//
//  Parms:     WPARAM =
//             TPARAM =
//  Returns:   0
//
LRESULT CCalPageGeneric::OnRcvClickedParseInput(WPARAM tWp, LPARAM tLp)
{
   OnBnClickedParseInput();
   UpdateDialogScreen();
   return(0);
}


/* ======   Local   Functions separator ===========================================
void ____Class_Methods____(){}
==============================================================================*/

//
//  Function:  CheckCallbacks
//  Purpose:   Check pending callbacks
//
//  Parms:     Parameter
//  Returns:   Compl code
//
int CCalPageGeneric::CheckCallbacks(int iParm)
{
   int iCc=0;
   //
   // Check pending callbacks
   //
   if(pfCallback)
   {
      iCc = CALL_MEMBER_PFN(this, pfCallback)(iParm);
      pfCallback = NULL;
   }
   return(iCc);
}

//
//  Function:  CreateProgressBar
//  Purpose:   Create the progress bar
//
//  Parms:     void
//  Returns:   void
//
void CCalPageGeneric::CreateProgressBar()
{
   clBarProgress.SetRange(1, CALIBRE_PROGRESS_RES);
   clBarProgress.SetStep(1);
   clBarProgress.SetPos(0);
}

//
//  Function:  ExportBooks
//  Purpose:   Export books/titles
//
//  Parms:
//  Returns:
//
void CCalPageGeneric::ExportBooks(CString *pclIn, CString *pclOut)
{
   CString  clStr;

   //
   // Parse a book list
   //
   if(pclCmd->IssueCommand(CMD_EXPORT_BOOKS, pclIn, pclOut))
   {
      clStr.Format(_T("%s  Exporting books to folder...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
   else
   {
      clStr.Format(_T("%s  Calibre is still busy...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
}

//
//  Function:  ParseAuthors
//  Purpose:   Parse the author list
//
//  Parms:     Filename
//  Returns:
//
void CCalPageGeneric::ParseAuthors(CString *pclFile)
{
   CString  clStr;

   //
   // Parse Calibre CSV database:
   //    DB_Author - lists All Authors in the database
   //
   if(pclCmd->IssueCommand(CMD_PARSE_DB, pclFile))
   {
      clStr.Format(_T("%s  Reading CSV database...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
   else
   {
      clStr.Format(_T("%s  Calibre is still busy...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
}

//
//  Function:  ParseBooks
//  Purpose:   Parse the book/titles list
//
//  Parms:     Filename
//  Returns:
//
void CCalPageGeneric::ParseBooks(CString *pclFile)
{
   CString  clStr;

   //
   // Parse a book list
   //
   if(pclCmd->IssueCommand(CMD_PARSE_BOOKS, pclFile))
   {
      clStr.Format(_T("%s  Parsing new books...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
   else
   {
      clStr.Format(_T("%s  Calibre is still busy...." CRLF), CVB_GetTimeDateStamp());
      StatusAddText(clStr.GetBuffer());
   }
}

//
//  Function:  SetupLogs
//  Purpose:   Setup all logfiles
//  Parms:
//  Returns:
//
void CCalPageGeneric::SetupLogs(void)
{
   const LOGLUT stLogLut[] =
   {
      { CALIBRE_LOG_ERRORS,     NVM_CHECKBOX_LOG_ERRORS,     NVM_PATH_LOG_ERRORS    },
      { CALIBRE_LOG_PROGRESS,   NVM_CHECKBOX_LOG_PROGRESS,   NVM_PATH_LOG_PROGRESS  },
      { CALIBRE_LOG_NEW_AUTHOR, NVM_CHECKBOX_LOG_AUTHORS,    NVM_PATH_LOG_AUTHORS   },
      { CALIBRE_LOG_DUPLICATES, NVM_CHECKBOX_LOG_DUPLICATES, NVM_PATH_LOG_DUPLICATES},
      { CALIBRE_LOG_NEW_TITLE,  NVM_CHECKBOX_LOG_TITLES,     NVM_PATH_LOG_TITLES    },
      { CALIBRE_LOG_BAD_FORMAT, NVM_CHECKBOX_LOG_BAD,        NVM_PATH_LOG_BAD       }
   };
   BOOL     fLogEnabled, fLogAppend;
   CString  clStr;

   pclNvm->NvmGet(NVM_CHECKBOX_LOG_APPEND, &fLogAppend);
   for(int i=0; i<sizeof(stLogLut)/sizeof(LOGLUT); i++)
   {
      pclNvm->NvmGet(stLogLut[i].iNvmIdFlag, &fLogEnabled);
      pclNvm->NvmGet(stLogLut[i].iNvmIdPath, &clStr);
      pclLog->LOG_Register((u_int16) stLogLut[i].tLog, &clStr, fLogAppend);
      pclLog->LOG_Enable((u_int16) stLogLut[i].tLog, fLogEnabled);
   }
   pclLog->LOG_EnableAll(TRUE);
}

//
//  Function:  StatusAddText
//  Purpose:   Add text to the status window
//
//  Parms:     Text^
//  Returns:   void
//
void CCalPageGeneric::StatusAddText(char *pcText)
{
   CString  clStr;
   int      iLines;

   clEditList.GetWindowText(clStr);
   clStr += pcText;

   iLines = clEditList.GetLineCount();

   clEditList.SetWindowText(clStr);
   clEditList.LineScroll(iLines);
}

//
//  Function:  UpdateDialogScreen
//  Purpose:   Update the variables in the dialog screen
//
//  Parms:     void
//  Returns:   void
//
void CCalPageGeneric::UpdateDialogScreen()
{
   BOOL     fEnabled;
   CString  clStr;

   pclNvm->NvmGet(NVM_NAME_DATABASE_FILE, &clStr);
   clEditDir.SetWindowText(clStr);

   pclNvm->NvmGet(NVM_CHECKBOX_DIRECTORY, &fEnabled);
   if(fEnabled) pclNvm->NvmGet(NVM_NAME_INPUT_DIR,  &clStr);
   else         pclNvm->NvmGet(NVM_NAME_INPUT_FILE, &clStr);
   clEditInput.SetWindowText(clStr);

   clStr.Format(_T("%d"), pclBook->iNumAuthors);
   clEditParseAuthorCount.SetWindowText(clStr);

   clStr.Format(_T("%d"), pclBook->iNumTitles);
   clEditParseTitleCount.SetWindowText(clStr);

   clStr.Format(_T("%d"), pclBook->iNumBadFormat);
   clEditParseNoMatchCount.SetWindowText(clStr);

   clStr.Format(_T("%d"), pclBook->iNumDuplicates);
   clEditParseDuplicateCount.SetWindowText(clStr);

   pclNvm->NvmGet(NVM_AUTHOR_THOLD, &clStr);
   clEditParseAuthorThold.SetWindowText(clStr);

   pclNvm->NvmGet(NVM_TITLE_THOLD, &clStr);
   clEditParseTitleThold.SetWindowText(clStr);

   pclNvm->NvmGet(NVM_WORD_THOLD, &clStr);
   clEditParseWordThold.SetWindowText(clStr);

   pclNvm->NvmGet(NVM_LETTER_THOLD, &clStr);
   clEditParseLetterThold.SetWindowText(clStr);

   UpdateData(FALSE);            // dialog variables --> window
}

//
//  Function:  UpdateProgressBar
//  Purpose:   Update the progress bar
//
//  Parms:     0-100%
//  Returns:   0
//
void CCalPageGeneric::UpdateProgressBar(int iPercentage)
{
   if(iPercentage > CALIBRE_PROGRESS_RES) iPercentage = CALIBRE_PROGRESS_RES;
   clBarProgress.SetPos(iPercentage);
   UpdateDialogScreen();
}

//
//  Function:  UpdateVariables
//  Purpose:   Update all the clEdit variables from screen --> class variables
//
//  Parms:
//  Returns:   0
//
void CCalPageGeneric::UpdateVariables(void)
{
   CString  clStr;

   UpdateData(TRUE);                // dialog window --> variables
   //
   clEditParseAuthorThold.GetWindowText(clStr);
   pclNvm->NvmPut(NVM_AUTHOR_THOLD, clStr);
   //
   clEditParseTitleThold.GetWindowText(clStr);
   pclNvm->NvmPut(NVM_TITLE_THOLD, clStr);
   //
   clEditParseWordThold.GetWindowText(clStr);
   pclNvm->NvmPut(NVM_WORD_THOLD, clStr);
   //
   clEditParseLetterThold.GetWindowText(clStr);
   pclNvm->NvmPut(NVM_LETTER_THOLD, clStr);
   //
}

/* ======   Local   Functions separator ===========================================
void ____Class_Callbacks____(){}
==============================================================================*/


//
//  Function:   CB_ParseInput
//  Purpose:    Callback helper
//
//  Parms:
//  Returns:
//
int CCalPageGeneric::CB_ParseInput(int iNum)
{
   OnBnClickedParseInput();
   return(iNum);
}


//
//  Function:  CreateAuthorList
//  Purpose:   Create the author dropdown list
//
//  Parms:     
//  Returns:   void
//
void CCalPageGeneric::CreateAuthorList()
{
   int      iNum = 0;
   CBOOK   *pclA;

   clComboAuthors.ResetContent();
   //
   do
   {
      pclA = pclBook->DB_GetAuthorInfo(iNum++, NULL);
      if(pclA)
      {
         clComboAuthors.AddString(pclA->pclName->GetBuffer());
      }
   }
   while(iNum < pclBook->iNumAuthors);
   clComboAuthors.SetCurSel(0);
}

//
//  Function:  CreateTitlesList
//  Purpose:   Create the titles dropdown list
//
//  Parms:     Author index 1..?
//  Returns:   void
//
void CCalPageGeneric::CreateTitlesList(int iAuthor)
{
   int      iNum=0;
   CBOOK   *pclA;
   CBOOK   *pclT;

   clComboTitles.ResetContent();
   pclA = pclBook->DB_GetAuthorInfo(iAuthor, NULL);
   //
   if(pclA)
   {
      do
      {
         pclT = pclA->DB_GetTitleInfo(iNum++, pclA, NULL);
         if(pclT)
         {
            clComboTitles.AddString(pclT->pclName->GetBuffer());
         }
      }
      while(iNum < pclBook->iNumTitles);
   }
   else
   {
      clComboTitles.AddString("No book titles available");
   }
   clComboTitles.SetCurSel(0);
}


