/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  CalibreThread.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Header file for the *.cpp
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       07 Aug 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_CALIBRETHREAD_H_)
#define _CALIBRETHREAD_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000


UINT CalibreThread         (LPVOID);

#endif // !defined(_CALIBRETHREAD_H_)
