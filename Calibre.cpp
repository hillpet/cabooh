// Calibre.cpp : Defines the class behaviors for the application.
//
// Moving to Win-7 VS2010 needs following adaptations:
// stdafx.h:
//    #define WINVER 0x0501            
// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
//    #define _WIN32_WINNT 0x0500      
// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
//    #define _WIN32_WINDOWS 0x0501    
// Change this to the appropriate value to target Windows Me or later.
//
// To suppress unsecure functions warnings (strcpy --> strcpy_s etc) : 
//    Add to Project Properties > Configuration Properties > C/C++ > Command Line: /D " _CRT_SECURE_NO_WARNINGS"
//
// Remaining issues are:
//    CalibreFiles.cpp(230): warning C4996: 'itoa': The POSIX name for this item is deprecated. Instead, use the ISO C++ conformant name: _itoa. See online help for details.
//    CalibreFiles.cpp(880): warning C4996: 'strcmpi': The POSIX name for this item is deprecated. Instead, use the ISO C++ conformant name: _strcmpi. See online help for details.
//    CalibreFiles.cpp(2102): warning C4996: 'strupr': The POSIX name for this item is deprecated. Instead, use the ISO C++ conformant name: _strupr. See online help for details.
//    CalibreFiles.cpp(2111): warning C4996: 'strupr': The POSIX name for this item is deprecated. Instead, use the ISO C++ conformant name: _strupr. See online help for details.
//
//

#include "stdafx.h"
#include "Calibre.h"
#include "CalSheet.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif




BEGIN_MESSAGE_MAP(CCalibreApp, CWinApp)
   ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

//
// CCalibreApp
//
CCalibreApp theApp;

//
//  Function:   CCalibreApp
//  Purpose:    Constructor
//
//  Parms:
//  Returns:
//
CCalibreApp::CCalibreApp()
{
   pclNvm = new CNvmStorage(NUM_WNVMS);
}

//
//  Function:   InitInstance
//  Purpose:    Init of theApp
//
//  Parms:
//  Returns:
//
BOOL CCalibreApp::InitInstance()
{
   int      iNr;
   CString  clStr, clDir;
   BOOL     fNvmOKee = TRUE;
   CWnd    *pclWnd   = CWnd::GetActiveWindow();

   //
   // InitCommonControls() is required on Windows XP if an application
   // manifest specifies use of ComCtl32.dll version 6 or later to enable
   // visual styles.  Otherwise, any window creation will fail.
   //
   InitCommonControls();
   CWinApp::InitInstance();
   AfxEnableControlContainer();
   ParseCommandLine(clCmdInfo);
   //
   // Check the archive first
   //
   GetCurrentDirectory(MAX_PATH, clDir.GetBufferSetLength(MAX_PATH));
   clDir.ReleaseBuffer();   
   clStorage.Format(_T("%s\\%s"), clDir, CABOOH_CONFIG);
   iNr = pclNvm->NvmCheckArchive(&clStorage);
   //
   if(iNr == NUM_WNVMS)
   {
      // Archive is OKee
      if(pclNvm->NvmRead(&clStorage) == FALSE)
      {
         // Archive is bad : take new one
         fNvmOKee = FALSE;
      }
      else
      {
         // NVM loaded correctly: check if we rolled the rev.
         pclNvm->NvmGet(NVM_DB_VERSION, &clStr);
         fNvmOKee = (clStr.Compare(CALIBRE_DB_VERSION) == 0);
      }
   }
   else
   {
      // Archive is bad : take new one
      fNvmOKee = FALSE;
   }
   if(!fNvmOKee)
   {
      NewNvm(&clDir);
   }
   //
   // Setup the property sheets, and activate them
   //
   CCalSheet clCalSheet("CaBooh Properties", pclWnd, 0);
   //
   INT_PTR iResult = clCalSheet.DoModal();
   //
   switch(iResult)
   {
      case IDOK:
         pclNvm->NvmPut(NVM_INITIALIZED, "0");
         pclNvm->NvmWrite(&clStorage);      // Save settings
         clMap.DB_FreeDB();               // Free linked list
         break;

      case IDCANCEL:
         clMap.DB_FreeDB();               // Free linked list
         break;                    
   }

   //
   // Since the dialog has been closed, return FALSE so that we exit the
   //  application, rather than start the application's message pump.
   //
   clCmd.IssueCommand(CMD_EXIT);          // Exit thread
   clCmdInfo.Cleanup();
   Sleep(500);
   //
   return FALSE;
}

/* ======   Local   Functions separator ===========================================
void ____Class_Methods_NVM____(){}
==============================================================================*/

//
//  Function:  BrowseFile
//  Purpose:   Browse an input file
//
//  Parms:     Path to File, path to directory, file ext wildcards
//  Returns:   TRUE if OK
//
//  The last parameter is used to determine the type of filename a file must have
//  to be displayed in the file list box. The first string in the string pair
//  describes the filter; the second string indicates the file extension to use.
//  Multiple extensions may be specified using ';' as the delimiter. The string ends
//  with two '|' characters, followed by a NULL character. You can also use a
//  CString object for this parameter.
//
//
BOOL CCalibreApp::BrowseFile(CString *pclPath, CString *pclDir, char *pcExt)
{
   BOOL        fCC = FALSE;
   int         iIdx;
   CString     clStr;
   CFileDialog clDlg (TRUE,
                      "txt",
                      *pclPath,
                      OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, pcExt);


   INT_PTR nResult = clDlg.DoModal ();

   switch(nResult)
   {
      case IDOK:
         if(pclPath) *pclPath = clDlg.GetPathName();
         if(pclDir)
         {
            // Retrieve the directory only
            *pclDir = clDlg.GetPathName();
            clStr   = clDlg.GetFileName();
            iIdx    = pclDir->Find(clStr);
            pclDir->GetBufferSetLength(iIdx-1);  //Remove trailing backslash
         }
         fCC = TRUE;
         break;

      case IDCANCEL:
         if(pclPath) *pclPath = clDlg.GetPathName();
         break;
   }
   return(fCC);
}

//
//  Function:  BrowseFolder
//  Purpose:   Browse for a folder name
//
//  Parms:
//  Returns: TRUE if OKee
//
BOOL CCalibreApp::BrowseFolder(CString *pclFolderPath, char *pcText)
{
   BOOL fCC=FALSE;

   CFolderDialog clDlg( pcText, *pclFolderPath, NULL, BIF_RETURNONLYFSDIRS | BIF_USENEWUI );
   //
   if( clDlg.DoModal() == IDOK  )
   {
       *pclFolderPath  = clDlg.GetFolderPath();
       fCC = TRUE;
   }
   return(fCC);
}

//
//  Function:  NewNvm
//  Purpose:   Init a new NVM
//
//  Parms:     Current working Dir 
//  Returns:
//
BOOL CCalibreApp::NewNvm(CString *pclStr)
{
   //
   // Take the Apps folder for default folder location
   // 
   pclNvm->NvmPut(NVM_VERSION,                  CALIBRE_VERSION);          //   NVM_VERSION,
   pclNvm->NvmPut(NVM_INITIALIZED,              "1");                      //   NVM_INITIALIZED,
   pclNvm->NvmPut(NVM_NAME_APPS_DIR,           *pclStr);                   //   NVM_NAME_APPS_DIR,
   pclNvm->NvmPut(NVM_NAME_DATABASE_FILE,       "");                       //   NVM_NAME_DATABASE_FILE,
   pclNvm->NvmPut(NVM_NAME_DATABASE_DIR,       *pclStr);                   //   NVM_NAME_DATABASE_DIR,
   pclNvm->NvmPut(NVM_NAME_INPUT_FILE,          "");                       //   NVM_NAME_INPUT_FILE,
   pclNvm->NvmPut(NVM_NAME_INPUT_DIR,          *pclStr);                   //   NVM_NAME_INPUT_DIR,
   pclNvm->NvmPut(NVM_NAME_MOVETO_DIR,          "");                       //   NVM_NAME_MOVETO_DIR,
   pclNvm->NvmPut(NVM_AUTHOR_THOLD,             "40");                     //   NVM_AUTHOR_THOLD,
   pclNvm->NvmPut(NVM_TITLE_THOLD,              "40");                     //   NVM_TITLE_THOLD,
   pclNvm->NvmPut(NVM_WORD_THOLD,               "80");                     //   NVM_WORD_THOLD,
   pclNvm->NvmPut(NVM_LETTER_THOLD,             "80");                     //   NVM_LETTER_THOLD,
   pclNvm->NvmPut(NVM_NAME_USER_DIR,           *pclStr);                   //   NVM_NAME_USER_DIR,
   pclNvm->NvmPut(NVM_NAME_USER_TEMP,           "cabooh.txt");             //   NVM_NAME_USER_TEMP,
   pclNvm->NvmPut(NVM_NAME_USER_LIST,           "");                       //   NVM_NAME_USER_LIST,
   pclNvm->NvmPut(NVM_CHECKBOX_DEEPSCAN,        "0");                      //   NVM_CHECKBOX_DEEPSCAN,
   pclNvm->NvmPut(NVM_CHECKBOX_DUP,             "0");                      //   NVM_CHECKBOX_DUP,
   pclNvm->NvmPut(NVM_CHECKBOX_NEW,             "1");                      //   NVM_CHECKBOX_NEW,
   pclNvm->NvmPut(NVM_CHECKBOX_CVS,             "0");                      //   NVM_CHECKBOX_CVS,
   pclNvm->NvmPut(NVM_CHECKBOX_BAD,             "0");                      //   NVM_CHECKBOX_BAD,
   pclNvm->NvmPut(NVM_CHECKBOX_DEL,             "0");                      //   NVM_CHECKBOX_DEL,
   pclNvm->NvmPut(NVM_CHECKBOX_DIRECTORY,       "1");                      //   NVM_CHECKBOX_DIRECTORY,
   pclNvm->NvmPut(NVM_CHECKBOX_GEN_LOG,         "1");                      //   NVM_CHECKBOX_GEN_LOG,
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_APPEND,      "0");                      //   NVM_CHECKBOX_LOG_APPEND,
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_PROGRESS,    "0");                      //   NVM_CHECKBOX_LOG_PROGRESS,
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_AUTHORS,     "1");                      //   NVM_CHECKBOX_LOG_AUTHORS,
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_TITLES,      "1");                      //   NVM_CHECKBOX_LOG_TITLES,
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_BAD,         "1");                      //   NVM_CHECKBOX_LOG_BAD,
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_DUPLICATES,  "0");                      //   NVM_CHECKBOX_LOG_DUPLICATES,
   pclNvm->NvmPut(NVM_CHECKBOX_LOG_ERRORS,      "1");                      //   NVM_CHECKBOX_LOG_ERRORS,
   pclNvm->NvmPut(NVM_CALIBRE_QUIETMODE,        "0");                      //   NVM_CALIBRE_QUIETMODE
   pclNvm->NvmPut(NVM_CALIBRE_EXECMODE,         "0");                      //   NVM_CALIBRE_EXECMODE <<--- calibredbdummy.exe
   pclNvm->NvmPut(NVM_PATH_LOG_ERRORS,          "cabooh_errors.log");      //   NVM_PATH_LOG_ERRORS
   pclNvm->NvmPut(NVM_PATH_LOG_PROGRESS,        "cabooh_progress.txt");    //   NVM_PATH_LOG_PROGRESS
   pclNvm->NvmPut(NVM_PATH_LOG_AUTHORS,         "cabooh_new_authors.txt"); //   NVM_PATH_LOG_AUTHORS
   pclNvm->NvmPut(NVM_PATH_LOG_DUPLICATES,      "cabooh_duplicate.txt");   //   NVM_PATH_LOG_DUPLICATES
   pclNvm->NvmPut(NVM_PATH_LOG_TITLES,          "cabooh_new_titles.txt");  //   NVM_PATH_LOG_TITLES
   pclNvm->NvmPut(NVM_PATH_LOG_BAD,             "cabooh_bad_entries.txt"); //   NVM_PATH_LOG_BAD
   //
   pclNvm->NvmPut(NVM_DB_VERSION,               CALIBRE_DB_VERSION);       //   NVM_DB_VERSION,
   //
   return(TRUE);
}

//
//  Function:  GetAppFolder
//  Purpose:   Retrieve the pathname of the Cabooh.exe folder
//
//  Parms:
//  Returns:   Pathname
//
CString CCalibreApp::GetAppFolder()
{
    CString  clPath;
    int      iPos=-1;

   clPath = AfxGetAppName();
   clPath.Append(".exe");
    
   HMODULE hMod   = GetModuleHandle(clPath);  
   DWORD iPathLen = GetModuleFileName(hMod, clPath.GetBufferSetLength(MAX_PATH+1), MAX_PATH);
   clPath.ReleaseBuffer(iPathLen);

    if( (iPos=clPath.ReverseFind(_T('\\'))) != -1)
    {
        clPath.Delete(iPos, iPathLen-iPos);
    }
    return(clPath);
}

