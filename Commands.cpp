/*  (c) Copyright:  2011 PATRN.NL
**
**  $Workfile:  Commands.cpp $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Implementation of the Calibre Commands
**
**  Entries:
**
**
**
**
**
 *  Compiler/Assembler: Platform independent
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       07 Aug 2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif



//
// Need to declare the static CCalCmd members
//
HANDLE   CCalCmd::tGlobalEvtHandle;
int      CCalCmd::iGlobalCommandNr;
int      CCalCmd::iGlobalCc;
//
CCMDS    CCalCmd::tGlobalCmd;
CString  CCalCmd::clGlobalParm1;
CString  CCalCmd::clGlobalParm2;
int      CCalCmd::iGlobalParm1;
int      CCalCmd::iGlobalParm2;
HWND     CCalCmd::tGlobalDlgWnd;


//
//  Function:   Constructor
//  Purpose:
//
//  Parms:
//  Returns:
//
CCalCmd::CCalCmd()
{
   tGlobalCmd = CMD_DONE;
   iGlobalCommandNr++;
}

//
//  Function:   Destructor
//  Purpose:
//
//  Parms:
//  Returns:
//
CCalCmd::~CCalCmd()
{
   iGlobalCommandNr--;
}

//
//  Function:  ExecuteCmd
//  Purpose:   Execute the command
//
//  Parms:     plcMap
//  Returns:   FALSE if exit requests
//
BOOL CCalCmd::ExecuteCmd(CTitle *pclTitle)
{
   BOOL  fCc=TRUE;
   int   iNum;

   switch(tGlobalCmd)
   {
      default:
         CommandDone(-1);
         break;

      case CMD_EXIT:
         CommandDone(0);
         fCc = FALSE;
         break;

      case CMD_PARSE_DB:
         iNum = pclTitle->DB_CalibreCsvRead(&clGlobalParm1);
         // cc= 0   No authors added
         // cc=+xx  xx authors added
         MessageToApp(WM_CALIBRE_DB_READY, iNum);
         CommandDone(0);
         break;

      case CMD_PARSE_BOOKS:
         iNum = pclTitle->TITLE_ParseBooks(&clGlobalParm1);
         // cc= 0   No books added
         // cc=+xx  xx books added
         MessageToApp(WM_CALIBRE_BOOKS_READY, iNum);
         CommandDone(0);
         break;

      case CMD_ADD_BOOKS:
         iNum = pclTitle->DB_CalibreCsvAddBooks(&clGlobalParm1, (CASTAT) iGlobalParm1);
         // cc= 0   No books added
         // cc=+xx  xx books added
         // cc=-1   Error
         MessageToApp(WM_CALIBRE_ADD_READY, iNum);
         CommandDone(0);
         break;

      case CMD_DELETE_BOOKS:
         iNum = pclTitle->DB_CalibreDeleteById(&clGlobalParm1, (CASTAT) iGlobalParm1);
         // cc= 0   No books deleted
         // cc=+xx  xx books deleted
         // cc=-1   Error
         MessageToApp(WM_CALIBRE_DELETE_READY, iNum);
         CommandDone(0);
         break;

      case CMD_EXPORT_BOOKS:
         iNum = pclTitle->DB_CalibreCsvExport(&clGlobalParm1, &clGlobalParm2);
         // cc=+1 OKee
         // cc=-1 Error
         MessageToApp(WM_CALIBRE_EXPORT_READY, iNum);
         CommandDone(0);
         break;
   }
   return(fCc);
}

//
//  Function:  IssueCommand
//  Purpose:   Issue a command into the command buffer
//
//  Parms:
//  Returns:   TRUE if command accepted
//
BOOL CCalCmd::IssueCommand(CCMDS tMyCmd)
{
   BOOL  fCc=FALSE;

   if(tGlobalCmd == CMD_DONE)
   {
      // Previous CMD has been completed
      if(pclMyParm1) clGlobalParm1  = *pclMyParm1;
      if(pclMyParm2) clGlobalParm2  = *pclMyParm2;
      //
      iGlobalParm1  = iMyParm1;
      iGlobalParm2  = iMyParm2;
      tGlobalCmd    = tMyCmd;
      tGlobalDlgWnd = tMyDlgWnd;
      fCc           = TRUE;
      //
      // Signal the CMD event to the thread
      //
      SetEvent(tGlobalEvtHandle);
   }
   return(fCc);
}

//
//  Function:  IssueCommand
//  Purpose:   Issue a command into the command buffer
//
//  Parms:
//  Returns:   TRUE if command accepted
//
BOOL CCalCmd::IssueCommand(CCMDS tCmd, CString *pclP1)
{
   pclMyParm1 = pclP1;
   pclMyParm2 = NULL;
   iMyParm1   = 0;
   iMyParm2   = 0;
   return( IssueCommand(tCmd) );
}

//
//  Function:  IssueCommand
//  Purpose:   Issue a command into the command buffer
//
//  Parms:
//  Returns:   TRUE if command accepted
//
BOOL CCalCmd::IssueCommand(CCMDS tCmd, CString *pclP1, CString *pclP2)
{
   pclMyParm1 = pclP1;
   pclMyParm2 = pclP2;
   iMyParm1   = 0;
   iMyParm2   = 0;
   return( IssueCommand(tCmd) );
}

//
//  Function:  IssueCommand
//  Purpose:   Issue a command into the command buffer
//
//  Parms:
//  Returns:   TRUE if command accepted
//
BOOL CCalCmd::IssueCommand(CCMDS tCmd, CString *pclP1, int iP1)
{
   pclMyParm1 = pclP1;
   pclMyParm2 = NULL;
   iMyParm1   = iP1;
   iMyParm2   = 0;
   return( IssueCommand(tCmd) );
}

//
//  Function:  CommandDone
//  Purpose:   Mark command DONE into the command buffer
//
//  Parms:     Completion code
//  Returns:
//
void CCalCmd::CommandDone(int iCompCode)
{
   tGlobalCmd = CMD_DONE;
   iGlobalCc  = iCompCode;
   // Reset the CMD event signal to the thread
   ResetEvent(tGlobalEvtHandle);
}

//
//  Function:  EventHandlePut
//  Purpose:   Store the command event handle
//
//  Parms:     Handle
//  Returns:
//
void CCalCmd::EventHandlePut(HANDLE tHdl)
{
   tGlobalEvtHandle = tHdl;
}

/* ======   Local   Functions separator ===========================================
void ____Static_Methods____(){}
==============================================================================*/

//
//  Function:   MessageToApp
//  Purpose:    Send a message to the host application (MainFrame)
//
//  Parms:      Message ID, iParm
//  Returns:
//
void CCalCmd::MessageToApp(int iMsg, int iParm)
{
   //
   // Put out a message to the main thread to signal something
   //
   ::PostMessage(tGlobalDlgWnd, iMsg, (WPARAM) NULL, (LPARAM) iParm);
}

//
//  Function:   TextMessageToApp
//  Purpose:    Send a message to the host application (MainFrame)
//
//  Parms:      Message ID, Message text
//  Returns:
//  NOTE:       Receiver has to release the message buffer memory
//
void CCalCmd::TextMessageToApp(int iMsg, char *pcMsg)
{
   TextMessageToApp(iMsg, FALSE, pcMsg);
}

//
//  Function:   TextMessageToApp
//  Purpose:    Send a message to the host application (MainFrame)
//
//  Parms:      Message ID, Timestamp YESNO, Message text
//  Returns:
//  NOTE:       Receiver has to release the message buffer memory
//
void CCalCmd::TextMessageToApp(int iMsg, BOOL fTimestamp, char *pcMsg)
{
   CString clStr;

   if(fTimestamp)
   {
      clStr  = CVB_GetTimeDateStamp();
   }
   clStr += pcMsg;

   char *pcParm = new char[clStr.GetLength()+1];
   if(pcParm)
   {
      //
      // Put out a message to the main thread to signal something.
      // Main thread needs to free the memory.
      //
      strcpy(pcParm, clStr.GetBuffer());
      ::PostMessage(tGlobalDlgWnd, iMsg, (WPARAM) pcParm, (LPARAM) 0);
   }
}

//
//  Function:   TextMessageToApp
//  Purpose:    Send a message to the host application (MainFrame)
//              Add the time and date stamp plus the parameter to the message
//
//  Parms:      Message ID, Timestamp YESNO, Message text, message parms
//  Returns:
//  NOTE:       Receiver has to release the message buffer memory
//
void CCalCmd::TextMessageToApp(int iMsg, BOOL fTimestamp, char *pcMsg, int iParm)
{
   CString clStr, clStrTs;

   if(fTimestamp)
   {
      clStrTs  = CVB_GetTimeDateStamp();
   }
   clStr.Format(_T("%s%s%d"), clStrTs, pcMsg, iParm);

   char *pcParm = new char[clStr.GetLength()+1];
   if(pcParm)
   {
      //
      // Put out a message to the main thread to signal something.
      // Main thread needs to free the memory.
      //
      strcpy(pcParm, clStr.GetBuffer());
      ::PostMessage(tGlobalDlgWnd, iMsg, (WPARAM) pcParm, (LPARAM) iParm);
   }
}

