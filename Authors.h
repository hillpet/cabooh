/*  (c)2011 Patrn.nl
**
**  $Workfile:  Authors.h $
**  $Revision:  $
**  $Modtime:   $
**
**  Purpose:    Define the Calibre Author class
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10 Mar 2012
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_AUTHORS_H_)
#define _AUTHORS_H_

//typedef class CTitle  CBOOK;

//
// CAuthor
//
class CAuthor : public CBook
{
public:
            CAuthor();
   virtual ~CAuthor();
   //
   // PUBLIC Methods
   //
public:
   char    *AUTHOR_GetAuthor              (int);
   CBOOK   *AUTHOR_GetAuthorList          (void);
   //
   CAuthor& CAuthor::operator=            (const CAuthor&);

   void     AUTHOR_CreateDatabaseList     (const char *);
   void     AUTHOR_CreateDatabaseList     (const char *, int);
   //
   // PRIVATE Methods
   //
private:
   BOOL     StatusDisplayAuthor           (CBOOK *, CString *, CString *, int);
   BOOL     StatusBookEnabled             (CBOOK *, int);
   BOOL     StatusEnabled                 (CASTAT, int);
   //
   //
   // PUBLIC Data members
   //
public:

   //
   // PRIVATE Data members
   //
private:

};


#endif // !defined(_AUTHORS_H_)
