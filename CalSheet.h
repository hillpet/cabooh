/*  (c) Copyright:  2011 PATRN.NL, Confidential Data
**
**  $Workfile:          CalSheet.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Setup the property pages for the dialogs header file
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       22Aug2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(AFX_CALSHEET_H)
#define AFX_CALSHEET_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "CalPageGeneric.h"
#include "CalPageWildcard.h"


class CCalSheet : public CPropertySheet
{
   DECLARE_DYNAMIC(CCalSheet)

// Construction
public:
   CCalSheet(UINT    nIDCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);
   CCalSheet(LPCTSTR pszCaption, CWnd* pParentWnd = NULL, UINT iSelectPage = 0);

// Attributes
public:
    CCalPageGeneric     clCalPageGeneric;
    CCalPageWildcard    clCalPageWildcard;

// Operations
public:

// Overrides
   //{{AFX_VIRTUAL(CCalSheet)
public:
   virtual BOOL OnInitDialog();
protected:
   virtual BOOL OnNotify(WPARAM wParam, LPARAM lParam, LRESULT* pResult);
   //}}AFX_VIRTUAL

// Implementation
public:
   virtual ~CCalSheet();

   // Generated message map functions
protected:
   //{{AFX_MSG(CCalSheet)
   //}}AFX_MSG
   DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CALSHEET_H)
