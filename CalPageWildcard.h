/*  (c) Copyright:  2011  PATRN.NL, Confidential Data
**
**  $Workfile:          CalPageWildcard.h $
**  $Revision:          $
**  $Modtime:           $
**
**  Purpose:            Property Page for wildcard searches issues
**
**
**
**
 *  Compiler/Assembler: MS Visual Studio / dotNet
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       25nov2011
**
 *  Revisions:
 *    $Log:   $
 *
 *
 *
 *
 *
 *
 *
 *
**/

#if !defined(_CALPAGEWILDCARD_H_)
#define _CALPAGEWILDCARD_H_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

//
// Define the Wildcard property page button behavior
// 
#define WC_NUM_BUTTONS  4
//
typedef enum
{                          // Filter:
   WC_CSV = 0,             // Calibre-CSV Database
   WC_NEW,                 // New books
   WC_DUP,                 // Duplicate books
   WC_BAD,                 // Bad formatted books

   WC_NUM_FILTERS
};

typedef struct WCBTNS
{
   int   iWcIdc;
   BOOL  fWcBtn;
}  WCBTNS;

class CCalPageWildcard : public CPropertyPage
{
   DECLARE_DYNCREATE(CCalPageWildcard)

// Construction
public:
   CCalPageWildcard                       ();
  ~CCalPageWildcard                       ();

// Dialog Data
   //{{AFX_DATA(CCalPageWildcard)
   enum { IDD = IDD_PROP_CAL_WILDCARD };
   //}}AFX_DATA


// Overrides
   //{{AFX_VIRTUAL(CCalPageWildcard)
public:
   virtual void OnOK                      ();
   virtual void OnCancel                  ();
protected:
   HICON             m_hIcon;
   virtual BOOL      OnInitDialog         ();
   virtual void      DoDataExchange       (CDataExchange *);
   virtual BOOL      PreTranslateMessage  (MSG *);
   //}}AFX_VIRTUAL

private:
   CCalibreApp      *pclApp;
   //
   CNvmStorage      *pclNvm;
   CCalCmd          *pclCmd;
   CBOOK            *pclBook;
   CLog             *pclLog;
   //
   CCheckListBox     clListbox;
   //
   CReadOnlyEdit     clChecklistStatus;
   CReadOnlyEdit     clListImportName;
   CReadOnlyEdit     clListDbName;
   CReadOnlyEdit     clListDestName;
   CReadOnlyEdit     clShowCommand;
   CReadOnlyEdit     clShowSelected;
   CReadOnlyEdit     clShowTotal;
   CReadOnlyEdit     clWildcard;

   CAutoFont        *pclFontNum;
   CAutoFont        *pclFontTxt;
   CAutoFont        *pclFontCmd;
   CAutoFont        *pclFontSta;
   int               iRadioDelete;
   int               iChecklistTotalNr;
   int               iChecklistSelectedNr;
   int               iChecklistIndex;
   CASTAT            tCurrentList;
   CASTAT            tDeleteStatus;
   //
   BOOL  BrowseFolder                        (CString *, char *);
   int   CreateAuthorList                    (CASTAT);
   int   CreateTitleList                     (CBOOK  *, CASTAT);
   void  AddBooks                            (CString *, CASTAT);
   void  DeleteBooksById                     (CString *, CASTAT);
   int   HandleCommand                       (CBOOK  *, CACMD);
   int   HandleCommandError                  (CString *, char *, CACMD);
   void  ParseBooks                          (CString *);
   void  StatusAddText                       (char *);
   void  UndeleteBooks                       (CASTAT);
   void  UpdateButtonStatus                  (CASTAT);
   void  UpdateCheckListBox                  (CASTAT);
   void  UpdateDialogScreen                  ();
   void  UpdateDialogScreenForced            (int);
   void  UpdateVariables                     ();
   BOOL  WildMatch                           (CString, CString, BOOL);
   BOOL  HandleKeys                          (char);
   BOOL  HandleSpaceKey                      (void);
   int   DisplayAlternatives                 (TLIST *, int);

protected:
   // Generated message map functions
   afx_msg void      OnPaint                 ();
   afx_msg HCURSOR   OnQueryDragIcon         ();
   afx_msg void      OnSysCommand            (UINT, LPARAM);
   //DECLARE_MESSAGE_MAP()
public:
   afx_msg void OnLbnSelchangeCheckListbox   ();
   afx_msg void OnEnChangeWcEditWildcard     ();
   //
   afx_msg void OnBnClickedButtonSelectAll   ();
   afx_msg void OnBnClickedButtonSelectNone  ();
   afx_msg void OnBnClickedButtonAdd         ();
   afx_msg void OnBnClickedButtonAddAuthor   ();
   afx_msg void OnBnClickedButtonBad         ();
   afx_msg void OnBnClickedButtonBrowse      ();
   afx_msg void OnBnClickedButtonCopy        ();
   afx_msg void OnBnClickedButtonDelete      ();
   afx_msg void OnBnClickedButtonDb          ();
   afx_msg void OnBnClickedButtonDup         ();
   afx_msg void OnBnClickedButtonMove        ();
   afx_msg void OnBnClickedButtonNew         ();
   afx_msg void OnBnClickedWcRadioDelFile    ();
   afx_msg void OnBnClickedWcRadioDelDir     ();
   afx_msg void OnBnClickedWcRadioDelDb      ();
   //
   afx_msg LRESULT OnRcvCalibreMessage       (WPARAM, LPARAM);
   afx_msg LRESULT OnRcvCalibreAddReady      (WPARAM, LPARAM);
   afx_msg LRESULT OnRcvCalibreDeleteReady   (WPARAM, LPARAM);
   //
   DECLARE_MESSAGE_MAP()
};
//{{AFX_INSERT_LOCATION}}



#endif // !defined(_CALPAGEWILDCARD_H_)

