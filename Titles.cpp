/*  (c) Copyright 2012 Patrn.nl
**
**  $Workfile:   Titles.cpp $
**  $Revision:   $
**  $Modtime:    $
**
**  Purpose:     Title class implementation
**
**
**  Entry Points:
**
**
**
**
**
 *  Compiler/Assembler: 
 *  Ext Packages:
**
 *  Author:             Peter Hillen
 *  Date Created:       10 Mar 2012
**
 *
 *
 *
 *
**/

#include "stdafx.h"
#include "Calibre.h"

extern const EPUB    stBookTypes[];
extern const int     iNumBookTypes;



//
//  Function:  CTitle()
//  Purpose:   constructor
//  Parms:
//  Returns:
//
CTitle::CTitle()
{
}

//
//  Function:  CTitle
//  Purpose:   Destructor
//  Parms:
//  Returns:
//
//  Note:
//
//
CTitle::~CTitle()
{
}

//
//  Function:  operator =
//  Purpose:   Copy class data
//  Parms:     Source data
//  Returns:   Copied class
//
//  Note:      clBook1 = clBook2 = clbook3
//
//
CTitle& CTitle::operator=(const CTitle& clBook)
{
   if (this != &clBook)
   {
   }
   return(*this);
}

/* ======   Local   Functions separator ===========================================
void ____Public_Methods____(){}
==============================================================================*/

//
//  Function:  TITLE_ParseBooks
//  Purpose:   Parse through the raw book list and create the author/titles list
//
//  Parms:     CString File or Directory name, Execute on result YESNO
//  Returns:   Total number of titles added
//
int CTitle::TITLE_ParseBooks(CString *pclPath)
{
   int         iNumAdded=0;
   HANDLE      tHandle=INVALID_HANDLE_VALUE;
   BOOL        fTitle, fReady=FALSE, fIsFile=TRUE;
   int         iSize, iCmp=1;
   int         iDbId;
   CString     clStr;
   u_int64     llSize=0, llSum=0;
   CBOOK      *pclT;
   CString     clStrFile, clStrDir;
   DIRS       *pstDirs;
   CADET       tCadet;

   *pclLibrary[0] = *pclPath;

   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
   clStr.Format(_T("   Book imports from [%s]"), pclPath->GetBuffer());
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
   pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
   //
   // Get file info first
   //
   if( DB_GetFileInfo(pclPath->GetBuffer(), NULL, NULL, &llSize) == FALSE)
   {
      //
      // This path is a directory: obtain the total filename sizes of all FILES
      //
      *pclDir = *pclPath;

#ifdef   FEATURE_NEST_BOOKS_FOLDER
      pstDirs=NULL;
      //
#ifdef   FEATURE_LOG_BOOKS_FOLDER
      //
      // Show source info
      //
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
      clStr.Format(_T("Import eBooks from [%s]"), pclPath->GetBuffer());
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, clStr.GetBuffer(), LOG_CRLF);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, (char *)pcCommentLines, LOG_CRLF);
#endif   //FEATURE_LOG_BOOKS_FOLDER
      do
      {
         pstDirs = CVB_GetFilesInDirectories(pstDirs, pclPath, &clStrFile, &clStrDir);
         if(pstDirs)
         {
#ifdef   FEATURE_LOG_BOOKS_FOLDER
            clStr.Format(_T("%s\\%s"), clStrDir.GetBuffer(), clStrFile.GetBuffer());
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, clStr.GetBuffer(), LOG_CRLF);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "", LOG_CRLF);
#endif   //FEATURE_LOG_BOOKS_FOLDER
            iSize   = clStrFile.GetLength() + 1;
            llSize += (u_int64) iSize;
         }
      }
      while(pstDirs);
      //
#ifdef   FEATURE_LOG_BOOKS_FOLDER
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "", LOG_CRLF);
#endif   //FEATURE_LOG_BOOKS_FOLDER

#else    //FEATURE_NEST_BOOKS_FOLDER
      *pclPath += "\\*.*";
      while( (tHandle = CVB_GetFileInDirectory(tHandle, pclPath, &clStr, NULL)) != INVALID_HANDLE_VALUE )
      {
         iSize = clStr.GetLength();
         llSize += (u_int64) iSize;
      }
#endif   //FEATURE_NEST_BOOKS_FOLDER
      fIsFile = FALSE;
   }
   
   //
   // Read all other records and subtract the author/title from them
   // 
   do
   {
      iSize = title_GetRecord(pclPath, fIsFile);
      //
      if(iSize)
      {
         llSum += (u_int64)iSize;
         //
         // pclRaw : "28-04-2011  21:17           120.752 What You Know - A. Bertram Chandler.epub" 
         // 
         // First extract the book type (epub, lit, txt, pdf, ...
         // Skip duplicate entries
         //
         tFormat = title_ParseBookType();
         //
         if(tFormat != CALIBRE_EBOOK_NONE)
         {
            //
            // This record has *.epub or similar format: check it out
            // ToDo: Unicode chars in pclRaw
            CVB_ReplaceUnicodeChars(pclRaw->GetBuffer());
            //
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "Import=", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclRaw, LOG_CRLF);
            //
            // ====================================================================================
            // Setup new search:
            // ====================================================================================
            DB_FreeAllCandidates();
            tAuthorTitle  = CALIBRE_AUTHOR;
            tSplit        = CALIBRE_SPLIT_TITLE_AUTHOR;
            iSplit        = 0;
            iScore        = 0;
            //
            // ====================================================================================
            // We need to distinct identical books with different formats (epub, lit, pdb, ...
            // ====================================================================================
            //
            tCadet = title_DetermineAuthor();
            switch(tCadet)
            {
               case CALIBRE_DETERMINE_MATCH:
                  //===============================================================================
                  // This entry has a recognized author:
                  //    pclRaw   : "What You Know - A. Bertram Chandler.epub"  
                  //    pclFile  : "What You Know - A. Bertram Chandler"  
                  //    pclName  : "A. Bertram Chandler"  
                  // Check if we have a title match in the CSV database
                  //===============================================================================
                  tAuthorTitle = CALIBRE_TITLE;
                  pclXref      = pstAltSolList->pclBook;      // Author: get the best solution
                  //
                  clStr.Format(_T("   [CSV] Author=[%s]:Score=%d%%"), pclXref->pclName->GetBuffer(), iScore);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                  DB_RetrieveCandidates(pstAltSolList, &clStr);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                  //
                  // We have positively identified the author: discard the alternative solutions.
                  // Go find a matching title, 
                  //
                  DB_FreeCandidates(pstAltSolList);
                  pstAltSolList = NULL;
                  fTitle = title_ParseTitles(this);
                  if(fTitle && DB_IsRecognized(CALIBRE_TITLE) )
                  {
                     //============================================================================
                     // == DUP ==
                     // Author exists, Title exists and could be :
                     //                  CALIBRE_STATUS_CSV or
                     //                  CALIBRE_STATUS_NEW
                     //
                     // copy DB Id (would be 0 for CALIBRE_STATUS_NEW !!) and add this title as
                     //                  CALIBRE_STATUS_DUP
                     //============================================================================
                     iNumDuplicates++;
                     iDbId = iCalibreId;
                     title_CopyTitleAuthor(this, CALIBRE_TITLE);
                     clStr.Format(_T("   [DUP] Title=[%s]:Score=%d%%"), pclName->GetBuffer(), iScore);
                     pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                     if(iDbId)   clStr.Format(_T("   [ORI] Title=[%s]"), pclName->GetBuffer());
                     else        clStr.Format(_T("   [DUP] Title=[%s]"), pclName->GetBuffer());
                     pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                     pclLog->LOG_Write(CALIBRE_LOG_DUPLICATES, pclName, LOG_CRLF);
                     //
                     DB_RetrieveCandidates(pstAltSolList, &clStr);
                     pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                     //
                     pclT = DB_AddTitle(pclName, pclXref, TRUE);
                     // Overrule status (EXISTS or DUPLICATE) to DUPLICATE
                     pclT->tStatus |= CALIBRE_STATUS_DUP;
                     pclT->iLibNum  = 0;
                     iCalibreId     = iDbId;
                  }
                  else
                  {
                     //============================================================================
                     // == ADD ==
                     // Author exists, Title seems new
                     //============================================================================
                     title_CopyTitleAuthor(this, CALIBRE_TITLE);
                     clStr.Format(_T("   [ADD] No likely title for [%s]"), pclName->GetBuffer());
                     pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                     if(iScore && pclName)
                     {
                        clStr.Format(_T("   [ADD] Best match is [%s]:Score=%d%%"), pclName->GetBuffer(), iScore);
                        pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                     }
                     else
                     {
                        clStr.Format(_T("   [ADD] No close match by [%s] has been found"), pclName->GetBuffer());
                        pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                     }
                     DB_RetrieveCandidates(pstAltSolList, &clStr);
                     pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                     //
                     pclT = DB_AddTitle(pclName, pclXref, TRUE);
                     iNumAdded++;
                     // Overrule status (EXISTS or DUPLICATE) to NEW
                     pclT->tStatus |= CALIBRE_STATUS_NEW;
                     pclT->iLibNum  = 0;
                     pclT->tFormat  = tFormat;
                     pclLog->LOG_Write(CALIBRE_LOG_NEW_TITLE, pclName, LOG_CRLF);
                  }
                  break;

               default:
               case CALIBRE_DETERMINE_INIT:
               case CALIBRE_DETERMINE_NEW:
                  //===============================================================================
                  // == NEW ==
                  // Author seems new, title also, apparently. No idea which is which, add all
                  // permutaions as Author and Title.
                  //===============================================================================
                  tAuthorTitle = CALIBRE_TITLE;
                  tSplit       = CALIBRE_SPLIT_TITLE_AUTHOR;
                  clStr.Format(_T("   No likely Author for [%s]:Score=%d%%: add all permutations"), pclFile->GetBuffer(), iScore);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                  //
                  DB_RetrieveCandidates(pstAltSolList, &clStr);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                  //
                  do
                  {
                     if( title_RetrieveTitleAuthor(this, CALIBRE_AUTHOR) == TRUE)
                     {
                        //===================================================================
                        // Add the next author permutation. The leading space keeps it on top
                        //===================================================================
                        pclXref = DB_AddAuthor(pclName);
                        pclXref->tStatus |= CALIBRE_STATUS_NEW;
                        pclXref->iLibNum  = 0;
                        pclLog->LOG_Write(CALIBRE_LOG_NEW_AUTHOR, pclName, LOG_CRLF);
                        //===================================================================
                        // Add the next title permutation. The leading space keeps it on top
                        //===================================================================
                        title_RetrieveTitleAuthor(this, CALIBRE_TITLE);
                        pclT = DB_AddTitle(pclName, pclXref, TRUE);
                        iNumAdded++;
                        // Overrule status (EXISTS or DUPLICATE) to NEW
                        pclT->tStatus |= CALIBRE_STATUS_NEW;
                        pclT->tFormat  = tFormat;
                        pclT->iLibNum  = 0;
                     }
                  }
                  while(tSplit != CALIBRE_SPLIT_DONE);
                  //
                  break;

               case CALIBRE_DETERMINE_BAD:
                  //===============================================================================
                  // == BAD ==
                  // No hits at all: unsure if this is a valid entry at all
                  //===============================================================================
                  tAuthorTitle = CALIBRE_TITLE;
                  title_CopyTitleAuthor(this, CALIBRE_AUTHOR);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, "   Badly formatted input:", LOG_NONE);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclName, LOG_CRLF);
                  //
                  DB_RetrieveCandidates(pstAltSolList, &clStr);
                  pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
                  //
                  pclXref = DB_AddAuthor("$-Bad Formatted Books");
                  pclXref->tStatus |= CALIBRE_STATUS_BAD;
                  pclXref->iLibNum  = 0;
                  title_CopyTitleAuthor(this, CALIBRE_TITLE);
                  pclT = DB_AddTitle(pclName, pclXref, TRUE);
                  iNumBadFormat++;
                  iNumAdded++;
                  // Overrule status (EXISTS or DUPLICATE) to BAD
                  pclT->tStatus |= CALIBRE_STATUS_BAD;
                  pclT->tFormat  = tFormat;
                  pclT->iLibNum  = 0;
                  pclLog->LOG_Write(CALIBRE_LOG_BAD_FORMAT, pclName, LOG_CRLF);
                  break;
            }
            //ToDo: Save this last state for duplicate entry (epub==lit==pdf) detection
            // pstSplitTemp = pstSplitLast;
            // pstSplitLast = pstSplit;
            // pstSplit     = pstSplitTemp;
         }
      }
      if(llSize) DB_UpdateProgressBar((int)((llSum*CALIBRE_PROGRESS_RES)/llSize));
   }
   while(iSize);
   //
   Sleep(500);
   DB_ReIndexAuthors();
   DB_UpdateProgressBar(0);
   DB_ReadRecord((char *) NULL);
   //
   return(iNumAdded);
}

//
//  Function:  TITLE_GetTitleByIndex
//  Purpose:   Find a listbox index in the title linked list
//
//  Parms:     Index 0..?
//  Returns:   pstTitle
//
CBOOK  *CTitle::TITLE_GetTitleByIndex(int iIndex)
{
   CBOOK      *pstA = pclAuthorList;
   CBOOK      *pstT;

   while(pstA)
   {
      pstT = pstA->pclXref;
      while(pstT)
      {
         if(pstT->iBoxIndex == iIndex)
         {
            return(pstT);
         }
         pstT = pstT->pclNext;
      }
      pstA = pstA->pclNext;
   }
   return(NULL);
}

//
//  Function:  TITLE_GetTitleByWildcardIndex
//  Purpose:   Find a listbox wildcard index in the title linked list
//
//  Parms:     Index 0..?
//  Returns:   pstTitle
//
CBOOK  *CTitle::TITLE_GetTitleByWildcardIndex(int iIndex)
{
   CBOOK      *pstA = pclAuthorList;
   CBOOK      *pstT;

   while(pstA)
   {
      pstT = pstA->pclXref;
      while(pstT)
      {
         if(pstT->iWildIndex == iIndex)
         {
            return(pstT);
         }
         pstT = pstT->pclNext;
      }
      pstA = pstA->pclNext;
   }
   return(NULL);
}


/* ======   Local   Functions separator =======================================
void ____Methods_Books____(){}
==============================================================================*/

//
//  Function:  title_CheckHighScore
//  Purpose:   Check and save the best score
//
//  Parms:     CBOOK *, new score
//  Returns:   TRUE if higher score
//
BOOL CTitle::title_CheckHighScore(CBOOK *pclBook, CAVAL tCaval)
{
   BOOL  fCc=FALSE;

   switch(tCaval)
   {
      case CALIBRE_VAL_NOMATCH:
         break;

      case CALIBRE_VAL_OKEEISH:
         fCc = TRUE;
         break;

      case CALIBRE_VAL_PERFECT:
         fCc = TRUE;
         break;
   }
   return(fCc);
}

//
//  Function:  title_CopyTitleAuthor
//  Purpose:   Copy the assumed author or title from the info block to plcName
//
//  Parms:     pclBook      : Book template  
//             tTarget      : CALIBRE_AUTHOR or CALIBRE_TITLE 
//  Returns:   TRUE if OKee
//
BOOL CTitle::title_CopyTitleAuthor(CBOOK *pclBook, CATA tTarget)
{
   return( title_CopyTitleAuthor(pclBook, tTarget, pclBook->tSplit, pclBook->iSplit) );
}

//
//  Function:  title_CopyTitleAuthor
//  Purpose:   Copy the assumed author or title from the info block to plcName
//
//  Parms:     pclBook      : Book template  
//             tTarget      : CALIBRE_AUTHOR or CALIBRE_TITLE 
//  Returns:   TRUE if OKee
//  Changes:   pclBook->pclName = Author or title CString
//
//                                                      |iSplit
//  IN:                                                 v
//    pclBook->pclFile : "01 - Series info - Title Info - Author name1 & Author name2 & Author name3"
//
//  OUT:
//    pclBook->pclName : "01   Series info   Title Info" 
//                         or
//                       "Author name1 & Author name2 & Author name3"
//
BOOL CTitle::title_CopyTitleAuthor(CBOOK *pclBook, CATA tTarget, SPLIT tMySplit, int iMySplit)
{
   BOOL  fCc=TRUE;
   int   iIndex, iCount;

   switch(tTarget)
   {
      case CALIBRE_TITLE:
         //
         // We are interrested in the TITLE: swap fields of interest first
         //
         switch(tMySplit)
         {
            case CALIBRE_SPLIT_AUTHOR_TITLE:
               tMySplit = CALIBRE_SPLIT_TITLE_AUTHOR; 
               break;

            case CALIBRE_SPLIT_TITLE_AUTHOR:
               tMySplit = CALIBRE_SPLIT_AUTHOR_TITLE;
               break;

            default:
            case CALIBRE_SPLIT_NONE:
               break;
         }
         break;

      default:
      case CALIBRE_AUTHOR:
         break;
   }
   //
   iIndex = iMySplit;
   iCount = pclBook->pclFile->GetLength();
   //
   // We have stripped this record so it contains just 1 hyphen, which separates
   // a possible Title from a possible Author.
   //
   switch(tMySplit)
   {
      default:
         fCc = FALSE;
         break;

      case CALIBRE_SPLIT_NONE:
         //
         // We have no hyphens in this book info : copy all
         //
         *pclBook->pclName = *pclBook->pclFile;
         break;

      case CALIBRE_SPLIT_TITLE_AUTHOR:
         //
         // Need 2nd half of this string
         //
         iCount -= iIndex+1;
        *pclBook->pclName = pclBook->pclFile->Right(iCount);
         //
         pclBook->pclName->Trim();
         break;

      case CALIBRE_SPLIT_AUTHOR_TITLE:
         //
         // Need 1st half of this string
         //
        *pclBook->pclName = pclBook->pclFile->Left(iIndex);
         pclBook->pclName->Trim();
         break;
   }
   return(fCc);
}

//
//  Function:  title_DetermineAuthor
//  Purpose:   Determine where to split the bookrecord into Author and title,
//             driven by a hyphen separation.
//
//  Parms:     void
//  Returns:   CC to signal we have a score that exceeds the initial thresholdclear split between author and title
//             CALIBRE_DETERMINE_MATCH : the author has been determined
//             CALIBRE_DETERMINE_NEW   : the author has not been determined
//             CALIBRE_DETERMINE_BAD   : the format is bad
//
//
//  In:        this->pclFile  : "01 - Title title title - Author A. A. Author"
//             this->tSplit   : CALIBRE_SPLIT_TITLE_AUTHOR, ..
//             this->iSplit   : A-T/T-A split index
//             this->iScore   : Initial (threshold) score
//
CADET CTitle::title_DetermineAuthor(void)
{
   int      iIdx, iHyphenNr, iNumHyphens, iHyphenIndex=0;
   CADET    tCadet=CALIBRE_DETERMINE_BAD;
   CString  clStr;
   //
   // Count all hyphens
   //
   iNumHyphens = pclFile->GetCharCount('-');
   //
   // Try all hyphen permutations and keep the best on top
   //
   for(iHyphenNr=0; iHyphenNr<iNumHyphens; iHyphenNr++)
   {
      iIdx  =  pclFile->Find("-", iHyphenIndex);
      if(iIdx != -1)
      {
         // Save filename, replace all hyphens except one
         clStr = *pclFile;
         pclFile->Replace('-', ' ');
         pclFile->SetAt(iIdx, '-');
         iSplit = iIdx;
         if( title_ExtractAuthor(this) == TRUE)
         {
            // Author or look-alike found in the CSV database
            if( DB_IsRecognized(this, CALIBRE_AUTHOR) == TRUE )
            {
               // Author score beyond user threshold: take it for real
               tCadet = CALIBRE_DETERMINE_MATCH;
            }
            else
            {
               // Author score below user threshold: consider not present yet
               tSplit = CALIBRE_SPLIT_NONE;
               iSplit = 0;
               tCadet = CALIBRE_DETERMINE_NEW;
            }
         }
         else
         {
            tSplit = CALIBRE_SPLIT_NONE;
            iSplit = 0;
            tCadet = CALIBRE_DETERMINE_NEW;
         }
         // Restore filename, set next hyphen index
         *pclFile     = clStr;
         iHyphenIndex = iIdx+1;
      }
   }
   return(tCadet);
}

//
//  Function:  title_ExtractAuthor
//  Purpose:   The pclFile holds the Author and Title separated by a single '-' char.
//             Find out the order (A-T or T-A) by trying to match the Author with all
//             known authors in the CSV list (pclAuthorList).
//
//  Parms:     CBOOK *pclBook : the Author/Title template for the new book
//  Returns:   TRUE if a better score found
//
//  IN:        The raw new book with Author/Title 
//             pclBook->iScore         : the initial score threshold
//             pclBook->pclFile        : "01   Series info   Title Info - Author name1 & Author name2 & Author name3"
//
//  OUT:       The raw new book with Author/Title 
//             pclBook->iScore         : the new best score
//             pclBook->pclName        : the name of the Author
//
BOOL CTitle::title_ExtractAuthor(CBOOK *pclBook)
{
   BOOL     fCc=FALSE, fDone=FALSE;
   CBOOK    clTA, clAT;
   SPLIT    tMySplit=pclBook->tSplit;
   CAVAL    tCaval;

   //
   // pclBook is the author of choice: make working copies
   //
   clTA = *pclBook;
   clAT = *pclBook;
   //
   // Copy the list of current titles for this author
   //
   clTA.pclXref = pclBook->pclXref;
   clAT.pclXref = pclBook->pclXref;
   //
   while(fDone == FALSE)
   {
      switch(tMySplit)
      {
         case  CALIBRE_SPLIT_NONE:
            tMySplit = CALIBRE_SPLIT_TITLE_AUTHOR;
            break;

         case  CALIBRE_SPLIT_TITLE_AUTHOR:
            // Assume this split is "Title - Author"
            // Copy the Author into the pclName buffer
            clTA.tSplit = tMySplit;
            title_CopyTitleAuthor(&clTA, CALIBRE_AUTHOR);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, " Find Author as T-A:", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, clTA.pclName, LOG_CRLF);
            tCaval = DB_ValidateTarget(&clTA, CALIBRE_AUTHOR, CALIBRE_DB_SENTENCE);
            fCc    = title_CheckHighScore(&clTA, tCaval);
            tMySplit = CALIBRE_SPLIT_AUTHOR_TITLE;
            break;

         case  CALIBRE_SPLIT_AUTHOR_TITLE:
            // Assume this split is "Author - Title"
            clAT.tSplit = tMySplit;
            title_CopyTitleAuthor(&clAT, CALIBRE_AUTHOR);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, " Find Author as A-T:", LOG_NONE);
            pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, clAT.pclName, LOG_CRLF);
            tCaval = DB_ValidateTarget(&clAT, CALIBRE_AUTHOR, CALIBRE_DB_SENTENCE);
            fCc    = title_CheckHighScore(&clAT, tCaval);
            tMySplit = CALIBRE_SPLIT_DONE;
            break;

         default:
         case  CALIBRE_SPLIT_DONE:
            if(clAT.iScore > clTA.iScore)
            {
               *pclBook = clAT;
            }
            else
            {
               *pclBook = clTA;
            }
            fDone = TRUE;
            break;
      }
   }
   return(fCc);
}

//
//  Function:  title_GetRecord
//  Purpose:   Get one record from the input
//
//  Parms:     File/dir path, File/Dir
//  Returns:   Bytes read of zero if done
//
int CTitle::title_GetRecord(CString *pclPath, BOOL fIsFile)
{
#ifdef FEATURE_NEST_BOOKS_FOLDER
   static DIRS *pstDirs=NULL;
   CString  clStrDir, clStrFile;
#else    //FEATURE_NEST_BOOKS_FOLDER
   static HANDLE tHandle=INVALID_HANDLE_VALUE;
#endif   //FEATURE_NEST_BOOKS_FOLDER
   CString  clTmp;
   int      iTotalSize=0, iSize;

   //
   // Read all other records and subtract the author/title from them
   //
   if(fIsFile)
   {
      do
      {
         iSize = DB_ReadRecord(pclPath);
         if(iSize > 0)
         {
            iTotalSize += iSize;
            if( title_ParseRecord(fIsFile) )
            {
               // Record seems to be fine
               break;
            }
         }
         else if(!fFileOpen) iTotalSize = 0;  // EOF !!
      }
      while(fFileOpen);
   }
   else
   {
#ifdef FEATURE_NEST_BOOKS_FOLDER
      pstDirs = CVB_GetFilesInDirectories(pstDirs, pclPath, &clStrFile, &clStrDir);
      if(pstDirs)
      {
         pclRaw->Format(_T("%s"), clStrFile.GetBuffer());
         iSize = pclRaw->GetLength();
         iTotalSize += iSize;
         if( title_ParseRecord(fIsFile) )
         {
            // Record seems to be fine
            *pclDir = clStrDir;
         }
      }
      else
      {
         //
         // All files/directories have been parsed: setup for next
         //
      }
#else    //FEATURE_NEST_BOOKS_FOLDER
      tHandle = CVB_GetFileInDirectory(tHandle, pclPath, pclRaw, NULL);
      if(tHandle != INVALID_HANDLE_VALUE)
      {
         iSize = pclRaw->GetLength();
         iTotalSize += iSize;
         if( title_ParseRecord(fIsFile) )
         {
            // Record seems to be fine
         }
      }
#endif   //FEATURE_NEST_BOOKS_FOLDER
   }
   return(iTotalSize);
}

//
//  Function:  title_ParseRecord
//  Purpose:   Parse this record to extract book info (author and title)
//
//  Parms:     Parse type (file/dir)
//  Returns:   TRUE if valid record for the type
//
//  Note:      Record input  = this->pclRaw
//             Record output = this->pclDir  (if parsed a directory name)
//             Record output = this->pclFile (for the cleaned up filename entry)
//             
//             Format string:
//             DIR:  " Directory of "
//                   "Title of the book - Author first last names"
//             FILE: "##-##-#### ##:##   ###.####.#### "
//
BOOL CTitle::title_ParseRecord(BOOL fIsFile)
{
   BOOL  fCC=FALSE;
   char *pcTemp;
   const char *pcDirId = " Directory of ";
   int   iIndex, iOffset = (int) strlen(pcDirId);

   pcTemp = pclRaw->GetBuffer();
   //
   if(fIsFile)
   {
      if(*pcTemp == ' ')
      {
         iIndex = pclRaw->Find(pcDirId, 0);
         if(iIndex != -1)
         {
            // This is the actual book info directory name:
            // " Directory of F:\eBooks\CalibreDB-EN\A. Bertram Chandler\What You Know"
            //
            pclRaw->Delete(0, iOffset);
            *pclDir = *pclRaw;
         }
      }
      else
      {
         // "30-08-2011  23:22    <DIR>          ."
         // "30-08-2011  23:22    <DIR>          .."
         // "03-06-2011  20:42    <DIR>          A. Bertram Chandler"
         // "12-08-2011  20:57    <DIR>          Addison E. Steele"
         // ""
         // " Directory of F:\eBooks\CalibreDB-EN\A. Bertram Chandler\What You Know"
         // "28-04-2011  21:22           100.276 cover.jpg"
         // "28-04-2011  21:17           120.752 What You Know - A. Bertram Chandler.epub"
         // "30-04-2011  20:35             1.089 metadata.opf"
         //
         // Skip date field
         //
         if( !isdigit(*pcTemp) ) return(fCC);
         pcTemp = CVB_SkipWord(pcTemp);
         //
         // Skip time field
         //
         if( !isdigit(*pcTemp) ) return(fCC);
         pcTemp = CVB_SkipWord(pcTemp);
         //
         // Skip size field
         //
         if( !isdigit(*pcTemp) ) return(fCC);
         pcTemp = CVB_SkipWord(pcTemp);
         //
         // Book info should be here
         //
         *pclFile = pcTemp;
         //
         // Good title record:
         // "What You Know - A. Bertram Chandler.epub"
         // 
         fCC = TRUE;
      }
   }
   else
   {
      *pclFile = *pclRaw;
      pclFile->Trim();
      fCC = TRUE;
   }
   return(fCC);
}

//
//  Function:  title_ParseBookType
//  Purpose:   Parse this book record to extract the type
//             Remove the filename extension when found.
//
//  Parms:      
//  Returns:   EBOOK type
//  In:        pclFile -> raw book filename "What You Know - A. Bertram Chandler.epub"
//  Out:       pclFile -> raw book filename "What You Know - A. Bertram Chandler"
//
EBOOK CTitle::title_ParseBookType(void)
{
   int      iIdx;
   CString  clStr;
   EBOOK    tFormat  = CALIBRE_EBOOK_NONE;
   //
   if(pclFile)
   {
      iIdx = pclFile->ReverseFind('.');
      if(iIdx != -1)
      {
         // 
         // Isolate extension
         //
         clStr = pclFile->Mid(iIdx+1, 4);
         //
         // Delete the book type (file extension)
         //
         pclFile->SetAt(iIdx, 0);
         pclFile->ReleaseBuffer(-1);
         //
         tFormat = DB_GetBinaryBookFormat(&clStr);
      }
   }
   return(tFormat);
}

//
//  Function:  title_ParseTitles
//  Purpose:   Parse through the other titles of the author of this book and try to find a match
//
//  Parms:     CBOOK *
//
//      pclBook-> +------+
//                |      |
//                | File | --> "Title of the book - AuthorLast, First"
//                | Name | --> "Title of the book"
//                | Xref | --> pclAuthor
//                +------+     |-->Xref = pclTitle1
//                                           pclTitle2
//                                              pclTitle3
//                                                 0  
//  Returns:   TRUE if score improved
//
BOOL CTitle::title_ParseTitles(CBOOK *pclBook)
{
   BOOL     fCc=FALSE;
   CAVAL    tCaval;
   int      iValidate=0, iDelta=0;
   CBOOK    clDbw;
   CBOOK    clDbs;
   CString  clStr;

   if(pclBook->tSplit == CALIBRE_SPLIT_TITLE_AUTHOR)
   {
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, " Find Title as T-A:", LOG_NONE);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclBook->pclFile, LOG_CRLF);
   }
   else
   {
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, " Find Title as A-T:", LOG_NONE);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, pclBook->pclFile, LOG_CRLF);
   }

   pclNvm->NvmGet(NVM_TITLE_THOLD, &pclBook->iScore);
   title_CopyTitleAuthor(pclBook, CALIBRE_TITLE);

   //
   //
   // Pick out the best title match:
   //       DB     = Aaaaa bb Cccc ee Ddddd
   //       Target = ee Ddddd
   //
   //    1) Verify DB-WORDS    against Target-SENTENCE:
   //       Look up each word in DB name in the target name sentence
   //    2) Verify DB-SENTENCE against Target-WORDS
   //       Look up each word in target name in the DB name sentence
   //
   // Check if the two titles are (almost) identical. If not use the lowest
   // score.
   //
   pclBook->iScore = 0;
   //
   // Make working copies of this book
   //
   clDbw  = *pclBook;
   clDbs  = *pclBook;
   //
   // Pass the author of this book
   //
   clDbw.pclXref = pclBook->pclXref;
   clDbs.pclXref = pclBook->pclXref;
   // 
   tCaval = CALIBRE_VAL_INIT;
   //
   while(iValidate < 2)
   {
      switch(tCaval)
      {
         default:
         case CALIBRE_VAL_INIT:
            tCaval = DB_ValidateTarget(&clDbs, CALIBRE_TITLE, CALIBRE_DB_SENTENCE);
            iValidate++;
            break;

         case CALIBRE_VAL_PERFECT:
         case CALIBRE_VAL_OKEEISH:
            fCc = TRUE;
         case CALIBRE_VAL_NOMATCH:
            iValidate++;
            break;
      } 
   }
   //
   if(fCc)
   {
      iDelta = abs(clDbw.iScore - clDbs.iScore);
      //
      clStr.Format(_T("         Delta Title (Dbw=%d)-(Dbs=%d)=%d%%"), clDbw.iScore, clDbs.iScore, iDelta);
      pclLog->LOG_Write(CALIBRE_LOG_PROGRESS, &clStr, LOG_CRLF);
      //
      if(iDelta > 10)
      {
         // Too much deviation: take the lowest score to be save
         if(clDbw.iScore > clDbs.iScore) *pclBook = clDbs;
         else                            *pclBook = clDbw;
      }
      else
      {
         // Close match: both entries are similar: take best one
         if(clDbw.iScore > clDbs.iScore) *pclBook = clDbw;
         else                            *pclBook = clDbs;
      }
   }
   return(fCc);
}

//
//  Function:  title_RetrieveAuthor
//  Purpose:   Retrieve part of the book filename as author
//
//  Parms:     index, split, Target Author/Title
//  Returns:   TRUE if part of the book filename was retrieved correctly
//
//  In:        this->pclFile  : "01 - Title title title - Author A. A. Author"
//             this->tSplit   : CALIBRE_SPLIT_xxxxx
//             this->iSplit   : Split index
// 
//  Out:       this->pclName  : "Author A. A. Author" or other permutation
//             this->tSplit   : Updated split
//             this->iSplit   : Updated Split index
//
BOOL CTitle::title_RetrieveTitleAuthor(CBOOK *pclBook, CATA tTarget)
{
   BOOL     fCc=FALSE, fDone=FALSE;
   int      iHyphenNr;
   CString  clStr;

   switch(tTarget)
   {
      default:
      case CALIBRE_TITLE:
         fCc   = title_CopyTitleAuthor(pclBook, CALIBRE_TITLE);
         fDone = TRUE;
         break;

      case CALIBRE_AUTHOR:
         while(fDone == FALSE)
         {
            iHyphenNr = pclBook->pclFile->Find("-", pclBook->iSplit+1);
            if(iHyphenNr != -1)
            {
               // save filename, replace all hyphens except one
               clStr = *pclBook->pclFile;
               pclBook->pclFile->Replace('-', ' ');
               pclBook->pclFile->SetAt(iHyphenNr, '-');
               fCc = title_CopyTitleAuthor(pclBook, CALIBRE_AUTHOR, pclBook->tSplit, iHyphenNr);
               // restore filename and hyphen
              *pclBook->pclFile = clStr;
               pclBook->iSplit  = iHyphenNr;
               fDone = TRUE;
            }
            else
            {
               // No more hyphens found, check next permutation
               switch(pclBook->tSplit)
               {
                  case CALIBRE_SPLIT_NONE:
                     pclBook->tSplit = CALIBRE_SPLIT_TITLE_AUTHOR;
                     pclBook->iSplit = 0;
                     break;
         
                  case CALIBRE_SPLIT_TITLE_AUTHOR:
                     pclBook->tSplit = CALIBRE_SPLIT_AUTHOR_TITLE;
                     pclBook->iSplit = 0;
                     break;
         
                  case CALIBRE_SPLIT_AUTHOR_TITLE:
                     pclBook->tSplit = CALIBRE_SPLIT_DONE;
                     pclBook->iSplit = 0;
                     break;
         
                  default:
                  case CALIBRE_SPLIT_DONE:
                     fDone = TRUE;
                     break;
               }
            }
         }
   }
   return(fCc);
}




